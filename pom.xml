<?xml version="1.0" encoding="utf-8"?><project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>com.almis.awe</groupId>
    <artifactId>awe-starter-parent</artifactId>
    <version>4.3.7-SNAPSHOT</version>
    <relativePath/>
  </parent>

  <groupId>com.almis.awe.clients</groupId>
  <artifactId>awe-angular-redux</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>AWE Angular Redux</name>
  <description>Almis Angular Redux Web Engine Client</description>
  <url>https://gitlab.com/aweframework/awe-angular-redux/tree/master</url>

  <properties>
    <!-- Project versions -->
    <revision>1.0.0-SNAPSHOT</revision>
    <java.debug>false</java.debug>

    <!-- Jacoco configuration -->
    <jacoco.base.reports>${project.basedir}/../tests-angular-redux/target</jacoco.base.reports>
    <jacoco.reportPaths>${jacoco.base.reports}/coverage-reports</jacoco.reportPaths>

    <!-- Sonar configuration -->
    <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
    <sonar.links.homepage>https://gitlab.com/aweframework/awe-angular-redux</sonar.links.homepage>
    <sonar.links.ci>https://gitlab.com/aweframework/awe-angular-redux/pipelines</sonar.links.ci>
    <sonar.links.issue>https://gitlab.com/aweframework/awe-angular-redux/issues</sonar.links.issue>
    <sonar.links.scm>https://gitlab.com/aweframework/awe-angular-redux.git</sonar.links.scm>
    <sonar.junit.reportPath>${jacoco.base.reports}/surefire-reports</sonar.junit.reportPath>
    <sonar.coverage.jacoco.xmlReportPaths>${jacoco.reportPaths}/jacoco.xml</sonar.coverage.jacoco.xmlReportPaths>
    <sonar.testExecutionReportPaths>
      ${project.basedir}/awe-client-angular-redux/target/reports/karma/junit/javascriptUnitTests.xml
    </sonar.testExecutionReportPaths>
    <sonar.sources>${project.basedir}/src/main</sonar.sources>
    <sonar.binaries>${project.build.directory}/classes</sonar.binaries>
    <sonar.java.source>${java.version}</sonar.java.source>

    <!-- plugin versions -->
    <maven-site-plugin.version>3.7.1</maven-site-plugin.version>
    <maven-project-info-reports-plugin.version>2.7</maven-project-info-reports-plugin.version>
    <nexus-staging-maven-plugin.version>1.6.8</nexus-staging-maven-plugin.version>
    <build-helper-maven-plugin.version>3.0.0</build-helper-maven-plugin.version>
    <maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>
    <maven-source-plugin.version>2.4</maven-source-plugin.version>
    <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version>
    <maven-javadoc-plugin.version>3.1.1</maven-javadoc-plugin.version>
    <plugin-resources.version>3.1.0</plugin-resources.version>
    <plugin-archetype.version>3.1.1</plugin-archetype.version>
    <plugin-site.version>3.5.1</plugin-site.version>
    <plugin-info-reports.version>2.9</plugin-info-reports.version>
    <plugin-dependency.version>3.0.2</plugin-dependency.version>
    <gitflow-maven-plugin.version>1.13.0</gitflow-maven-plugin.version>
    <plugin-lombok.version>1.18.6.0</plugin-lombok.version>

    <!-- Build environment -->
    <build.environment>development</build.environment>

    <!-- Skip stages -->
    <skip.javadoc>true</skip.javadoc>
  </properties>

  <modules>
    <module>awe-client-angular-redux</module>
    <module>test-angular-redux</module>
  </modules>

  <build>
    <plugins>
      <!-- Git flow -->
      <plugin>
        <groupId>com.amashchenko.maven.plugin</groupId>
        <artifactId>gitflow-maven-plugin</artifactId>
        <version>${gitflow-maven-plugin.version}</version>
        <configuration>
          <gitFlowConfig>
            <productionBranch>master</productionBranch>
            <developmentBranch>develop</developmentBranch>
            <featureBranchPrefix>feature/</featureBranchPrefix>
            <releaseBranchPrefix>release/</releaseBranchPrefix>
            <hotfixBranchPrefix>hotfix/</hotfixBranchPrefix>
            <supportBranchPrefix>support/</supportBranchPrefix>
            <versionTagPrefix>v</versionTagPrefix>
            <origin>origin</origin>
          </gitFlowConfig>
        </configuration>
      </plugin>
    </plugins>
    <pluginManagement>
      <plugins>
        <!-- Maven source plugin -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-source-plugin</artifactId>
          <version>${maven-source-plugin.version}</version>
          <executions>
            <execution>
              <id>attach-sources</id>
              <goals>
                <goal>jar-no-fork</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <!-- Maven javadoc plugin -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>${maven-javadoc-plugin.version}</version>
          <configuration>
            <skip>${skip.javadoc}</skip>
          </configuration>
          <executions>
            <execution>
              <id>attach-javadocs</id>
              <goals>
                <goal>jar</goal>
              </goals>
            </execution>
            <execution>
              <id>aggregate</id>
              <goals>
                <goal>aggregate</goal>
              </goals>
              <phase>site</phase>
            </execution>
          </executions>
        </plugin>
        <!-- Deploy plugin -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${maven-deploy-plugin.version}</version>
        </plugin>
        <!-- Dependency plugin -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${plugin-dependency.version}</version>
        </plugin>
        <!-- Site -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${plugin-site.version}</version>
        </plugin>
        <!-- Project info report -->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <version>${plugin-info-reports.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <profiles>
    <!-- GPG Signature on release -->
    <profile>
      <id>release-sign-artifacts</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <version>${maven-gpg-plugin.version}</version>
            <executions>
              <execution>
                <id>sign-artifacts</id>
                <phase>verify</phase>
                <goals>
                  <goal>sign</goal>
                </goals>
                <configuration>
                  <gpgArguments>
                    <arg>--pinentry-mode</arg>
                    <arg>loopback</arg>
                  </gpgArguments>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-deploy-plugin</artifactId>
            <configuration>
              <skip>true</skip>
            </configuration>
          </plugin>
          <!-- Deploy nexus -->
          <plugin>
            <groupId>org.sonatype.plugins</groupId>
            <artifactId>nexus-staging-maven-plugin</artifactId>
            <version>${nexus-staging-maven-plugin.version}</version>
            <extensions>true</extensions>
            <configuration>
              <serverId>central</serverId>
              <nexusUrl>https://oss.sonatype.org/</nexusUrl>
              <autoReleaseAfterClose>true</autoReleaseAfterClose>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <licenses>
    <license>
      <name>The Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:https://gitlab.com/aweframework/awe-angular-redux.git</connection>
    <developerConnection>scm:git:https://gitlab.com/aweframework/awe-angular-redux.git</developerConnection>
    <url>https://gitlab.com/aweframework/awe-angular-redux/tree/master/</url>
  </scm>

  <developers>
    <developer>
      <name>Pablo García</name>
      <email>pablo.garcia@almis.com</email>
      <organization>Almis I.F.</organization>
      <organizationUrl>http://www.almis.com</organizationUrl>
    </developer>
    <developer>
      <name>Pablo Vidal</name>
      <email>pablo.vidal@almis.com</email>
      <organization>Almis I.F.</organization>
      <organizationUrl>http://www.almis.com</organizationUrl>
    </developer>
    <developer>
      <name>Aitor Ugarte</name>
      <email>aitor.ugarte@almis.com</email>
      <organization>Almis I.F.</organization>
      <organizationUrl>http://www.almis.com</organizationUrl>
    </developer>
  </developers>

  <repositories>
    <repository>
      <id>oss.sonatype.org-snapshot</id>
      <name>Snapshots</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
      <releases>
        <enabled>false</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>central</id>
      <name>Releases</name>
      <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
    </repository>
    <snapshotRepository>
      <id>central</id>
      <name>Snapshots</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>

</project>
