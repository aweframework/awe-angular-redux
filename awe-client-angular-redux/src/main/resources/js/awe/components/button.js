import { aweApplication } from "../awe";

import "../services/button";

// Button component
aweApplication.component('aweButton', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="{{::$ctrl.componentClass}}" ng-attr-uid="{{::$ctrl.uid}}" ng-cloak>
     <button type="{{::$ctrl.buttonType}}" id="{{::$ctrl.id}}" title="{{$ctrl.attributes.title | translateMultiple}}"
             class="{{$ctrl.buttonClass}}" ng-disabled="$ctrl.isDisabled()" ng-click="$ctrl.onClick($event)"
             ng-mousedown="$ctrl.onMouseDown($event)">
       <i ng-if="::$ctrl.attributes.icon !== ''" class="fa fa-{{::$ctrl.attributes.icon}}"></i>
       <span ng-if="::$ctrl.attributes.label !== ''" class="button-text" translate-multiple="{{$ctrl.attributes.label}}"></span>
     </button>
     <awe-context-menu ng-cloak></awe-context-menu>
   </div>`,
  bindings: {
    'id': '@buttonId'
  },
  controller: ['Button', '$scope',
    function ($button, $scope) {
      // Initialize as button
      $button.init(this, $scope);
    }
  ]
});
