import { aweApplication } from "../../awe";
import { templateSelector } from "../../services/selector";

// Select multiple component
aweApplication.component('aweInputSelectMultiple', {
  template: templateSelector(true, false),
  bindings: {
    'id': '@inputSelectMultipleId'
  },
  controller: ['Selector', '$scope',
    function ($selector, $scope) {
      // Initialize as select multiple
      $selector.initSelectMultiple(this, $scope);
    }
  ]
});

