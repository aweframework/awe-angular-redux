import { aweApplication } from "../../awe";

import "../../services/criterion";

// Text component
aweApplication.component('aweInputTextarea', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-uid="{{::$ctrl.uid}}"
     ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="$ctrl.groupClass" ng-cloak>
       <label ng-attr-for="{{::$ctrl.id}}" ng-class="$ctrl.labelClass" ng-style="$ctrl.labelStyle" ng-cloak>
         {{$ctrl.attributes.label| translateMultiple}}
         <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
       </label>
       <div class="validator input {{$ctrl.validatorGroup}} focus-target" ng-class="{'input-group': $ctrl.attributes.unit}">
         <span ng-if="$ctrl.attributes.icon" ng-class="$ctrl.iconClass" ng-cloak></span>
         <textarea class="form-control {{$ctrl.inputClass}}" ng-disabled="$ctrl.attributes.readonly" ng-model="$ctrl.value.value"
                ng-click="$ctrl.onClick($event)" ng-attr-id="{{::$ctrl.attributes.id}}" ng-attr-name="{{::$ctrl.id}}"
                placeholder="{{$ctrl.attributes.placeholder | translateMultiple}}" ng-model-options="{updateOn: 'change'}"
                ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()" ng-change="$ctrl.onChange()" autocomplete="off"
                rows="{{::$ctrl.attributes.areaRows}}"></textarea>
         <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
         <span ng-if="$ctrl.attributes.unit" class="input-group-addon unit" translate-multiple="{{$ctrl.attributes.unit}}" ng-cloak></span>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputTextareaId'
  },
  controller: ['Criterion', '$scope',
    function ($criterion, $scope) {
      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});
