import { aweApplication } from "../../awe";
import { templateDate } from "../../services/dateTime";

// Date component
aweApplication.component('aweInputFilteredCalendar', {
  template: templateDate,
  bindings: {
    'id': '@inputFilteredCalendarId'
  },
  controller: ['DateTime', '$scope',
    function ($dateTime, $scope) {
      // Initialize as filtered calendar
      $dateTime.initFilteredCalendar(this, $scope);
    }
  ]
});
