import { aweApplication } from "../../awe";

import "../../services/criterion";

// Text component
aweApplication.component('aweInputTextView', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="::$ctrl.groupClass" ng-cloak>
       <label ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
         {{$ctrl.attributes.label| translateMultiple}}
         <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
       </label>
       <div class="input {{$ctrl.inputClass}} {{::$ctrl.validatorGroup}}">
         <span ng-if="$ctrl.attributes.unit" class="label label-warning pull-right" translate-multiple="{{$ctrl.attributes.unit}}"></span>
         <div ng-click="$ctrl.onClick()">
           <span ng-if="::$ctrl.attributes.icon" class="text-icon fa fa-{{::$ctrl.attributes.icon}}" ng-cloak></span>
           <span class="text-value" ng-cloak>{{$ctrl.value.label || $ctrl.value.value || ""}}</span>
         </div>
         <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputTextViewId'
  },
  controller: ['Criterion', '$scope',
    function ($criterion, $scope) {
      // Get controller
      let $ctrl = this;

      // Initialize as criterion
      $criterion.init($ctrl, $scope);
    }
  ]
});
