import { aweApplication } from "../../awe";
import "../../services/checkboxRadio";

// Date component
aweApplication.component('aweInputButtonRadio', {
  template:
  `<label ng-show="$ctrl.attributes.visible" class="criterion btn btn-awe validator input {{$ctrl.criterionClass}} focus-target" ng-class="{'active btn-primary': $ctrl.value.value === $ctrl.attributes.value, 'disabled': $ctrl.attributes.readonly}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <input type="radio"
       class="form-control px {{$ctrl.inputClass}}"
       ng-attr-id="{{::$ctrl.id}}"
       ng-attr-name="{{::$ctrl.id}}"
       ng-focus="$ctrl.onFocus()"
       ng-blur="$ctrl.onBlur()"
       ng-model="$ctrl.value.value"
       ng-value="$ctrl.attributes.value"
       ng-change="$ctrl.onChange()"
       ng-disabled="$ctrl.attributes.readonly"/>
     <i ng-if="::$ctrl.attributes.icon !== ''" class="fa fa-{{::$ctrl.attributes.icon}}"></i>
     <span class="lbl label-{{::$ctrl.attributes.size}}">
       {{$ctrl.attributes.label| translateMultiple}}
       <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
     </span>
     <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
   </label>`,
  bindings: {
    'id': '@inputButtonRadioId'
  },
  controller: ['CheckboxRadio', '$scope',
    function ($checkboxRadio, $scope) {
      // Initialize as radio
      $checkboxRadio.initRadio(this, $scope);
    }
  ]
});
