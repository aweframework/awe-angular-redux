import { aweApplication } from "../../awe";
import "../../services/checkboxRadio";

// Date component
aweApplication.component('aweInputRadio', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="$ctrl.groupClass" ng-cloak>
       <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-cloak></label>
       <div class="validator input">
         <label class="radio">
           <input type="radio"
             class="form-control px {{$ctrl.inputClass}}"
             ng-attr-id="{{::$ctrl.id}}"
             ng-attr-name="{{::$ctrl.group}}"
             ng-model="$ctrl.value.value"
             ng-value="$ctrl.attributes.value"
             ng-change="$ctrl.onChange()"
             ng-focus="$ctrl.onFocus()"
             ng-blur="$ctrl.onBlur()"
             ng-disabled="$ctrl.attributes.readonly"/>
           <span class="lbl label-{{::$ctrl.attributes.size}}">
             {{$ctrl.attributes.label| translateMultiple}}
             <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
           </span>
           <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
         </label>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputRadioId'
  },
  controller: ['CheckboxRadio', '$scope',
    function ($checkboxRadio, $scope) {
      // Initialize as radio
      $checkboxRadio.initRadio(this, $scope);
    }
  ]
});
