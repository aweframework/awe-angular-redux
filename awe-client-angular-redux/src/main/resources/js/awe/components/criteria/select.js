import { aweApplication } from "../../awe";
import { templateSelector } from "../../services/selector";

// Select component
aweApplication.component('aweInputSelect', {
  template: templateSelector(false, false),
  bindings: {
    'id': '@inputSelectId'
  },
  controller: ['Selector', '$scope',
    function ($selector, $scope) {
      // Initialize as select
      $selector.initSelect(this, $scope);
    }
  ]
});
