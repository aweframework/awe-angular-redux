import { aweApplication } from "../../awe";
import { templateNumeric } from "../../services/numeric";

// Numeric component
aweApplication.component('aweInputNumeric', {
  template: templateNumeric,
  bindings: {
    'id': '@inputNumericId'
  },
  controller: ['Numeric', '$scope',
    function ($numeric, $scope) {
      // Initialize as criterion
      $numeric.init(this, $scope);
    }
  ]
});
