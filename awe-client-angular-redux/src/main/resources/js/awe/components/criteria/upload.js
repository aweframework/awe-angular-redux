import { aweApplication } from "../../awe";
import "../../services/uploader";

// Date component
aweApplication.component('aweInputUploader', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion uploader" ng-class="$ctrl.criterionClass" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="::$ctrl.groupClass" ng-cloak>
       <label ng-attr-for="::$ctrl.id" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
         {{$ctrl.attributes.label| translateMultiple}}
         <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
       </label>
       <div class="validator input {{::$ctrl.validatorGroup}} focus-target">
         <div ng-show="$ctrl.uploading" class="uploading-{{::$ctrl.attributes.size}}">
           <div class="progress progress-striped active" ng-if="$ctrl.uploading">
             <div class="progress-bar" ng-style="{width: $ctrl.uploadProgress}"></div>
           </div>
         </div>
         <div ng-show="$ctrl.value === null && !$ctrl.uploading" class="pixel-file-input {{$ctrl.inputClass}}"
              ng-disabled="$ctrl.attributes.readonly"
              ngf-select
              ngf-change="$ctrl.onChooseFile($files)"
              ngf-validate-fn="$ctrl.onValidate($file)">
           <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
           <span class="pfi-filename pfi-placeholder">{{controller.placeholder| translateMultiple}}</span>
           <div class="pfi-actions" ng-if="!$ctrl.attributes.readonly">
             <button type="button"
               class="btn btn-xs btn-primary pfi-choose"
               translate-multiple="BUTTON_CHOOSE"
               ng-focus="$ctrl.onFocus()"
               ng-blur="$ctrl.onBlur()"></button>
           </div>
         </div>
         <div ng-show="$ctrl.value !== null && !$ctrl.uploading" class="pixel-file-input {{$ctrl.inputClass}}" ng-disabled="$ctrl.attributes.readonly" >
           <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
           <span class="pfi-filename" ng-click="$ctrl.onDownloadFile($event)">{{$ctrl.value.label}}</span>
           <div class="pfi-actions" ng-if="!$ctrl.attributes.readonly">
             <button type="button"
               ng-click="$ctrl.onClearFile($event)"
               class="btn btn-xs"
               ng-disabled="$ctrl.deleting"
               ng-focus="$ctrl.onFocus()"
               ng-blur="$ctrl.onBlur()"><i class="fa {{$ctrl.deleting ? 'fa-refresh fa-spin' : 'fa-times'}}"></i> <span translate-multiple="BUTTON_CLEAR"></span></button>
           </div>
         </div>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputUploaderId'
  },
  controller: ['Uploader', '$scope',
    function ($uploader, $scope) {
      // Initialize as uploader
      $uploader.init(this, $scope);
    }
  ]
});
