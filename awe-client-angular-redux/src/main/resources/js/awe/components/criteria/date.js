import { aweApplication } from "../../awe";
import { templateDate } from "../../services/dateTime";

// Date component
aweApplication.component('aweInputDate', {
  template: templateDate,
  bindings: {
    'id': '@inputDateId'
  },
  controller: ['DateTime', '$scope',
    function ($dateTime, $scope) {
      // Initialize as date
      $dateTime.initDate(this, $scope);
    }
  ]
});
