import { aweApplication } from "../../awe";
import "angular-bootstrap-colorpicker";

// Add requirements
aweApplication.requires.push("colorpicker.module");

import "../../services/criterion";

// Text component
aweApplication.component('aweInputColor', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-uid="{{::$ctrl.uid}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="::$ctrl.groupClass" ng-cloak>
       <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
         {{$ctrl.attributes.label| translateMultiple}}
         <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
       </label>
       <div class="validator input-group colorpicker-element {{::$ctrl.validatorGroup}} focus-target">
         <span ng-if="::controller.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
         <input type="text" class="form-control {{$ctrl.inputClass}}" ng-disabled="$ctrl.attributes.readonly" ng-model="$ctrl.value.value"
           ng-press-enter="$ctrl.onSubmit($event)" autocomplete="off" ng-click="$ctrl.click($event)" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"
           ng-attr-id="{{::$ctrl.id}}" ng-attr-name="{{::$ctrl.id}}" placeholder="{{$ctrl.attributes.placeholder| translateMultiple}}" ng-change="$ctrl.onChange()"/>
         <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
         <span ng-switch="$ctrl.attributes.readonly" class="input-group-addon">
           <span ng-switch-when="false" colorpicker ng-model="$ctrl.value.value" ng-change="$ctrl.onChange()"><i ng-class="{'transparent': !$ctrl.value.value}" ng-style="{backgroundColor: $ctrl.value.value}"></i></span>
           <span ng-switch-when="true" class="disabled"><i ng-class="{'transparent': !$ctrl.value.value}" ng-style="{backgroundColor: $ctrl.value.value}"></i></span>
         </ng-switch>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputColorId'
  },
  controller: ['Criterion', '$scope',
    function ($criterion, $scope) {
      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});
