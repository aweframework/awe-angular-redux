import { aweApplication } from "../../awe";
import { templateSelector } from "../../services/selector";

// Suggest multiple component
aweApplication.component('aweInputSuggestMultiple', {
  template: templateSelector(true, true),
  bindings: {
    'id': '@inputSuggestMultipleId'
  },
  controller: ['Selector', '$scope',
    function ($selector, $scope) {
      // Initialize as suggest multiple
      $selector.initSuggestMultiple(this, $scope);
    }
  ]
});
