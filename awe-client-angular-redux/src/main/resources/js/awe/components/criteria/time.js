import { aweApplication } from "../../awe";
import "../../services/dateTime";

// Time component
aweApplication.component('aweInputTime', {
  template:
  `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
      <awe-context-menu ng-cloak></awe-context-menu>
      <div ng-class="$ctrl.groupClass" ng-cloak>
        <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
          {{$ctrl.attributes.label| translateMultiple}}
          <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
        </label>
        <div class="validator input-group input-append date {{::$ctrl.validatorGroup}} focus-target"
          ng-readonly="$ctrl.attributes.readonly">
          <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
          <input type="text"
              ui-time="$ctrl.config"
              class="form-control {{$ctrl.inputClass}}"
              ng-disabled="$ctrl.attributes.readonly"
              ng-model="$ctrl.value.value"
              ng-change="$ctrl.onChange()"
              ng-click="$ctrl.onClick($event)"
              ng-attr-id="{{::$ctrl.attributes.id}}"
              ng-attr-name="{{::$ctrl.id}}"
              placeholder="{{$ctrl.attributes.placeholder | translateMultiple}}"
              ng-model-options="{updateOn: 'change'}"
              ng-focus="$ctrl.onFocus()"
              ng-blur="$ctrl.onBlur()"
              autocomplete="off"
              ng-press-enter="$ctrl.onSubmit($event)"/>
          <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
          <span class="input-group-addon add-on">
            <i class="fa fa-clock-o"></i>
          </span>
       </div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputTimeId'
  },
  controller: ['DateTime', '$scope',
    function ($dateTime, $scope) {
      // Initialize as time
      this.initializePlugin = true;
      $dateTime.initTime(this, $scope);
    }
  ]
});
