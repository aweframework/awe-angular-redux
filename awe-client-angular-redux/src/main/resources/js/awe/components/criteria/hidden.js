import { aweApplication } from "../../awe";

import "../../services/criterion";

// Text component
aweApplication.component('aweInputHidden', {
  bindings: {
    'id': '@inputHiddenId'
  },
  controller: ['Criterion', '$scope',
    function ($criterion, $scope) {
      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});
