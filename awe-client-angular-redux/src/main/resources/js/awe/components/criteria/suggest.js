import { aweApplication } from "../../awe";
import { templateSelector } from "../../services/selector";

// Suggest component
aweApplication.component('aweInputSuggest', {
  template: templateSelector(false, true),
  bindings: {
    'id': '@inputSuggestId'
  },
  controller: ['Selector', '$scope',
    function ($selector, $scope) {
      // Initialize as suggest
      $selector.initSuggest(this, $scope);
    }
  ]
});
