import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/component";

// Image column
aweApplication.component('aweColumnImage', {
  template:
    `<img ng-src="{{$ctrl.value.image}}" class="grid-image no-animate {{$ctrl.value.style}}" ng-alt="{{$ctrl.value.label| translateMultiple}}" title="{{$ctrl.value.label| translateMultiple}}"/>`,
  controller: ['Column', 'Component', '$scope',
    function ($column, $component, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as component
      $component.init(this, $scope);
    }
  ]
});