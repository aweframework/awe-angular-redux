import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/criterion";

// Text column
aweApplication.component('aweColumnTextarea', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
      <span class="visible-value" ng-cloak>{{$ctrl.value.value}}</span>
      <span class="edition">
        <div class="input focus-target">
          <textarea class="form-control col-xs-12 {{$ctrl.inputClass}} {{$ctrl.value.style}}" ng-model="$ctrl.value.value" 
            ng-disabled="$ctrl.attributes.readonly" autocomplete="off" ng-model-options="{updateOn: 'change'}" ng-click="$ctrl.onClick($event)" 
            ng-change="$ctrl.onChange()" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"></textarea>
        </div>
        <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
      </span>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'Criterion', '$scope',
    function ($column, $criterion, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});