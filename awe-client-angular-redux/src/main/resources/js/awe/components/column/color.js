import { aweApplication } from "../../awe";
import "angular-bootstrap-colorpicker";

// Add requirements
aweApplication.requires.push("colorpicker.module");

import "../../services/column";
import "../../services/criterion";

// Text column
aweApplication.component('aweColumnColor', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
      <span class="visible-value" ng-cloak><i class="fa fa-eyedropper" ng-if="$ctrl.value.value" ng-style="{color: $ctrl.value.value}"></i>
        <span class="text-semibold text-xs text-uppercase">{{$ctrl.value.value}}</span>
      </span>
      <span class="edition">
        <div ng-class="::$ctrl.groupClass" ng-cloak>
          <div class="validator input-group colorpicker-element input-group-{{::$ctrl.attributes.size}} focus-target">
            <input type="text" class="form-control {{$ctrl.inputClass}} {{$ctrl.value.style}}" ng-disabled="$ctrl.attributes.readonly" ng-model="$ctrl.value.value" 
              ng-press-enter="$ctrl.onSaveRow($event)" ng-click="$ctrl.onClick($event)" ng-change="$ctrl.onChange()" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"/>
            <span ng-if="!$ctrl.attributes.readonly" class="input-group-addon" colorpicker ng-model="$ctrl.value.value" ng-change="$ctrl.onChange()" ><i 
              ng-class="{'transparent': !$ctrl.value.value}" ng-style="{backgroundColor: $ctrl.value.value}"></i></span>
            <span ng-if="$ctrl.attributes.readonly" class="input-group-addon disabled"><i ng-class="{'transparent': !$ctrl.value.value}" 
              ng-style="{backgroundColor: $ctrl.value.value}"></i></span>
          </div>
          <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
        </div>
      </span>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'Criterion', '$scope',
    function ($column, $criterion, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});