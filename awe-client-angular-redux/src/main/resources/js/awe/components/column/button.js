import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/button";

// Text column
aweApplication.component('aweColumnButton', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="column text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
      <button type="button" id="{{$ctrl.id}}" title="{{$ctrl.attributes.title| translateMultiple}}"
              class="{{$ctrl.buttonClass}} {{$ctrl.value.style}}" ng-disabled="$ctrl.isDisabled()" ng-click="$ctrl.onClick($event)" ng-mousedown="$ctrl.onMouseDown()" ng-cloak>
        <i ng-if="$ctrl.attributes.icon || $ctrl.value.icon" class="fa {{$ctrl.value.icon || 'fa-' + $ctrl.attributes.icon}}"></i>
        <span ng-if="$ctrl.value.label" class="button-text" translate-multiple="{{$ctrl.value.label}}"></span>
      </button>
    </div>`,
  controller: ['Column', 'Button', '$scope',
    function ($column, $button, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as button
      $button.init(this, $scope);
    }
  ]
});