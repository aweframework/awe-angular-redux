import { aweApplication } from "../../awe";

import "../../services/column";
import { templateSelectorColumn } from "../../services/selector";

// Select multiple column
aweApplication.component('aweColumnSelectMultiple', {
  template: templateSelectorColumn(true, false),
  controller: ['Column', 'Selector', '$scope',
    function ($column, $selector, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as select
      $selector.initSelect(this, $scope);
    }
  ]
});