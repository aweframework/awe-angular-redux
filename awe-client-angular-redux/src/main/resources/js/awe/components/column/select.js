import { aweApplication } from "../../awe";

import "../../services/column";
import { templateSelectorColumn } from "../../services/selector";

// Select column
aweApplication.component('aweColumnSelect', {
  template: templateSelectorColumn(false, false),
  controller: ['Column', 'Selector', '$scope',
    function ($column, $selector, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as select
      $selector.initSelect(this, $scope);
    }
  ]
});