import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/component";

// Icon column
aweApplication.component('aweColumnIcon', {
  template:
    `<span class="fa {{$ctrl.value.icon}} {{$ctrl.value.style}} no-animate" title="{{($ctrl.value.title || $ctrl.value.label) | translateMultiple}}"></span>`,
  controller: ['Column', 'Component', '$scope',
    function ($column, $component, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as component
      $component.init(this, $scope);
    }
  ]
});