import { aweApplication } from "../../awe";

import "../../services/column";
import { templateDateColumn } from "../../services/dateTime";

// Filtered calendar column
aweApplication.component('aweColumnFilteredCalendar', {
  template: templateDateColumn,
  controller: ['Column', 'DateTime', '$scope',
    function ($column, $dateTime, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as filtered calendar
      $dateTime.initFilteredCalendar(this, $scope);
    }
  ]
});