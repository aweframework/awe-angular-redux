import { aweApplication } from "../../awe";

import "../../services/column";
import { templateDateColumn } from "../../services/dateTime";

// Date column
aweApplication.component('aweColumnDate', {
  template: templateDateColumn,
  controller: ['Column', 'DateTime', '$scope',
    function ($column, $dateTime, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as date
      $dateTime.initDate(this, $scope);
    }
  ]
});