import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/dateTime";

// Time column
aweApplication.component('aweColumnTime', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
      <span class="visible-value" ng-cloak>{{$ctrl.value.label || $ctrl.value.value}}</span>
      <span class="edition" ng-if="$ctrl.initializePlugin">
        <div class="input-group input-group-{{::$ctrl.attributes.size}} input-append date focus-target">
          <input type="text" ui-time="$ctrl.config" class="form-control add-on col-xs-12 {{$ctrl.inputClass}} {{$ctrl.value.style}}"
            autocomplete="off" ng-model="$ctrl.value.value" ng-disabled="$ctrl.attributes.readonly" ng-model-options="{updateOn: 'change'}" 
            ng-click="$ctrl.onClick($event)" ng-change="$ctrl.onChange()" ng-press-enter="$ctrl.onSaveRow($event)" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"/>
          <span class="input-group-addon add-on">
            <i class="fa fa-clock-o"></i>
          </span>
        </div>
        <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
      </span>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'DateTime', '$scope',
    function ($column, $dateTime, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as date
      $dateTime.initTime(this, $scope);
    }
  ]
});