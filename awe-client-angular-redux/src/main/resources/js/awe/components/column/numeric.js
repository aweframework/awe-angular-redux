import { aweApplication } from "../../awe";

import "../../services/column";
import {templateNumericColumn} from "../../services/numeric";

// Text column
aweApplication.component('aweColumnNumeric', {
  template: templateNumericColumn,
  controller: ['Column', 'Numeric', '$scope',
    function ($column, $numeric, $scope) {
      let $ctrl = this;

      // Initialize as column
      $column.init($ctrl, $scope);

      // Initialize as criterion
      $numeric.init($ctrl, $scope);
    }
  ]
});
