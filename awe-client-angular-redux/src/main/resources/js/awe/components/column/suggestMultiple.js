import { aweApplication } from "../../awe";

import "../../services/column";
import { templateSelectorColumn } from "../../services/selector";

// Suggest multiple column
aweApplication.component('aweColumnSuggestMultiple', {
  template: templateSelectorColumn(true, true),
  controller: ['Column', 'Selector', '$scope',
    function ($column, $selector, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as select
      $selector.initSuggest(this, $scope);
    }
  ]
});