import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/component";

// Progress column
aweApplication.component('aweColumnProgress', {
  template:
    `<div ng-show="$ctrl.attributes.visible && $ctrl.value.value > 0" class="column-progress no-animate" ng-attr-title="{{$ctrl.value.title}}" ng-cloak>
      <div class="progress-text">{{$ctrl.value.label}}</div>
      <div class="progress progress-striped active">
        <div class="progress-bar {{$ctrl.value.style}}" ng-style="{width: ($ctrl.value.value + '%')}"></div>
      </div>
    </div>`,
  controller: ['Column', 'Component', '$scope',
    function ($column, $component, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as component
      $component.init(this, $scope);
    }
  ]
});