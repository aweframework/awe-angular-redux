import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/criterion";

// Text view column
aweApplication.component('aweColumnTextView', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="column-input text-{{::$ctrl.attributes.align}} no-animate" ng-mouseup="$ctrl.onClick()" ng-cloak title="{{$ctrl.value.title}}">
      <span class="{{$ctrl.inputClass}} {{$ctrl.value.style}} col-xs-{{($ctrl.attributes.unit || $ctrl.value.unit) ? '10' : '12'}}" ng-cloak>
        <span ng-if="$ctrl.attributes.icon || $ctrl.value.icon" class="text-icon fa fa-fw fa-{{$ctrl.attributes.icon || $ctrl.value.icon}}" ng-cloak></span>
        <span class="text-value" ng-cloak>{{$ctrl.value.label || $ctrl.value.value}}</span>
      </span>
      <span ng-if="$ctrl.attributes.unit || $ctrl.value.unit" class="col-xs-2 text-right" ng-cloak>
        <span class="label label-warning" translate-multiple="{{$ctrl.attributes.unit || $ctrl.value.unit}}"></span>
      </span>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'Criterion', '$scope',
    function ($column, $criterion, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});
