import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/checkboxRadio";

// Checkbox column
aweApplication.component('aweColumnCheckbox', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion column-checkbox no-animate" ng-click="$ctrl.onClick($event)" ng-cloak>
      <div class="visible-value input">
        <label class="checkbox">
          <input type="checkbox" class="px form-control" ng-checked="$ctrl.value.selected" disabled="disabled"/>
          <span class="lbl"></span>
        </label>
      </div>
      <div class="edition input">
        <label class="checkbox">
          <input type="checkbox" class="px form-control" ng-model="$ctrl.value.selected" ng-change="$ctrl.onChange()" ng-disabled="$ctrl.attributes.readonly"/>
          <span class="lbl"></span>
        </label>
      </div>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'CheckboxRadio', '$scope',
    function ($column, $checkboxRadio, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as criterion
      $checkboxRadio.initCheckbox(this, $scope);
    }
  ]
});