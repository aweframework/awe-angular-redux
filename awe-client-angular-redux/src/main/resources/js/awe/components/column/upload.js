import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/uploader";

// Text column
aweApplication.component('aweColumnUploader', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion uploader text-{{::$ctrl.attributes.align}} {{$ctrl.value.style}} no-animate" ng-cloak>
      <div class="visible-value" ng-cloak>{{$ctrl.value.label}}</div>
      <div class="edition focus-target">
        <div ng-show="$ctrl.uploading" class="uploading-{{::$ctrl.attributes.size}} no-animate">
          <div class="progress progress-striped active" ng-if="$ctrl.uploading">
            <div class="progress-bar" ng-style="{width: $ctrl.uploadProgress}"></div>
          </div>
        </div>
        <div ng-show="$ctrl.value === null && !$ctrl.uploading" class="pixel-file-input {{$ctrl.inputClass}} no-animate"
             ng-disabled="$ctrl.attributes.readonly" ngf-select ngf-change="$ctrl.onChooseFile($files)" ngf-validate-fn="$ctrl.onValidate($file)">
          <span ng-if="::$ctrl.attributes.icon" ng-class="::iconClass" ng-cloak></span>
          <span class="pfi-filename pfi-placeholder">{{::$ctrl.attributes.placeholder| translateMultiple}}</span>
          <div class="pfi-actions" ng-if="!$ctrl.attributes.readonly">
            <button type="button" class="btn btn-xs btn-primary pfi-choose" translate-multiple="BUTTON_CHOOSE"></button>
          </div>
        </div>
        <div ng-show="$ctrl.value !== null && !$ctrl.uploading" class="pixel-file-input {{$ctrl.inputClass}} no-animate" ng-disabled="$ctrl.attributes.readonly">
          <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.attributes.iconClassSmall" ng-cloak></span>
          <span class="pfi-filename" ng-click="$ctrl.onDownloadFile($event)">{{$ctrl.value.label}}</span>
          <div class="pfi-actions" ng-if="!$ctrl.attributes.readonly">
            <button type="button" ng-click="$ctrl.onClearFile($event)" class="btn btn-xs" ng-disabled="$ctrl.deleting"><i class="fa {{$ctrl.deleting ? 'fa-refresh fa-spin' : 'fa-times'}}"></i> <span translate-multiple="BUTTON_CLEAR"></span></button>
          </div>
        </div>
      </div>
    </div>`,
  controller: ['Column', 'Uploader', '$scope',
    function ($column, $uploader, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as uploader
      $uploader.init(this, $scope);
    }
  ]
});