import { aweApplication } from "../../awe";

import "../../services/column";
import "../../services/criterion";

// Password column
aweApplication.component('aweColumnPassword', {
  template:
    `<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
      <span class="visible-value" ng-cloak>{{($ctrl.value.value || "").length > 0 ? "****" : ""}}</span>
      <span class="edition">
        <div class="input input-group-{{::$ctrl.attributes.size}} focus-target">
          <input type="password" class="form-control col-xs-12 {{$ctrl.inputClass}} {{$ctrl.value.style}}" ng-disabled="$ctrl.attributes.readonly" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"
            ng-model="$ctrl.value.value" ng-model-options="{updateOn: 'change'}" ng-change="$ctrl.onChange()" ng-press-enter="$ctrl.onSaveRow($event)" autocomplete="off"/>
          <span ng-show="$root.isCapsLockOn" class="fa fa-{{::$ctrl.attributes.size}} fa-arrow-circle-up form-control-feedback"></span>
        </div>
        <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
      </span>
      <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>`,
  controller: ['Column', 'Criterion', '$scope',
    function ($column, $criterion, $scope) {
      // Initialize as column
      $column.init(this, $scope);

      // Initialize as criterion
      $criterion.init(this, $scope);
    }
  ]
});