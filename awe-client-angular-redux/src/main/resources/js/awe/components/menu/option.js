import { aweApplication } from "../../awe";

import { updateOption } from '../../actions/menu';

/**
 * Filter options function
 * @param {Object} options Option list to filter
 * @return {Object} Options filtered
 */
export function filterOptions (options = []) {
  return options.filter(option => option.allowed);
}

// Option component
aweApplication.component('aweOption', {
  template:
  `<li class="{{::$ctrl.staticClasses}}" ng-class="$ctrl.getOptionClasses()" ng-cloak>
     <a ng-if="::!$ctrl.attributes.separator" title="{{::$ctrl.$ctrl.attributes.title| translateMultiple}}" name="{{::$ctrl.attributes.name}}"
        class="{{::$ctrl.$ctrl.attributes.style}}" ng-click="$ctrl.onClick($ctrl.attributes)">
       <i ng-if="::$ctrl.attributes.icon" class="menu-icon fa fa-{{::$ctrl.attributes.icon}}"></i>
       <span ng-if="::$ctrl.attributes.label" class="mm-text" translate-multiple="{{::$ctrl.attributes.label}}"></span>
     </a>
     <ul ng-if="::$ctrl.hasVisibleChildren()" class="{{::$ctrl.staticSubmenuClasses}}" ng-class="$ctrl.getSubmenuClasses()">
       <div ng-if="::$ctrl.attributes.label" class="mmc-title" translate-multiple="{{::$ctrl.attributes.label}}"></div>
       <awe-option ng-repeat="option in $ctrl.filterOptions($ctrl.attributes.options) track by option.name" attributes="option"
         status="$ctrl.status" on-option-click="$ctrl.onOptionClick(option)" menu-type="{{::$ctrl.menuType}}"
         close-first-level="$ctrl.closeFirstLevel()" first-level="false" selected="$ctrl.selected"></awe-option>
     </ul>
   </li>`,
  bindings: {
    'menuType': '@',
    'attributes': '=',
    'firstLevel': '=',
    'status': '=',
    'selected': '=',
    'closeFirstLevel': '&',
    'onOptionClick': '&'
  },
  controller: ['$scope', 'ActionController', '$ngRedux',
    function ($scope, $actionController, $ngRedux) {
      let $ctrl = this;

      ////////////////////////////////////
      // CONTROLLER API
      ////////////////////////////////////

      /**
       * On component initialization
       */
      $ctrl.$onInit = function() {
        $ctrl.setStaticValues();
      }

      /**
       * Check if option has visible children
       */
      $ctrl.hasVisibleChildren = function () {
        return $ctrl.filterOptions($ctrl.attributes.options).length > 0 && !$ctrl.attributes.separator;
      };

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function() {
        $ctrl.staticClasses = getStaticOptionClasses();
        $ctrl.staticSubmenuClasses = getStaticSubmenuClasses();
      }


      /**
       * Dynamic option classes
       * @returns {Array}
       */
      $ctrl.getOptionClasses = function () {
        var classes = [];
        // Opened status
        if ($ctrl.attributes.opened) {
          if (isFloating() && $ctrl.status.minimized) {
            classes.push('mmc-dropdown-open');
          } else {
            classes.push('open');
          }
        }

        // Active status
        if ($ctrl.selected === $ctrl.attributes.name) {
          classes.push('active');
        }

        return classes.join(" ");
      };

      /**
       * Dynamic submenu classes
       * @returns {Array}
       */
      $ctrl.getSubmenuClasses = function () {
        var classes = [];

        // Opened status
        if ($ctrl.attributes.opened) {
          if (isFloating() && $ctrl.status.minimized) {
            classes.push('mmc-dropdown-open-ul');
          } else {
            classes.push('opened');
          }
        }

        // Hide if not is opened or is floating
        if (!($ctrl.attributes.opened || isFloating())) {
          classes.push("ng-hide");
        }

        return classes.join(" ");
      };

      /**
       * Click button function
       */
      $ctrl.onClick = function (option) {
        if ($ctrl.hasVisibleChildren()) {
          // Node is a branch
          if ($ctrl.attributes.opened) {
            // Node is an opened branch
            $ctrl.closeOption();
          } else {
            // Node is a closed branch
            if ($ctrl.firstLevel) {
              $ctrl.closeFirstLevel();
            }
            $ngRedux.dispatch(updateOption($ctrl.attributes.name, {opened: true}));
          }
        } else {
          // Node is a leaf
          if ("actions" in $ctrl.attributes) {
            // Close all previous actions
            $actionController.closeAllActions();

            // All action list to stack
            $actionController.addActionList($ctrl.attributes.actions, false, {address: $ctrl.address});

            // Call option clicked
            $ctrl.onOptionClick(option);
          }
        }
      };

      /**
       * Closes the option and marks option as closed after animation
       */
      $ctrl.closeOption = function () {
        $ngRedux.dispatch(updateOption($ctrl.attributes.name, {opened: false}));
      };

      /**
       * Filter options
       */
      $ctrl.filterOptions = filterOptions;

      ////////////////////////////////////
      // PRIVATE METHODS
      ////////////////////////////////////

      /**
       * Check if option is opened
       */
      function isFloating() {
        return $ctrl.firstLevel && $ctrl.status.resolution !== 'mobile';
      }

      /**
       * Static option classes
       * @returns {Array}
       */
      function getStaticOptionClasses() {
        var classes = [];
        // Visible children
        if ($ctrl.hasVisibleChildren()) {
          classes.push('mm-dropdown');
        }

        // First level
        if ($ctrl.firstLevel) {
          classes.push('mm-dropdown-root');
        }

        // Separator
        if ($ctrl.attributes.separator) {
          classes.push('divider');
        }

        return classes.join(" ");
      };

      /**
       * Static submenu classes
       * @returns {Array}
       */
      function getStaticSubmenuClasses() {
        var classes = [];

        // First level classes
        if ($ctrl.firstLevel) {
          classes.push('mm-dropdown-first');
          classes.push('menu-shadow');
        } else {
          classes.push('mm-dropdown-target');
        }

        return classes.join(" ");
      }
   }]
 });
