import {aweApplication} from "../../awe";

import {ClientActions} from "../../actions/clientActions";
import {filterOptions} from "./option";

import {clearMenu, updateAllOptions, updateMenu, updateOptions, updateStatus} from '../../actions/menu';
import _ from "lodash";

// Menu component
aweApplication.component('aweMenu', {
  template:
    `<ul class="awe-menu {{::$ctrl.attributes.style}}" ng-show="$ctrl.isVisible()" ng-class="{'menu-minimized': $ctrl.attributes.minimized}" ng-cloak>
      <awe-option ng-repeat="option in $ctrl.filterOptions($ctrl.options) track by option.name" attributes="option" status="$ctrl.menuStatus"
              on-option-click="$ctrl.onClick(option)" menu-type="{{::$ctrl.menuType}}"
              close-first-level="$ctrl.closeFirstLevel()" first-level="true" selected="$ctrl.selected"></awe-option>
    </ul>`,
  bindings: {
    'id': '@menuId'
  },
  controller: ['$scope', '$element', 'Control', 'Component', '$document', '$window', 'AweUtilities', 'ActionController', '$ngRedux',
    function ($scope, $element, $control, $component, $document, $window, $utilities, $actionController, $ngRedux) {
      let $ctrl = this;
      let $body = $("body");
      $ctrl.module = null;
      $ctrl.options = [];
      $ctrl.selected = {};
      $ctrl.menuStatus = {minimized: false, animating: false, resolution: "desktop"};

      // Initialize as component
      $component.init($ctrl, $scope);

      // Define stateCheck function
      let stateCheck = ((state) => ({
        menu: state.menu,
        selected: state.router.currentParams.subScreenId || state.router.currentParams.screenId || null,
        module: state.components['module'] || {model: {values: []}},
        option: state.screen.report.option || null
      }));

      // Connect redux store updates
      $ctrl.events.push($ngRedux.connect(stateCheck)(onChangeMenu));

      /******************************************************************************
       * COMPONENT METHODS
       *****************************************************************************/

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function () {
        $ctrl.menuType = $ctrl.attributes.style && $ctrl.attributes.style.includes("horizontal") ? "horizontal" : "vertical";

        // Initialize menu
        $ngRedux.dispatch(updateMenu($ctrl.attributes.options, $ctrl.module));
        $ngRedux.dispatch(updateStatus($ctrl.menuStatus));

        // Call onResize on load
        onResize();
      };

      /**
       * Update component classes
       */
      $ctrl.updateClasses = function () {
      };

      /**
       * Act in case of option click
       */
      $ctrl.onClick = function (option) {
        switch ($ctrl.menuType) {
          case "horizontal":
            $ctrl.closeFirstLevel();
            break;

          case "vertical":
          default:
            switch ($ctrl.menuStatus.resolution) {
              case "mobile":
                if (!$ctrl.menuStatus.minimized) {
                  $ctrl.toggleMenu();
                }
                break;
              case "tablet":
              case "desktop":
              default:
                if ($ctrl.menuStatus.minimized) {
                  $ctrl.closeFirstLevel();
                }
                break;
            }
        }
      };

      /**
       * Check if option is opened
       */
      $ctrl.isVisible = function () {
        return $ctrl.attributes.visible && $ctrl.options.length > 0;
      };

      /**
       * Update component classes
       */
      $ctrl.filterOptions = filterOptions;

      /**
       * Clear first level opened options
       */
      $ctrl.closeFirstLevel = function () {
        $ngRedux.dispatch(updateOptions($ctrl.options.reduce((filtered, option) => option.opened ? [...filtered, option.name] : filtered, []), {opened: false}));
      };

      /**
       * Toggle menu to visible or not visible
       * @param {Action} action Action received
       */
      $ctrl.toggleMenu = function (action) {
        // Toggle visibility depending on menu type
        // Change menu visibility
        if ($ctrl.menuType === "horizontal") {
          $control.changeAttribute($ctrl.address, {visible: !$ctrl.attributes.visible});
          // Toggle mmc class to body
          $body.toggleClass("mmc", !$ctrl.attributes.visible);
        } else {
          // Close first level first (if resolution is tablet)
          let minimized = !$ctrl.menuStatus.minimized;
          let animating = true;
          $body.toggleClass("mmc", minimized);
          switch ($ctrl.menuStatus.resolution) {
            case "tablet":
            case "mobile":
              $body.toggleClass("mme", !minimized);
              break;
            default:
              $body.toggleClass("mme", false);
              break;
          }

          // Send status change
          $ngRedux.dispatch(updateStatus({minimized: minimized, animating: animating}));

          // Close first level if not mobile
          if ($ctrl.menuStatus.resolution !== "mobile") {
            $ctrl.closeFirstLevel();
          }
        }

        // Finish toggle action
        $utilities.timeout(function () {
          $ctrl.onEndToggleMenu(action);
        }, 250);
      };

      /**
       * Finish toggle menu visibility
       * @param {Action} action Action received
       */
      $ctrl.onEndToggleMenu = function (action) {
        $ngRedux.dispatch(updateStatus({
          animating: false,
          resizing: true
        }));

        // Publish layout event
        $utilities.publish("resize-action");

        // Finish screen action
        if (action) {
          $actionController.acceptAction(action);
        }
      };

      /**
       * Toggle navigation bar to visible or not visible
       * @param {Action} action Action received
       */
      $ctrl.toggleNavbar = function (action) {
        // main-navbar-collapse
        let $node = $('#main-navbar-collapse');
        if ($node.hasClass('collapse')) {
          $node.removeClass('collapse');
        } else {
          $node.addClass('collapse');
        }
        // Finish action
        $actionController.acceptAction(action);
      };

      /**
       * Change the menu
       * @param {Object} action Action parameters
       */
      $ctrl.changeMenu = function (action) {
        // Change menu attributes
        $ngRedux.dispatch(updateAllOptions(parameters.options));

        // Finish action
        $actionController.acceptAction(action);
      };

      /******************************************************************************
       * SERVICE METHODS
       *****************************************************************************/

      /**
       * Update state
       */
      function onChangeMenu(state) {
        $ctrl.options = state.menu.options;
        $ctrl.selected = state.selected;

        // If module has changed, update menu
        let module = state.module.model.values.filter(value => value.selected)
          .reduce((selected, value) => selected = selected || value.value, null);
        if (module != $ctrl.module) {
          $ctrl.module = module;
          $ngRedux.dispatch(updateMenu($ctrl.attributes.options, $ctrl.module));
        }

        // If status has changed, react to change
        if (!_.isEqual($ctrl.menuStatus, state.menu.status)) {
          $ctrl.menuStatus = state.menu.status;
        }
      }

      /**
       * Checks if dropdown is visible
       * @returns {Boolean} Dropdown is visible or not
       */
      function isDropdownVisible() {
        if ($ctrl.filterOptions($ctrl.options).filter(option => option.opened).length > 0) {
          return true;
        }
        return false;
      }

      /**
       * Calculate resolution
       */
      function getResolution() {
        let resolution = "desktop";

        // Calculate resolution
        if ($window.innerWidth > 640 && $window.innerWidth <= 768) {
          resolution = "tablet";
        } else if ($window.innerWidth <= 640) {
          // Mobile size
          resolution = "mobile";
        }
        return resolution;
      }

      /**
       * Finish toggle menu visibility
       */
      function onResize() {
        let resolution = getResolution();

        // Act depending on resolution
        if ($ctrl.menuType === "vertical") {
          if ($ctrl.menuStatus.minimized) {
            if (resolution != "mobile") {
              $ctrl.closeFirstLevel();
            }
          } else {
            if (resolution != "desktop") {
              $ctrl.toggleMenu();
            }
          }
        }

        if (resolution != $ctrl.menuStatus.resolution) {
          $ngRedux.dispatch(updateStatus({resolution: resolution}));
        }
      }

      // Watch click event to close menu if click is outside menu
      $document.bind('click', function (event) {
        if ($ctrl.menuType === "horizontal" ||
          ($ctrl.menuStatus.minimized && $ctrl.menuStatus.resolution !== "mobile")) {
          var isClickedElementChildOfDropdown = $element.find(event.target).length > 0;
          if (!isClickedElementChildOfDropdown && isDropdownVisible()) {
            $ctrl.closeFirstLevel();
          }
        }
      });

      /******************************************************************************
       * EVENT LISTENERS
       *****************************************************************************/
      _.each(ClientActions.menu, function (actionOptions, actionId) {
        $ctrl.events.push($scope.$on("/action/" + actionId, function (event, action) {
          return $ctrl[actionOptions.method](action);
        }));
      });

      $ctrl.events.push($scope.$on('resize', function () {
        if ($ctrl.menuStatus.resizing) {
          $ngRedux.dispatch(updateStatus({resizing: false}));
        } else {
          onResize();
        }
      }));

      /**
       * Manage disconnection
       */
      $ctrl.onDisconnect = function () {
        // Remove body classes
        $body.removeClass("mmc mme");
        $ngRedux.dispatch(clearMenu());
      };
    }
  ]
});
