import { aweApplication } from "../../awe";

import "../../services/maximize";

// Window component
aweApplication.component('aweWindow', {
  transclude: true,
  template:
  `<div class="panel panel-awe {{::$ctrl.panelClass}} panel-{{::$ctrl.attributes.size}} expandible-vertical"
     ng-class="{'maximized': $ctrl.maximized, 'maximizing': $ctrl.maximizing, 'resizing resizeTarget': $ctrl.panelResizing, 'expand': $ctrl.isExpanded || $ctrl.maximized}" ng-cloak>
     <div ng-show="::$ctrl.panelTitle" class="awe-panel-heading panel-heading" ng-cloak>
       <i ng-if="::$ctrl.panelIcon" class="panel-title-icon fa fa-{{::$ctrl.panelIcon}}"></i>
       <span translate-multiple="{{::$ctrl.panelTitle}}"></span>
       <div ng-if="::$ctrl.attributes.maximize" class="btn-group pull-right">
         <button role="button" type="button" class="maximize-button" aria-hidden="true" ng-click="$ctrl.togglePanel()"
           title="{{$ctrl.togglePanelText| translateMultiple}}">
           <i class="fa {{$ctrl.iconMaximized ? 'fa-compress' : 'fa-expand'}}"></i>
         </button>
       </div>
     </div>
     <div ng-transclude class="awe-panel-content panel-content maximize-content expandible-{{::$ctrl.expandDirection}}"
       ng-class="{'expand': $ctrl.isExpanded || $ctrl.maximized}" ng-cloak></div>
   </div>`,
  bindings: {
    'id': '@windowId'
  },
  controller: ['$scope', '$element', 'Component', 'Maximize',
    function ($scope, $element, $component, $maximize) {
      // Initialize controller
      let $ctrl = this;

      // Initialize as component
      $component.init($ctrl, $scope);

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function() {
        // Expand parent element if expanded style
        $element.addClass($ctrl.attributes.style);

        // Init as maximizable
        if ($ctrl.attributes.maximize) {
          $maximize.initMaximize($scope, $ctrl, $element);
        }

        // Window variables
        $ctrl.panelClass = $ctrl.attributes.style || "";
        $ctrl.panelTitle = $ctrl.attributes.label || null;
        $ctrl.panelIcon = $ctrl.attributes.icon || null;
      }
    }
  ]
});
