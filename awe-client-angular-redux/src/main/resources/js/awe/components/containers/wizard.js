import { aweApplication } from "../../awe";
import { ClientActions } from "../../actions/clientActions";
import _ from "lodash";

import "../../directives/containers/wizardpanel";

// Tab component
aweApplication.component('aweInputWizard', {
  transclude: true,
  template:
  `<div class="wizard wizard-{{::$ctrl.attributes.size}} expand expandible-vertical" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div class="{{::$ctrl.criterionClass}} expand expandible-vertical">
       <div class="wizard-wrapper">
         <ul class="wizard-steps" ng-class="{disabled:$ctrl.isDisabled()}">
           <li ng-class="{'active':$ctrl.model.selectedIndex === $index, 'completed':$ctrl.model.selectedIndex > $index}"
             ng-repeat="wizard in $ctrl.model.values track by wizard.value" ng-click="$ctrl.onClick(wizard.value)">
             <span class="wizard-step-number">{{$index + 1}}</span>
             <span class="wizard-step-caption">
               <span translate-multiple="{{::$ctrl.attributes.label}}"></span> {{$index + 1}}
               <span class="wizard-step-description" translate-multiple="{{::wizard.label}}"></span>
             </span>
           </li>
         </ul>
       </div>
       <div ng-transclude class="wizard-content wizard-panel wizard-panel-{{::$ctrl.attributes.size}} expand expandible-vertical"></div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputWizardId'
  },
  controller: ['$scope', '$element', 'Criterion', 'AweSettings', 'Control', 'AweUtilities', '$ngRedux',
    function ($scope, $element, $criterion, $settings, $control, $utilities, $ngRedux) {
      // Initialize controller
      let $ctrl = this;

      // Initialize as component
      $criterion.init($ctrl, $scope);

      // Define stateCheck function
      let stateCheck = (state) => ({disabled: state.actions.running});

      // Connect redux store updates
      $ctrl.events.push($ngRedux.connect(stateCheck)(onChangeDisabled));

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function() {
        // Expand element always
        $element.addClass("expand expandible-vertical");

        // Select first if there is none selected
        if ($ctrl.model.values.length > 0 &&
            $ctrl.model.values.filter((value) => value.selected).length === 0) {
          // Change model
          $ctrl.selectTab($ctrl.model.values[0].value);
        }
      };

      /**
       * Check if button is disabled
       */
      $ctrl.isDisabled = function() {
        return $ctrl.disabled || $ctrl.attributes.disabled;
      };

      /**
       * Event on click tab
       * @param {type} value
       */
       $ctrl.onClick = function (value) {
         // Return if disabled
         if (!$ctrl.isDisabled()) {
           $ctrl.selectTab(value);
         }
       };

      /**
       * Select a tab
       * @param {type} value
       */
      $ctrl.selectTab = function (value) {
        // Change model
        $control.changeModel($ctrl.address, {selected: value});
      };

      /**
       * Event on select tab
       */
       $ctrl.onSelectTab = function () {
        // Change attributes to all children components
        $control.changeAttributes($ctrl.model.values.map((value) => ({
          address: {
            ...$ctrl.address,
            component: value.value
          },
          data: {
            active: value.selected
          }
        })));

        // Launch resize action ???
        let resizeAction = () => $utilities.publishFromScope("resize-action", {}, $scope);
        $utilities.timeout(resizeAction);
        $utilities.timeout(resizeAction, 250);
       };

      /**
       * Update state
       */
      $ctrl.updateModel = function() {
        // Get value
        let newValue = $ctrl.getValue() || {};
        if (($ctrl.value || {}).value !== newValue.value) {
          $ctrl.onSelectTab();
        }
      };

      // Animation variables (retrieve from settings)
      let animationTime = 300;
      let useCSS3Animation = true;
      let minStepWidth = 200;

      /**
       * Disable global buttons
       * @param {Object} state Disabled state
       */
      function onChangeDisabled(state) {
        $ctrl.disabled = state.disabled;
      }

      /**
       * Resize wizard steps
       */
      function initSteps() {
        // Retrieve step container (once)
        if (!$ctrl.stepContainer) {
          $ctrl.stepContainer = $element.find('.wizard-steps');
        }
        // Retrieve steps (once)
        if (!$ctrl.steps) {
          $ctrl.steps = $ctrl.stepContainer.children();
        }
      }

      /**
       * Resize wizard steps
       */
      function resize() {
        // Retrieve wizard width
        let width = $element.width();

        // Check if width has changed
        if (width !== $ctrl.prevWidth) {
          // Initialize steps
          initSteps();

          // Calculate step width
          $ctrl.stepWidth = steps[0].offsetWidth;

          // Retrieve step width
          if ($ctrl.stepWidth <= minStepWidth) {
            $ctrl.moveSteps = true;
            let currentIndex = $ctrl.model.values.findIndex(value => value.selected);
            $ctrl.moveStepsTo(currentIndex);
          } else {
            $ctrl.moveSteps = false;
            $ctrl.stepContainer.animate({left: 0});
          }

          // Store previous width
          $ctrl.prevWidth = width;
        }
      }

      ////////////////////////////////////////////////////////////////////////////////////
      // Wizard Client Actions API
      ////////////////////////////////////////////////////////////////////////////////////

      /**
       * Move steps to the selected index
       * @param {int} position Move the steps to show the position
       */
      $ctrl.moveStepsTo = function (position) {
        // Initialize steps
        initSteps();

        // Retrieve wizard width
        let width = $element.width();

        // Retrieve step width
        let containerWidth = $ctrl.stepWidth * steps.length;
        let containerLeft = width / 2 - (($ctrl.stepWidth * position) + ($ctrl.stepWidth / 2));
        containerLeft = Math.max(width - containerWidth, Math.min(containerLeft, 0));
        let animation = {left: containerLeft};
        if (useCSS3Animation) {
          $utilities.animateCSS($ctrl.stepContainer, animation, animationTime);
        } else {
          $utilities.animateJavascript($ctrl.stepContainer, animation, animationTime);
        }
      };

      /**
       * Go to next panel
       */
      $ctrl.next = function () {
        let values = $ctrl.model.values;
        let currentIndex = values.findIndex((option) => option.selected);
        if (currentIndex + 1 < values.length) {
          $ctrl.selectTab(values[currentIndex + 1].value);
        }
      };
      /**
       * Go to previous panel
       */
      $ctrl.prev = function () {
        var nextTab = component.model.selectedIndex - 1;
        if (nextTab >= 0) {
          controller.selectTab(component.model.values[nextTab].value);
        }
      };

      // Define listeners
      _.each(ClientActions.wizard, function (actionOptions, actionId) {
        $ctrl.events.push($scope.$on("/action/" + actionId, function (event, action) {
          return $ctrl[actionOptions.method](action);
        }));
      });
    }
  ]
});