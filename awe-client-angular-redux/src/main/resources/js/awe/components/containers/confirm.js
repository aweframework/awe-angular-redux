import {aweApplication} from "../../awe";
import "../../directives/uiModal";

// Confirm directive
aweApplication.component('aweConfirm', {
  template:
    `<div class="modal modal-alert modal-warning fade" opened="$ctrl.opened" ui-modal on-close="$ctrl.onClose()" data-backdrop="static" ng-cloak>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <i class="fa fa-warning"></i>
          </div>
          <div class="modal-title" translate-multiple="{{$ctrl.confirmTitle}}"></div>
          <div class="modal-body" translate-multiple="{{$ctrl.confirmMessage}}"></div>
          <div class="modal-footer">
            <button id="confirm-cancel" type="button" class="btn btn-awe btn-default btn-{{::$ctrl.confirmSize}}" data-dismiss="modal" title="{{'BUTTON_CANCEL'| translateMultiple}}">
              <i class="fa fa-close"></i>
              <span translate-multiple="BUTTON_CANCEL"></span>
            </button>
            <button id="confirm-accept" type="button" class="btn btn-awe btn-primary btn-{{::$ctrl.confirmSize}}" data-dismiss="modal" ng-click="$ctrl.accept()" title="{{'BUTTON_ACCEPT'| translateMultiple}}">
              <i class="fa fa-check"></i>
              <span translate-multiple="BUTTON_ACCEPT"></span>
            </button>
          </div>
        </div>
      </div>
    </div>`,
  controller: ['$scope', '$element', 'Component', 'ActionController', 'AweUtilities', 'AweSettings', '$ngRedux',
    function ($scope, $element, $component, $actionController, $utilities, $settings, $ngRedux) {
      // Initialize controller
      let $ctrl = this;
      let stateCheck = (state) => ({confirm: state.messages.confirm});
      $ctrl.confirmSize = $settings.get("defaultComponentSize");
      $ctrl.accepted = false;
      $ctrl.opened = false;

      /**
       * Component initialization
       */
      $ctrl.$onInit = function() {
        // Connect redux store updates
        $ctrl.disconnect = $ngRedux.connect(stateCheck)(onUpdate);
      };

      /**
       * Accept action in confirm modal
       */
      $ctrl.accept = function () {
        $ctrl.accepted = true;
      };

      /**
       * On close action
       */
      $ctrl.onClose = function () {
        $ctrl.opened = false;
        // Launch close dialog delayed
        $utilities.timeout(function () {
          // Remove modal action stack
          $actionController.removeStack();
          // Close dialog action
          if ($ctrl.accepted) {
            $actionController.acceptAction($ctrl.confirmAction);
          } else {
            $actionController.rejectAction($ctrl.confirmAction);
          }
        }, 200);
      }

      /**
       * On data update
       * @param {Object} data Confirm message data
       */
      function onUpdate(data) {
        if (!data.confirm) return;

        $ctrl.confirmTitle = data.confirm.title;
        $ctrl.confirmMessage = data.confirm.message;
        $ctrl.confirmAction = data.confirm.action;
        $ctrl.opened = true;
        $ctrl.accepted = false;
      }
    }
  ]
});
