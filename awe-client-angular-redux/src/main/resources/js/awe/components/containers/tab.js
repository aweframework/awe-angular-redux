import { aweApplication } from "../../awe";

import "../../directives/containers/tabcontainer";

// Tab component
aweApplication.component('aweInputTab', {
  transclude: true,
  template:
  `<div class="tab-container tab-container-{{::$ctrl.attributes.size}} expand expandible-vertical"
     ng-class="{'maximized': $ctrl.maximized, 'maximizing': $ctrl.maximizing, 'resizing resizeTarget': $ctrl.panelResizing, 'expand': $ctrl.isExpandible || $ctrl.maximized}"
     ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div class="{{::$ctrl.criterionClass}} expand expandible-vertical">
       <ul class="nav nav-tabs" ui-tabdrop ng-class="{disabled: $ctrl.isDisabled()}">
         <li ng-class="{'active': tab.selected}" ng-repeat="tab in $ctrl.model.values" ng-attr-id="tab-{{::tab.value}}">
           <a ng-click="$ctrl.onClick(tab.value)">
             <i ng-if="::tab.icon" class="panel-title-icon fa fa-{{::tab.icon}}"></i>
             <span translate-multiple="{{::tab.label}}"></span>
           </a>
         </li>
         <li ng-if="::maximize" class="maximize-handler pull-right active">
           <a>
             <button role="button" type="button" class="maximize-button" aria-hidden="true" ng-click="$ctrl.togglePanel()" title="{{$ctrl.togglePanelText| translate}}">
               <i class="fa {{$ctrl.iconMaximized ? 'fa-compress' : 'fa-expand'}}"></i>
             </button>
           </a>
         </li>
       </ul>
       <div ng-transclude class="tab-content maximize-content expand expandible-vertical"></div>
     </div>
   </div>`,
  bindings: {
    'id': '@inputTabId'
  },
  controller: ['$scope', '$element', 'Criterion', 'Maximize', 'AweSettings', 'Control', 'AweUtilities', '$ngRedux',
    function ($scope, $element, $criterion, $maximize, $settings, $control, $utilities, $ngRedux) {
      // Initialize controller
      let $ctrl = this;

      // Initialize as component
      $criterion.init($ctrl, $scope);

      // Define stateCheck function
      let stateCheck = (state) => ({disabled: state.actions.running});

      // Connect redux store updates
      $ctrl.events.push($ngRedux.connect(stateCheck)(onChangeDisabled));

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function() {
        // Expand element always
        $element.addClass("expand expandible-vertical");

        // Init as maximizable
        if ($ctrl.attributes.maximize) {
          $maximize.initMaximize($scope, $ctrl, $element);
        }

        // Select first if there is none selected
        if ($ctrl.model.values.length > 0 && $ctrl.getValue() == null) {
          // Change model
          $ctrl.selectTab($ctrl.model.values[0].value);
          $control.keepModel($ctrl.address);
        }
      };

      /**
       * Check if button is disabled
       */
      $ctrl.isDisabled = function() {
        return $ctrl.disabled || $ctrl.attributes.disabled;
      };

      /**
       * Event on click tab
       * @param {type} value
       */
       $ctrl.onClick = function (value) {
         // Return if disabled
         if (!$ctrl.isDisabled()) {
           $ctrl.selectTab(value);
         }
       };

      /**
       * Select a tab
       * @param {type} value
       */
      $ctrl.selectTab = function (value) {
        // Change model
        $control.changeModel($ctrl.address, {selected: value});
      };


      /**
       * Event on select tab
       */
      $ctrl.onSelectTab = function () {
        // Change attributes to all children components
        $control.changeAttributes($ctrl.model.values.map((value) => ({
          address: {
            ...$ctrl.address,
            component: value.value
          },
          data: {
            active: value.selected
          }
        })));

        // Launch select tab action
        $utilities.publishDelayedFromScope("selectTab", {}, $scope);
      };

      /**
       * Update state
       */
      $ctrl.updateModel = function() {
        // Get value
        let newValue = $ctrl.getValue() || {};
        if (($ctrl.value || {}).value !== newValue.value) {
          $ctrl.onSelectTab();
        }
      };

      /**
       * Disable global buttons
       * @param {Object} state Disabled state
       */
      function onChangeDisabled(state) {
        $ctrl.disabled = state.disabled;
      }
    }
  ]
});
