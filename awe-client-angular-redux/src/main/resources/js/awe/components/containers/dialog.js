import {aweApplication} from "../../awe";
import "../../directives/uiModal";

// Dialog component
aweApplication.component('aweDialog', {
  transclude: true,
  template:
    `<div ng-attr-id="{{::$ctrl.id}}" ng-cloak>
       <div class="modal fade" ui-modal opened="$ctrl.attributes.opened" close="$ctrl.close()" on-open="$ctrl.onOpen()" on-close="$ctrl.onClose()" ng-cloak>
         <div class="modal-dialog {{::$ctrl.attributes.style}}"  ng-class="::{'fullHeight expandible-vertical': $ctrl.isExpanded}">
           <div class="modal-content" ng-class="::{'expand expandible-vertical': $ctrl.isExpanded}">
             <div ng-if="::$ctrl.attributes.label" class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">
                 <i ng-show="::$ctrl.attributes.icon" class="panel-title-icon fa fa-{{::$ctrl.attributes.icon}}"></i>
                 <span translate-multiple="{{::$ctrl.attributes.label}}"></span>
               </h4>
             </div>
             <div ng-class="::{'expand expandible-vertical': $ctrl.isExpanded}" ng-transclude></div>
           </div>
         </div>
       </div>
     </div>`,
  bindings: {
    'id': '@dialogId'
  },
  controller: ['$scope', '$element', 'Component', 'ActionController', 'AweUtilities', 'Control',
    function ($scope, $element, $component, $actionController, $utilities, $control) {
      // Initialize controller
      let $ctrl = this;

      // Initialize as component
      $component.init($ctrl, $scope);

      /**
       * On open action
       */
      $ctrl.onOpen = function () {
        // Add new modal action stack
        $actionController.addStack();

        // Launch action list
        // $actionController.addActionList([{type: 'resize', parameters: {delay: 200}}], true, {address: $ctrl.address});
      };

      /**
       * Close dialog
       */
      $ctrl.close = function () {
        // Close dialog
        $control.changeAttribute($ctrl.address, {opened: false});
      };

      /**
       * On close action
       */
      $ctrl.onClose = function () {
        // Launch close dialog delayed
        $utilities.timeout(function () {
          // Remove modal action stack
          $actionController.removeStack();
          // Close dialog action
          if ($ctrl.accept) {
            $actionController.acceptAction($ctrl.attributes.openAction);
          } else {
            $actionController.rejectAction($ctrl.attributes.openAction);
          }
          // Set default action on accept
          $ctrl.accept = $ctrl.attributes.acceptOnClose;
        }, 200);
      };
    }
  ]
});
