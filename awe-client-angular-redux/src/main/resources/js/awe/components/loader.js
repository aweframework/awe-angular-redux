import { aweApplication } from "../awe";

// Button component
aweApplication.component('aweLoader', {
  bindings: {
    iconLoader: '@'
  },
  controller: ['$scope', '$element', 'ServerData', '$compile', '$templateCache', 'AweUtilities',
    function ($scope, $element, $serverData, $compile, $templateCache, $utilities) {
      // Action Controller methods
      let TEMPLATE_LOADING = "--LOADING--";
      let $ctrl = this;

      /**
       * Initialization method
       */
      $ctrl.$onInit = function() {
        initLoader($ctrl.iconLoader);
      }

      /**
       * Compile the template
       * @param {type} template
       * @returns {undefined}
       */
      function compileTemplate(template) {
        // Compile the received data
        var newElement = $compile(template)($scope);
        // Which we can then append to our DOM element.
        $element.append(newElement);
      }

      /**
       * Compile the template
       * @param {type} template
       * @returns {undefined}
       */
      var endWatchTemplateLoaded;
      function onTemplateLoaded(event, template) {
        endWatchTemplateLoaded && endWatchTemplateLoaded();
        compileTemplate(template);
      }

      /**
       * Loader initialization
       * @param {String} iconLoader
       */
      function initLoader(iconLoader) {
        // Add action
        var templateName = `loader/${iconLoader || "google"}`;

        // Check parameters
        var template = $templateCache.get(templateName);
        if (!template) {
          $templateCache.put(templateName, TEMPLATE_LOADING);
          $serverData.preloadAngularTemplate({path: templateName}, function (data) {
            $utilities.publishDelayed(`template-${templateName}`, data);
            compileTemplate(data);
          });
        } else if (template === TEMPLATE_LOADING) {
          endWatchTemplateLoaded = $scope.$on(`template-${templateName}`, onTemplateLoaded);
        } else {
          compileTemplate(template);
        }
      }
    }
  ]
});
