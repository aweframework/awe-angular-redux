import { aweApplication } from "../../awe";

// Info component
aweApplication.component('aweInfo', {
  transclude: true,
  template:
  `<li title="{{$ctrl.infoTitle| translateMultiple}}" class="info navbar-form {{::$ctrl.infoStyle}}" ng-cloak ng-transclude></li>`,
  bindings: {
    'infoTitle': '@',
    'infoStyle': '@'
  }
});