import { aweApplication } from "../../awe";

import "../../services/button";

// Info dropdown component
aweApplication.component('aweInfoDropdown', {
  transclude: true,
  template:
  `<li ng-show="$ctrl.attributes.visible" ng-attr-id="{{::$ctrl.id}}" title="{{$ctrl.attributes.title| translateMultiple}}" class="info nav-icon-btn {{::$ctrl.attributes.style}}" ng-class="::{'dropdown': $ctrl.attributes.children}" ng-cloak>
     <a ng-click="$ctrl.onClick($event)" ng-class="::{'dropdown-toggle': $ctrl.attributes.children}" ng-attr-data-toggle="{{$ctrl.attributes.children ? 'dropdown' : ''}}">
       <i ng-if="::$ctrl.attributes.icon" class="nav-icon fa fa-{{::$ctrl.attributes.icon}} fa-fw" ng-class="::{'w-text': $ctrl.attributes.text}"></i>
       <span ng-if="$ctrl.attributes.unit" class="label" translate-multiple="{{$ctrl.attributes.unit}}"></span>
       <span ng-if="$ctrl.model.values[0].label" class="info-text" translate-multiple="{{$ctrl.model.values[0].label}}"></span>
       <span ng-if="$ctrl.attributes.text" class="info-text" translate-multiple="{{$ctrl.attributes.text}}"></span>
       <span ng-if="$ctrl.attributes.label" class="info-text" translate-multiple="{{$ctrl.attributes.label}}"></span>
       <span ng-if="$ctrl.attributes.title" class="small-screen-text" translate-multiple="{{$ctrl.attributes.title}}"></span>
     </a>
     <ul ng-click="$ctrl.onDropdownClick($event)" ng-if="::$ctrl.attributes.children" class="dropdown-menu {{::$ctrl.attributes.dropdownStyle}}" ng-transclude></ul>
   </li>`,
  bindings: {
    'id': '@infoDropdownId'
  },
  controller: ['Button', '$scope', 'AweUtilities', '$element',
    function ($button, $scope, $utilities, $element) {
      // Define controller
      let $ctrl = this;

      // Initialize as button
      $button.init($ctrl, $scope);

      // Cancel bubble on dropdown click
      $ctrl.onDropdownClick = function($event) {
        $utilities.stopPropagation($event);
      };

      // Hide children on dropdown hide
      $element.on('hidden.bs.dropdown', () => $scope.$broadcast('dropdown-hidden'));
    }
  ]
});