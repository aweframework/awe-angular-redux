import { aweApplication } from "../../awe";

// File viewer component
aweApplication.component('aweFileViewer', {
  template:
  `<div class="{{::$ctrl.attributes.style}}" ng-cloak>
     <pre>{{$ctrl.content}}</pre>
   </div>`,
  bindings: {
    'id': '@fileViewerId'
  },
  controller: ['Component', '$scope', '$element', 'AweUtilities', 'AweSettings', 'ServerData', 'Connection',
    function ($component, $scope, $element, $utilities, $settings, $serverData, $connection) {
      let $ctrl = this;

      // Initialize as component
      $component.init($ctrl, $scope);

      /**
       * On component initialization
       */
      $ctrl.onInitialize = function() {
        $ctrl.initial = true;
        $ctrl.stickBottom = $utilities.getFirstDefinedValue($ctrl.attributes.parameters.stickBottom, false);
      }

      /**
       * Reload data from element
       */
      $ctrl.filter = function() {
        var parameters = $serverData.getFormValues();
        var action = $ctrl.attributes[$settings.get("serverActionKey")];
        var target = $ctrl.attributes[$settings.get("targetActionKey")];
        var url = $serverData.getGenericFileUrl(action, target);
        $connection.post(url, parameters).then(manageResponse);
      };

      /**
       * Manage file retrieval
       * @param {Object} response Response
       */
      function manageResponse(response) {
        if (response.data && response.status === 201) {
          let top = $element[0].scrollHeight;
          var moveScroll = $element.scrollTop() + $element.height() >= top;
          $ctrl.content = response.data;
          stickToBottom(moveScroll, top);
        }
      }

      /**
       * Stick scroll to bottom
       * @param {boolean} moveScroll Move scroll or not
       * @param {integer} top Position to move
       */
      function stickToBottom(moveScroll, top) {
        if ($ctrl.stickBottom && (moveScroll || $ctrl.initial)) {
          $ctrl.initial = false;
          $utilities.timeout(() => $element.animate({scrollTop: top}));
        }
      }
    }
  ]
});