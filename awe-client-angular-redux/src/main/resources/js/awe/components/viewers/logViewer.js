import { aweApplication } from "../../awe";

// Log viewer directive
aweApplication.component('aweLogViewer', {
  template:
  `<div class="log-viewer {{::$ctrl.attributes.style}}" ng-cloak>
     <div class="content">
       <pre class="test-line">...</pre>
       <pre class="visible-text">{{$ctrl.content}}</pre>
     </div>
   </div>`,
  bindings: {
    'id': '@logViewerId'
  },
  controller: ['Component', '$scope', '$element', 'ServerData', 'AweUtilities', 'AweSettings', 'ActionController',
    function ($component, $scope, $element, $serverData, $utilities, $settings, $actionController) {
      let $ctrl = this;

      // Initialize as component
      $component.init($ctrl, $scope);

      /**
       * On component initialization
       */
      $ctrl.onInitialize = function() {
        $ctrl.initial = true;
        $ctrl.stickBottom = $utilities.getFirstDefinedValue($ctrl.attributes.parameters.stickBottom, false);
        $ctrl.allContent = [];

        $ctrl.lineHeight = $element.find(".test-line").height();
        $ctrl.contentLayer = $element.find(".content");
        $ctrl.positionLayer = $element.find(".visible-text");
        $ctrl.scrollPosition = null;
        $ctrl.lastScrollCheck = -1;
      }

      /**
       * Print text at scroll position
       */
      function printTextAtScrollPosition() {
        // If scroll has moved
        if ($ctrl.lastScrollCheck !== $ctrl.scrollPosition) {

          // Get positions
          $ctrl.scrollPosition = $element.scrollTop();
          $ctrl.lastScrollCheck = $ctrl.scrollPosition;
          var scrollTotal = $element[0].scrollHeight || 1;
          var containerHeight = $element.height();
          var scrollPercent = $ctrl.scrollPosition / scrollTotal;
          var linesToPrint = Math.ceil(containerHeight / $ctrl.lineHeight) + 60;
          var totalLines = $ctrl.allContent.length;
          var initialTextPosition = Math.max(0, Math.floor(totalLines * scrollPercent) - 30);
          var finalTextPosition = Math.min(totalLines, initialTextPosition + linesToPrint);

          // Get text layer position
          var textLayerPosition = initialTextPosition * $ctrl.lineHeight;

          // Move text container to visible part
          $ctrl.positionLayer[0].style.top = textLayerPosition + "px";

          // Fulfill with text
          $ctrl.content = $ctrl.allContent.slice(initialTextPosition, finalTextPosition).join("\n");
        }
      }

      /**
       * Print log delta
       */
      function printLogDelta(event, action) {
        var currentContent = $utilities.getFirstDefinedValue((action.parameters || {}).log, []);
        if (currentContent.length > 0) {
          // Add content to log
          $ctrl.allContent = $ctrl.allContent.concat(currentContent);

          // Check whether to move scroll
          let top = $element[0].scrollHeight;
          var moveScroll = $element.scrollTop() + $element.height() >= top;

          // Increase height
          var height = $ctrl.allContent.length * $ctrl.lineHeight;
          $ctrl.contentLayer.height(height);

          // Stick scroll to bottom
          stickToBottom(moveScroll, top);

          // Trigger scroll event
          $element.trigger("scroll");
        }

        /**
         * Stick scroll to bottom
         * @param {boolean} moveScroll Move scroll or not
         * @param {integer} top Position to move
         */
        function stickToBottom(moveScroll, top) {
          if ($ctrl.stickBottom && (moveScroll || $ctrl.initial)) {
            $ctrl.initial = false;
            $utilities.timeout(() => $element.animate({scrollTop: top}));
          }
        }

        // Accept action
        $actionController.acceptAction(action);
      }

      /**
       * Reload data from element
       */
      $ctrl.reload = function () {
        let isSilent = true;
        let isAsync = true;
        // Retrieve silent and async if arguments eq 2
        if (arguments.length === 2) {
          isAsync = arguments[0];
          isSilent = arguments[1];
        }

        // Add action to actions stack
        let values = {
          type: $ctrl.attributes[$settings.get("serverActionKey")],
          [$settings.get("targetActionKey")]: $ctrl.attributes[$settings.get("targetActionKey")],
          offset: $ctrl.allContent.length
        };

        // Generate server action
        let serverAction = $serverData.getServerAction($ctrl.address, values, isAsync, isSilent);

        // Send action list
        $actionController.addActionList([serverAction], false, {address: $ctrl.address});
      };

      // On scroll or resize, render scroll data
      let renderScroll = () => $utilities.debounce(printTextAtScrollPosition, 10);

      // On log delta, print log and scroll down
      $ctrl.events.push($scope.$on('/action/log-delta', printLogDelta));

      // Bind scroll event
      $element.on("scroll", renderScroll);

      // Bind resize event
      $ctrl.events.push($scope.$on("resize", renderScroll));
    }
  ]
});
