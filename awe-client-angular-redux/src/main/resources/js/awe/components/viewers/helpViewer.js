import { aweApplication } from "../../awe";

// Help viewer component
aweApplication.component('aweHelpViewer', {
  template:
  `<div class="help-viewer {{::$ctrl.attributes.style}}" ng-include="$ctrl.helpUrl" ng-cloak></div>`,
  bindings: {
    'id': '@helpViewerId'
  },
  controller: ['Component', '$scope', 'AweSettings', '$templateRequest', '$templateCache', 'ServerData', '$ngRedux',
    function ($component, $scope, $settings, $templateRequest, $templateCache, $serverData, $ngRedux) {
      let $ctrl = this;

      // Initialize as component
      $component.init($ctrl, $scope);

      // Define stateCheck function
      let stateCheck = (state) => ({ option: state.router.currentParams.subScreenId || state.router.currentParams.screenId || null});

      // Connect redux store updates
      $ctrl.events.push($ngRedux.connect(stateCheck)(onOptionChange));

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function() {
        // Get help URL
        let option = $ctrl.attributes[$settings.get('serverActionKey')] !== "application-help" ? $ctrl.option : null;
        $ctrl.helpUrl = $serverData.getHelpUrl(option);
      }

      /**
       * On option change
       */
       function onOptionChange(state) {
         // Model bindings
         Object.assign($ctrl, state);
       };
    }
  ]
});