import { aweApplication } from "../../awe";

// Route methods
import { routeMethods } from '../../actions/route';

// View controller
import '../../controllers/view';

/**
 * Manages the view load
 * @param {String} Controller name
 * @param {Function} Controller function
 */
aweApplication.component('baseViewComponent', {
  bindings: {
    view: '<'
  },
  templateUrl: routeMethods.templateBase,
  controller: 'ViewController as $view'
});