import {aweApplication} from "../awe";
import {ClientActions} from "../actions/clientActions";
import "angular-ui-grid";
import "../directives/uiGridSuperHeader";
import {ROW_IDENTIFIER, SMALL_GRID} from "../actions/components";
import _ from "lodash";

// Add requirements
aweApplication.requires.push.apply(aweApplication.requires,
  ["ui.grid", "ui.grid.resizeColumns", "ui.grid.pinning", "ui.grid.selection", "ui.grid.pagination", "ui.grid.moveColumns", "ui.grid.treeView"]);

// Decorator for avoiding mousewheel on viewport
aweApplication
  .config(["$provide", function ($provide) {
    $provide.decorator('Grid', ["$delegate", "$timeout", function ($delegate, $timeout) {
      $delegate.prototype.renderingComplete = function () {
        if (angular.isFunction(this.options.onRegisterApi)) {
          this.options.onRegisterApi(this.api);
        }
        this.api.core.raise.renderingComplete(this.api);
        $timeout(function () {
          let $viewport = $('.ui-grid-render-container');
          ['touchstart', 'touchmove', 'touchend', 'keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
            $viewport.unbind(eventName);
          });
        }.bind(this));
      };
      return $delegate;
    }]);
  }]);

// Grid component
aweApplication.component('aweGrid', {
  transclude: true,
  template:
    `<div ng-show="$ctrl.attributes.visible" class="grid expandible-vertical table-light {{::$ctrl.gridStyle}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-if="$ctrl.attributes.headerModel.length" ng-attr-id="grid-header-{{::$ctrl.id}}" ui-grid-super-header="$ctrl.headerOptions" ng-cloak></div>
     <div ng-attr-id="scope-{{::$ctrl.id}}" class="expand expandible-vertical grid-container" ng-cloak>
       <div ng-if="::$ctrl.editable" class="save-button">
         <button id="{{::$ctrl.id}}-grid-row-cancel" type="button" ng-class="::$ctrl.gridButtonClass" class="btn-awe btn-danger grid-row-cancel"
           title="{{'BUTTON_CANCEL'| translateMultiple}}" ng-click="$ctrl.onCancelRow()" ng-attr-tabindex="{{$ctrl.attributes.editing ? 0 : -1}}"
           ng-disabled="$ctrl.attributes.saving">
           <i class="fa" ng-class="{'fa-refresh fa-spin': $ctrl.rowAction === 'cancel' && $ctrl.savingRow, 'fa-close': $ctrl.rowAction !== 'cancel'}"></i>
           <span class="button-text" translate-multiple="BUTTON_CANCEL"></span>
         </button>
         <button id="{{::$ctrl.id}}-grid-row-save" type="button" ng-class="::$ctrl.gridButtonClass" class="btn-awe btn-primary grid-row-save"
           title="{{'BUTTON_SAVE_ROW'| translateMultiple}}" ng-click="$ctrl.onSaveRow()" ng-attr-tabindex="{{$ctrl.attributes.editing ? 0 : -1}}"
           ng-disabled="$ctrl.attributes.saving">
           <i class="fa" ng-class="{'fa-refresh fa-spin': $ctrl.rowAction === 'save' && $ctrl.attributes.saving, 'fa-save': $ctrl.rowAction !== 'save'}"></i>
           <span class="button-text" translate-multiple="BUTTON_SAVE_ROW"></span>
         </button>
       </div>
       <div ng-if="::($ctrl.gridOptions.definedAttributes && $ctrl.gridOptions.definedModel)" ng-attr-id="grid-{{::$ctrl.id}}" ui-grid="$ctrl.gridOptions"
         ui-grid-selection ui-grid-pagination ui-grid-resize-columns ui-grid-move-columns ui-grid-pinning class="awe-grid grid-node no-border-hr"></div>
     </div>
     <div class="table-footer" ng-cloak>
       <div class="pagination-content grid-buttons fixed-height {{$ctrl.footerButtonStyle}}">
         <div ng-transclude></div>
       </div>
       <div ng-if="::!$ctrl.attributes.paginationDisabled" class="pagination-content {{$ctrl.footerPaginationStyle}}">
         <div ng-if="$ctrl.gridOptions.totalItems > 0 && $ctrl.bigGrid">
           <div class="hidden-xs col-sm-8 text-center pagination-content">
             <ul uib-pagination ng-change="$ctrl.onSetPage($ctrl.gridOptions.paginationCurrentPage)" total-items="$ctrl.gridOptions.totalItems"
               items-per-page="$ctrl.gridOptions.paginationPageSize" ng-model="$ctrl.gridOptions.paginationCurrentPage" max-size="5"
               class="pagination pagination-{{::$ctrl.attributes.size}}" previous-text="{{'SCREEN_TEXT_GRID_PREVIOUS'| translateMultiple}}"
               next-text="{{'SCREEN_TEXT_GRID_NEXT'| translateMultiple}}" force-ellipses="true" boundary-link-numbers="true"></ul>
           </div>
           <div class="hidden-xs col-sm-4 text-right pagination-content">
             {{$ctrl.paginationText}}
           </div>
         </div>
         <div class="col-xs-12 pagination-content text-right" ng-if="$ctrl.gridOptions.totalItems > 0 && !$ctrl.bigGrid">
           <ul ng-class="::$ctrl.gridPaginationClass">
             <li ng-class="{disabled: $ctrl.gridOptions.paginationCurrentPage <= 1}">
               <a href="#" ng-click="$ctrl.onSetPage($ctrl.gridOptions.paginationCurrentPage - 1)" title="{{'SCREEN_TEXT_GRID_PREVIOUS'| translateMultiple}}"><i
               class="fa fa-arrow-circle-left"></i></a>
             </li>
           </ul>
           <div class="pagination-content pagination-number">{{$ctrl.paginationTextSmall}}</div>
           <ul ng-class="::$ctrl.gridPaginationClass">
             <li ng-class="{disabled: $ctrl.gridOptions.paginationCurrentPage >= $ctrl.gridOptions.totalItems}">
               <a href="#" ng-click="$ctrl.onSetPage($ctrl.gridOptions.paginationCurrentPage + 1)" title="{{'SCREEN_TEXT_GRID_NEXT'| translateMultiple}}"><i
               class="fa fa-arrow-circle-right"></i></a>
             </li>
           </ul>
         </div>
       </div>
     </div>
     <awe-loader class="grid-loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoader}}" ng-cloak></awe-loader>
   </div>`,
  bindings: {
    'id': '@gridId'
  },
  controller: ['$scope', 'Component', 'AweSettings', '$element', 'AweUtilities', 'Control', 'uiGridConstants', '$log', '$filter', '$translate', '$ngRedux', '$animate', 'rowSorter',
    function ($scope, $component, $settings, $element, $utilities, $control, $uiGridConstants, $log, $filter, $translate, $ngRedux, $animate, $rowSorter) {
      let $ctrl = this;
      $ctrl.element = $element;
      $ctrl.renderedRows = {};

      // Initialize as component
      $component.init($ctrl, $scope);

      // Define default measures
      $ctrl.measures = {width: undefined, height: undefined};

      // Define default constants
      $ctrl.constants = {ROW_IDENTIFIER: ROW_IDENTIFIER};

      let numericOptions = $settings.get("numericOptions");
      $ctrl.defaultOptions = {
        // Elements per page
        paginationPageSize: $settings.get("recordsPerPage"),
        paginationPageSizes: [],
        // Total width
        totalWidth: true,
        // Default formatting options
        formatOptions: {
          // Decimal separator
          decimalSeparator: numericOptions.decimalCharacter,
          // Thousands separator
          thousandsSeparator: numericOptions.digitGroupSeparator,
          // Decimal places
          decimalPlaces: numericOptions.decimalPlaces
        },
        enableRowSelection: true,
        enableFullRowSelection: true,
        enableRowHeaderSelection: false,
        enablePagination: true,
        enablePaginationControls: false,
        enableColumnResizing: true,
        enableHorizontalScrollbar: $uiGridConstants.scrollbars.WHEN_NEEDED,
        enableVerticalScrollbar: $uiGridConstants.scrollbars.WHEN_NEEDED,
        fastWatch: true,
        flatEntityAccess: true,
        enableSorting: true,
        enableColumnMoving: true,
        rowTemplate: "grid/row",
        paginationTemplate: "grid/pagination",
        enableMinHeightCheck: false,
        enableColumnMenus: false,
        virtualizationThreshold: 50,
        excessRows: 40,
        excessColumns: 5,
        scrollThreshold: 20,
        scrollDebounce: 70,
        node: $element,
        icons: {
          menu: '<i class="fa fa-bars"/>',
          filter: '<i class="fa fa-filter"/>',
          sortAscending: '<i class="fa fa-sort-alpha-asc"/>',
          sortDescending: '<i class="fa fa-sort-alpha-desc"/>'
        },
        onRegisterApi: function (gridApi) {
          $ctrl.api = gridApi;

          // Init grid
          $ctrl.initGrid();
        },
        getRowIdentity: (row) => row.entity[ROW_IDENTIFIER],
        customScroller: (uiGridViewport, scrollHandler) => $ctrl.customScroller(uiGridViewport, scrollHandler)
      };

      /**
       *  Initialize grid
       */
      $ctrl.initGrid = function () {
        // Broadcast rows rendered
        $ctrl.api.core.on.rowsRendered($scope, onRowsRenderedDebounced);

        // Manage on cell select events
        $ctrl.api.selection.on.rowSelectionChanged($scope, onSelectRow);
        $ctrl.api.selection.on.rowSelectionChangedBatch($scope, onSelectRow);
        $ctrl.api.colMovable.on.columnPositionChanged($scope, onChangeColumns);
        $ctrl.api.colResizable.on.columnSizeChanged($scope, onChangeColumns);
        $ctrl.api.core.on.filterChanged($scope, onFilterChanged);

        // Manage scroll events
        $ctrl.api.core.on.scrollBegin($scope, onScrollStart);
        $ctrl.api.core.on.scrollEnd($scope, onScrollEnd);

        // Manage sort
        $ctrl.api.core.on.sortChanged($scope, onSort);

        // Manage pagination
        $ctrl.api.pagination.on.paginationChanged($scope, onPagination);

        let getScreenUpdate = (state => ({visible: state.screen[$ctrl.address.view].visible}));
        let disconnectScreenUpdate = $ngRedux.connect(getScreenUpdate)((state) => {
          if (state.visible) {
            $utilities.executeWhenVisible($scope, $element.find(".grid-node")[0], () => {
              // Notify data change
              $ctrl.notifyGrid();

              // Resize
              $ctrl.onResize();
            });
            disconnectScreenUpdate();
          }
        });
        $ctrl.events.push(disconnectScreenUpdate);
      };

      /**
       *  Initialize static values
       */
      $ctrl.setStaticValues = function () {
        // Update grid styles
        $ctrl.gridStyle = `grid-${$ctrl.attributes.size} ${$ctrl.attributes.style || ""}`.trim();
        $ctrl.gridButtonClass = `btn btn-${$ctrl.attributes.size}`;
        $ctrl.gridPaginationClass = `pagination pagination-${$ctrl.attributes.size}`;

        // Expand parent element if expanded style
        if ($ctrl.isExpanded) {
          $element.addClass("expand expandible-vertical");
        }
      };

      /**
       *  Initialize static values
       */
      $ctrl.getLayers = function () {
        $ctrl.layers = {
          content: $element.find(".ui-grid-canvas"),
          wrapper: $element.find('.ui-grid-contents-wrapper'),
          header: $element.find('.ui-grid-header'),
          viewport: $element.find('.ui-grid-viewport'),
          save: $element.find('.save-button')
        };
        return Object.values($ctrl.layers).reduce((hasValues, value) => hasValues && (value.length > 0), true);
      };

      /**
       * Get row id
       * @param {type} row
       */
      $ctrl.getRowIdentity = (row) => row.entity.id;

      /**
       * Retrieve first record
       */
      $ctrl.getFirstRecord = () => $ctrl.gridOptions ? ($ctrl.gridOptions.paginationCurrentPage - 1) * $ctrl.gridOptions.paginationPageSize + 1 : 0;

      /**
       * Retrieve last record
       */
      $ctrl.getLastRecord = () => $ctrl.gridOptions ? Math.min($ctrl.gridOptions.paginationCurrentPage * $ctrl.gridOptions.paginationPageSize, $ctrl.gridOptions.totalItems) : 0;

      /**
       * Retrieve total pages
       */
      $ctrl.getTotalPages = () => $ctrl.api ?
        $ctrl.api.pagination.getTotalPages() :
        parseInt((($ctrl.gridOptions || {}).totalItems || $ctrl.model.records || 0) / ($ctrl.attributes.max || $ctrl.model.total), 10);

      /**
       * Retrieve row number
       * @param {type} row
       */
      $ctrl.getRowNumber = (row) => $ctrl.api.core.getVisibleRows().indexOf(row) + $ctrl.getFirstRecord();

      /**
       * Get cell visible value
       */
      $ctrl.getCellVisibleValue = (cell = {}) => {
        let cellValue = angular.isArray(cell) ? cell.filter(row => row.selected)[0] : cell;
        return (cellValue || {}).label || (cellValue || {}).value || cellValue;
      };

      /**
       * Get cell value
       */
      $ctrl.getCellValue = (cell = {}) => {
        let cellValue = angular.isArray(cell) ? cell.filter(row => row.selected)[0] : cell;
        return (cellValue || {}).value || cellValue;
      };

      /**
       * Get cell style
       */
      $ctrl.getCellStyle = (cell = {}) => (cell || {}).style || null;

      /**
       * Resize the grid
       */
      $ctrl.onResize = () => {
        if ($ctrl.onGridSizeChange()) {
          // Manage api resize
          $ctrl.bigGrid = $ctrl.measures.width > SMALL_GRID;

          // Update footer
          updateFooter($ctrl);

          // Handle grid
          if ($ctrl.api) {
            $ctrl.api.core.handleWindowResize();
            $ctrl.api.core.queueGridRefresh();
          }
        }
      };

      /**
       * Generate a sorting algorithm
       * @param {object|string|number|boolean} a
       * @param {object|string|number|boolean} b
       * @returns {function|number}
       */
      $ctrl.sortingAlgorithm = function (a, b) {
        return $rowSorter.basicSort($ctrl.getCellValue(a), $ctrl.getCellValue(b));
      };

      /**
       * Manage the grid scroll
       * @param uiGridViewport
       * @param scrollHandler
       * @returns {*}
       */
      $ctrl.customScroller = function (uiGridViewport, scrollHandler) {
        uiGridViewport.on('scroll', (event) => {
          repositionSaveButton();
          scrollHandler(event);
        });
      };

      $ctrl.getDefaultColumnValues = function () {
        return {
          displayName: "",
          pinnedLeft: $ctrl.hasFrozen,
          enableColumnResizing: false,
          enableColumnMoving: false,
          enableColumnMenu: false,
          enableFiltering: false,
          enableSorting: false
        };
      };

      /**
       * Add initial columns
       */
      $ctrl.initialColumns = function () {
        let rowNumberColumnSize = Math.max((String($ctrl.getLastRecord()).length + 1) * $ctrl.attributes.charSize, 30);

        return [{
          ...$ctrl.getDefaultColumnValues(),
          minWidth: rowNumberColumnSize,
          width: rowNumberColumnSize,
          name: "_RowNum_",
          headerCellTemplate: "grid/header",
          cellTemplate: "grid/cellRowNumber",
          visible: $ctrl.attributes.rownumbers
        }, {
          ...$ctrl.getDefaultColumnValues(),
          minWidth: 26,
          width: 26,
          name: "_ChkBox_",
          headerCellTemplate: "grid/headerCheckbox",
          cellTemplate: "grid/cellCheckbox",
          visible: $ctrl.attributes.multiselect
        }];
      };

      /**
       * Add final columns
       */
      $ctrl.finalColumns = function () {
        return [{
          ...$ctrl.getDefaultColumnValues(),
          minWidth: 24,
          width: 24,
          name: "RowTyp",
          cellTemplate: getColumnTemplate({component: "icon", name: "RowTyp"}),
          cellClass: () => "multioperation-icon text-center",
          visible: $ctrl.attributes.multioperation
        }];
      };

      /**
       * Get column model
       */
      $ctrl.getColumnModel = function () {
        return [
          ...$ctrl.initialColumns(),
          ...$ctrl.attributes.columnModel.map(column => ({
            ...column,
            width: column.charlength * $ctrl.attributes.charSize || column.width || 20 * $ctrl.attributes.charSize,
            visible: !column.hidden,
            displayName: $utilities.getFirstDefinedValue(column.label, ""),
            headerClass: $utilities.getFirstDefinedValue(column.style, ""),
            name: $utilities.getFirstDefinedValue(column.name, column.id),
            id: column.name,
            sortField: $utilities.getFirstDefinedValue(column.sortField, column.name, column.id),
            field: $utilities.getFirstDefinedValue(column.name, column.id),
            pinnedLeft: column.frozen,
            cellClass: (grid, row, col) => {
              return [`text-${col.colDef.align}`, col.colDef.style || '', $ctrl.getCellStyle(row.entity[col.field]) || ''].join(" ").trim();
            },

            // Column templates
            headerCellTemplate: column.headerCellTemplate || "grid/header",
            cellTemplate: getColumnTemplate(column),
            footerCellTemplate: getColumnFooterTemplate(column),
            filterHeaderTemplate: column.filterHeaderTemplate || "grid/headerFilter",

            // Sorting algorithm
            sortingAlgorithm: $ctrl.sortingAlgorithm
          })),
          ...$ctrl.finalColumns()
        ];
      };

      /**
       * Retrieve header model
       */
      $ctrl.getHeaderModel = function () {
        !$ctrl.layers && $ctrl.getLayers();
        let columns = $ctrl.api.grid.columns;
        let headers;

        // Calculate headers
        if (!$ctrl.headerOptions) {
          headers = getHeaders(columns, $ctrl.attributes.headerModel);
        } else {
          headers = $ctrl.headerOptions.headers;
        }

        // Retrieve columns and headers
        return {
          columns: columns,
          headers: headers,
          layers: $ctrl.layers
        };
      };

      /**
       * Update model
       */
      $ctrl.updateModel = function () {
        $ctrl.gridOptions = "gridOptions" in $ctrl ? $ctrl.gridOptions : {...$ctrl.defaultOptions};
        Object.assign($ctrl.gridOptions, {
          // Grid model change
          data: $ctrl.model.values || [],
          paginationCurrentPage: $ctrl.model.page || 1,
          totalItems: $ctrl.model.records || $ctrl.model.values.length || 0,
          definedModel: true
        });

        // Notify data change
        $ctrl.notifyGrid();
      };

      /**
       * Update attributes
       */
      $ctrl.updateAttributes = function () {
        // Define grid options when state loaded
        $ctrl.gridOptions = "gridOptions" in $ctrl ? $ctrl.gridOptions : {...$ctrl.defaultOptions};

        // Define frozen variable
        $ctrl.hasFrozen = $ctrl.attributes.columnModel.filter(column => column.frozen).length > 0;

        // Define editable grid
        $ctrl.editable = $ctrl.attributes.editable || $ctrl.attributes.multioperation;

        // Assign changes to grid
        Object.assign($ctrl.gridOptions, {
          // Grid options change
          columnDefs: $ctrl.getColumnModel(),
          paginationPageSize: $ctrl.attributes.max || $ctrl.gridOptions.paginationPageSize,
          multiSelect: $ctrl.attributes.multiselect,
          noUnselect: $ctrl.attributes.editable && !$ctrl.attributes.multiselect,
          enableFiltering: $ctrl.attributes.enableFilters,
          useExternalSorting: !$ctrl.attributes.loadAll,
          useExternalPagination: !$ctrl.attributes.loadAll,
          rowHeight: parseInt($ctrl.attributes.rowHeight, 10) || 28,
          showColumnFooter: $ctrl.attributes.showTotals,
          definedAttributes: true
        });

        // Notify data change
        $ctrl.notifyGrid();
      };

      /**
       * Notify the grid data has changed
       */
      $ctrl.notifyGrid = function () {
        if ($ctrl.api) {
          // Notify grid changes
          $ctrl.api.core.notifyDataChange($uiGridConstants.dataChange.ALL);
          $ctrl.api.core.refresh();

          // Update header options
          $ctrl.notifyColumnChange();

          // If editing, reposition button
          $ctrl.editing && repositionSaveButton();
        }
      };

      /**
       * Notify the grid columns have changed
       */
      $ctrl.notifyColumnChange = function () {
        if ($ctrl.api) {
          // Update header options
          $ctrl.headerOptions = $ctrl.getHeaderModel();

          // Notify columns changed
          $utilities.publishFromScope("columns-changed", {}, $scope);
        }
      };

      /**
       *  On Select All rows
       */
      $ctrl.onSelectAllClick = function () {
        if ($ctrl.api.grid.selection.selectAll) {
          $ctrl.api.grid.api.selection.selectAllRows();
          $control.changeModel($ctrl.address, {selected: $ctrl.api.selection.getSelectedRows().map(row => row[ROW_IDENTIFIER])});
        } else {
          // Clear selected rows
          $ctrl.api.grid.api.selection.clearSelectedRows();
          $control.changeModel($ctrl.address, {selected: []});
        }
      };

      /**
       * Retrieve pagination text
       */
      $ctrl.onPaginationTextChanged = function (value) {
        if (value) {
          // Update total pages
          updatePaginationText($ctrl);
        }
      };

      /**
       * On grid size change
       */
      $ctrl.onGridSizeChange = function () {
        if ($utilities.isVisible($element[0])) {
          $ctrl.measures = getGridSize();
          return true;
        } else {
          return false;
        }
      };

      /**
       * On grid set page
       * @param page New page
       */
      $ctrl.onSetPage = function (page) {
        $ctrl.gridOptions.paginationCurrentPage = page;
        if ($ctrl.api) {
          $ctrl.api.pagination.seek(page);
        }
      };

      /**
       * Check if grid is visible
       */
      $ctrl.isGridVisible = function () {
        return $utilities.isVisible($element[0]);
      };

      /**
       * On save row event
       */
      $ctrl.onSaveRow = function () {
        $ctrl.editing = false;

        // Flush debounced model first
        $control.flushDebouncedModel();

        // Send save row event
        $control.changeModel($ctrl.address, {event: "save-row"});

        // Hide save button
        repositionSaveButton();

        // Clear selected rows
        $control.changeModel($ctrl.address, {selected: [], event: "after-save-row"});
      };

      /**
       * On cancel row event
       */
      $ctrl.onCancelRow = function () {
        $ctrl.editing = false;

        // Flush debounced model first
        $control.flushDebouncedModel();

        // Hide save button
        repositionSaveButton();

        // Get selected rows
        let [selectedRow] = $ctrl.api.selection.getSelectedRows();
        $control.restoreRowModel({...$ctrl.address, row: selectedRow[ROW_IDENTIFIER]});
      };

      /**
       * Select first row of the grid
       */
      $ctrl.onSelectFirstRow = function () {
      };

      /**
       * Select last row of the grid
       */
      $ctrl.onSelectLastRow = function () {
      };

      /**
       * Select all rows of the grid
       */
      $ctrl.onSelectAllRows = function () {
        $ctrl.api.grid.selection.selectAll = true;
        $ctrl.onSelectAllClick();
      };

      /**
       * Unselect all rows of the grid
       */
      $ctrl.onUnselectAllRows = function () {
        $ctrl.api.grid.selection.selectAll = false;
        $ctrl.onSelectAllClick();
      };

      /**
       * Check that only one row is selected
       */
      $ctrl.onCheckOneRowSelected = function () {
      };

      /**
       * Check that there's at least one row selected
       */
      $ctrl.onCheckSomeRowSelected = function () {
      };

      /**
       * Check that grid has at least one row
       */
      $ctrl.onCheckRecordsGenerated = function () {
      };

      /**
       * Delete selected row
       */
      $ctrl.onDeleteRow = function () {
      };

      /**
       *
       */
      $ctrl.onAfterDeleteRow = function () {
      };

      /**
       * Add a row
       */
      $ctrl.onAddRow = function () {
      };

      /**
       * Add a row on top
       */
      $ctrl.onAddRowTop = function () {
      };

      /**
       * Add a row after selected row
       */
      $ctrl.onAddRowAfterSelected = function () {
      };

      /**
       * Add a row before selected row
       */
      $ctrl.onAddRowBeforeSelected = function () {
      };

      /**
       *
       */
      $ctrl.onAfterAddRow = function () {
      };

      /**
       * Update a row
       */
      $ctrl.onUpdateRow = function () {
      };

      /**
       * Copy a row
       */
      $ctrl.onCopyRow = function () {
      };

      /**
       * Copy a row on top
       */
      $ctrl.onCopyRowTop = function () {
      };

      /**
       * Copy a row after selected row
       */
      $ctrl.onCopyRowAfterSelected = function () {
      };

      /**
       * Copy a row before selected row
       */
      $ctrl.onCopyRowBeforeSelected = function () {
      };

      /**
       * Copy selected rows to clipboard
       */
      $ctrl.onCopySelectedRowsToClipboard = function () {
      };

      /**
       * Add columns
       */
      $ctrl.onAddColumns = function () {
      };

      /**
       * Replace columns
       */
      $ctrl.onReplaceColumns = function () {
      };

      /**
       * Update a cell value
       */
      $ctrl.onUpdateCell = function () {
      };

      /**
       * Show columns
       * @param {object} parameters Action parameters
       */
      $ctrl.onShowColumns = function (parameters) {
        setColumnAttribute(parameters.columns, "hidden", false);
      };

      /**
       * Hide columns
       * @param {object} parameters Action parameters
       */
      $ctrl.onHideColumns = function (parameters) {
        setColumnAttribute(parameters.columns, "hidden", true);
      };

      /**
       * Toggle columns visibility
       * @param {object} parameters Action parameters
       */
      $ctrl.onToggleColumnsVisibility = function (parameters) {
        setColumnAttribute(parameters.columns, "hidden", !parameters.show);
      };

      /**
       * Change columns label
       * @param {object} parameters Action parameters
       */
      $ctrl.onChangeColumnLabel = function (parameters) {
        setColumnAttribute(parameters.columns, "label", parameters.label);
      };

      // Define client actions for each grid
      $utilities.defineActionListeners($ctrl.events, ClientActions.grid.commons, $scope, $ctrl);

      ////////////////////////////////////
      // PRIVATE METHODS
      ////////////////////////////////////

      /**
       * Set column attribute
       * @param {string} columns Column list as comma-separated string list
       * @param {string} attribute Attribute name
       * @param {*} value Value to assign
       */
      function setColumnAttribute(columns, attribute, value) {
        let columnList = columns.split(",").map(column => column.trim());
        let changed = false;

        // Update column model attribute
        let columnModel = $ctrl.attributes.columnModel.reduce((columns, column) => {
          let newValue = columnList.includes(column.name) ? value : column[attribute];
          changed = changed || newValue !== column[attribute];
          return [
            ...columns,
            {...column, [attribute]: newValue}
          ];
        }, []);

        // Change column model deferred
        changed && $control.changeAttributesDeferred($ctrl.address, {columnModel: columnModel});
      }

      /**
       * Get super headers
       * @param {object} columns Column
       * @param {object} headerModel Header model
       * @returns {object} Super header structure
       */
      function getHeaders(columns, headerModel) {
        let currentHeaderColumn = 0;
        let currentHeader = null;
        return columns.reduce((calculatedHeaders, column) => {
          if (currentHeaderColumn > 0) {
            currentHeaderColumn--;
          } else {
            let header = getHeader(headerModel, column.name);
            if (header) {
              currentHeader = header;
              currentHeaderColumn = header.numberOfColumns - 1;
            } else {
              currentHeader = null;
            }
          }

          // Stored each header for each column
          if (currentHeader !== null) {
            calculatedHeaders[column.name] = currentHeader;
          }
          return calculatedHeaders;
        }, {});
      }

      /**
       * Get column header
       * @param headers Headers
       * @param columnName Column name
       * @returns {object|boolean} Found header or false
       */
      function getHeader(headers, columnName) {
        return headers.reduce((found, header) => found || (header.startColumnName === columnName) ? header : found, false);
      }

      /**
       * Retrieve component-based column template
       * @param {object} column Column
       * @returns {string} Column template
       */
      function getColumnTemplate(column) {
        if (column.component) {
          return `<div class="ui-grid-cell-contents component" column-id="${column.name}">
                    <awe-column-${column.component}/>
                  </div>`;
        } else {
          return column.cellTemplate || "grid/cell";
        }
      }

      /**
       * Retrieve component-based column footer template
       * @param {object} column Column
       * @returns {string} Column template
       */
      function getColumnFooterTemplate(column) {
        if (column.component && column.summaryType) {
          return `<div class="ui-grid-cell-contents ui-grid-cell-footer" column-id="${column.name}">
                    <awe-column-${column.component}/>
                  </div>`;
        } else {
          return column.footerCellTemplate || "grid/footer";
        }
      }

      // Avoid multiple rows rendered event
      let debouncedRowsRendered = $utilities.debounce(onRowsRendered, 100);

      /**
       * On rows rendered event
       */
      function onRowsRenderedDebounced() {
        if ($ctrl.gridOptions.definedAttributes && $ctrl.gridOptions.definedModel) {
          // Call debounced
          debouncedRowsRendered();
        }
      }

      /**
       * On rows rendered event
       */
      function onRowsRendered() {
        $log.info("Rows rendered");

        // Mark rows as selected
        markSelectedRows();

        // Update save button
        repositionSaveButton();

        let visibleRowsId = $ctrl.api.core.getVisibleRows()
          .filter(row => !(row.entity[ROW_IDENTIFIER] in $ctrl.renderedRows))
          .map(row => row.entity[ROW_IDENTIFIER]);

        // Generate cell components
        if (visibleRowsId.length > 0) {
          $control.generateCellComponents($ctrl.address, visibleRowsId);
          visibleRowsId.forEach(id => $ctrl.renderedRows[id] = true);
        }

        // Mark loading as false (if still true)
        if ($ctrl.attributes.loadAll && $ctrl.attributes.loading) {
          $control.changeAttribute($ctrl.address, {loading: false});
        }
      }

      /**
       * On Select Row
       * @param {object} row Selected row
       * @param {event} event Event
       */
      function onSelectRow(row, event) {
        // Stop event
        event && $utilities.stopPropagation(event, true);

        // Flush debounced model
        $control.flushDebouncedModel();

        // Apply event if row is a selected row
        if (!angular.isArray(row)) {
          // Get selected rows
          let selectedRows = $ctrl.api.selection.getSelectedRows();

          // Mark select all selection (if all rows have been selected)
          $ctrl.api.grid.selection.selectAll = selectedRows.length === $ctrl.model.values.length;

          // Update redux model
          $control.changeModel($ctrl.address, {
            event: "select-row",
            selected: selectedRows.map(value => value[ROW_IDENTIFIER])
          });

          // Edit row if editable && selectedRows.length is 1
          markEditingRow(selectedRows);
        }
      }

      /**
       * Set row as editing or not
       * @param {array} selectedRows Selected rows
       */
      function markEditingRow(selectedRows) {
        if ($ctrl.editable) {
          if (selectedRows.length === 1) {
            let [row] = selectedRows;
            onEditRow(row[ROW_IDENTIFIER]);
          } else {
            onEndEditingRow();
          }
        }
      }

      /**
       * On edit row
       * @param {string} rowId Selected row id
       */
      function onEditRow(rowId) {
        // Set editing
        $ctrl.editing = true;

        // Keep row model
        $control.keepRowModel({...$ctrl.address, row: rowId});

        // Reposition save button
        repositionSaveButton();
      }

      /**
       * On end editing row
       */
      function onEndEditingRow() {
        // Set editing
        $ctrl.editing = false;
      }

      /**
       * Mark selected rows
       */
      function markSelectedRows() {
        // Get selected rows
        let changed = false;
        let selectedRows = $ctrl.model.values
          .filter(row => row.selected)
          .map(value => value[ROW_IDENTIFIER]);
        // Mark selected rows
        $ctrl.api.grid.rows.forEach(row => {
          let selectRow = selectedRows.includes(row.entity[ROW_IDENTIFIER]);
          changed = changed || selectRow !== row.isSelected;
          row.isSelected = selectRow;
        });

        // Mark editing row
        changed && markEditingRow(selectedRows);
      }

      /**
       * Update pagination text
       * @param {Object} $ctrl Controller
       */
      function updatePaginationText($ctrl) {
        let records = $ctrl.gridOptions.totalItems;
        let total = $ctrl.getTotalPages();
        let page = $ctrl.gridOptions.paginationCurrentPage;
        const VIEW = $translate.instant('SCREEN_TEXT_GRID_VIEW');
        const OF = $translate.instant('SCREEN_TEXT_GRID_OF');

        // Generate pagination text
        $ctrl.paginationText = `${VIEW} ${$filter('number')($ctrl.getFirstRecord(), 0)} - ${$filter('number')($ctrl.getLastRecord(), 0)} ${OF} ${$filter('number')(records, 0)}`;
        $ctrl.paginationTextSmall = `${$filter('number')(page, 0)} / ${$filter('number')(total, 0)}`;
      }

      /**
       * Update footer
       * @param {Object} $ctrl Controller
       */
      function updateFooter($ctrl) {
        // Generate pagination sizes
        if ($ctrl.attributes.paginationDisabled) {
          if ($ctrl.attributes.buttonModel.length > 0) {
            $ctrl.footerButtonStyle = "col-xs-12";
          } else {
            $ctrl.footerButtonStyle = "hidden";
          }
        } else {
          if ($ctrl.bigGrid) {
            $ctrl.footerButtonStyle = "col-xs-3";
            $ctrl.footerPaginationStyle = "col-xs-9";
          } else {
            if ($ctrl.attributes.buttonModel.length > 0) {
              $ctrl.footerButtonStyle = "col-xs-4";
              $ctrl.footerPaginationStyle = "col-xs-8";
            } else {
              $ctrl.footerButtonStyle = "hidden";
              $ctrl.footerPaginationStyle = "col-xs-12";
            }
          }
        }
      }

      /**
       *  On Change columns
       */
      function onChangeColumns() {
        $ctrl.onResize();
        $ctrl.notifyGrid();
      }

      /**
       *  On Filter changed
       */
      function onFilterChanged() {
        $control.changeAttribute($ctrl.address, {loading: true});
      }

      /**
       *  On scroll start
       */
      function onScrollStart() {
        if (!$ctrl.layers && !$ctrl.getLayers()) return;
        // Hide context menu if showing
        // $ctrl.hideContextMenu();
        // Bind clickout scroll
        $ctrl.previousScroll = Math.abs($ctrl.layers.viewport[0].scrollTop);
        $ctrl.layers.viewport.on("scroll", onChangeLayout);
        $animate.enabled(false);
      }

      /**
       *  On scroll end
       */
      function onScrollEnd() {
        if (!$ctrl.layers && !$ctrl.getLayers()) return;

        // Update save button
        repositionSaveButton();

        // Bind clickout scroll
        $ctrl.layers.viewport.off("scroll", onChangeLayout);
        $animate.enabled(true);
      }

      /**
       * Change layout
       */
      function onChangeLayout() {
        // Don't remove. Makes a fluid scroll
        $scope.$apply();

        // Update save button
        //repositionSaveButton();
      }

      /**
       *  On sort
       */
      function onSort(grid, sortColumns) {
        $control.changeAttribute($ctrl.address, {
          specificParameters: {
            ...$ctrl.attributes.specificParameters || {},
            sort: sortColumns.map(column => ({id: column.colDef.sortField, direction: column.sort.direction}))
          },
          loading: true
        });

        // Reload grid if load-all is false
        if (!$ctrl.attributes.loadAll) {
          $ctrl.reload(false, true);
        }
      }

      /**
       *  On pagination
       */
      function onPagination(page, pageSize) {
        $control.changeAttribute($ctrl.address, {
          specificParameters: {
            ...$ctrl.attributes.specificParameters || {},
            page: page,
            max: $ctrl.attributes.loadAll ? 0 : pageSize
          },
          loading: true
        });

        // Reload grid if load-all is false
        if (!$ctrl.attributes.loadAll) {
          $ctrl.reload(false, true);
        } else {
          $control.changeModel($ctrl.address, {page: page});
        }
      }

      /**
       * Calculate grid sizes
       * @returns {object} Grid width and height in px
       */
      function getGridSize() {
        return {
          width: $element[0].offsetWidth,
          height: $element[0].offsetHeight
        };
      }

      /**
       * Reposition the save button
       */
      function repositionSaveButton() {
        // If not editing and not showing, return
        if (!$ctrl.editing && !$ctrl.saveButtonShowing) return;

        // Initialize layers if not initialized
        if (!$ctrl.layers || !$ctrl.layers.save.length) {
          $ctrl.getLayers();
        }
        if ($ctrl.layers && $ctrl.layers.save.length) {
          // Define initial button position
          let buttonPosition = {
            top: -10000,
            left: -10000
          };
          $ctrl.saveButtonShowing = $ctrl.editing;
          if ($ctrl.editing) {
            // Calculate button position if editing
            buttonPosition = getButtonPosition();
          }

          // Set button position
          if (!_.isEqual(buttonPosition, $ctrl.buttonPosition)) {
            $ctrl.layers.save.css(buttonPosition);
            $ctrl.buttonPosition = buttonPosition;
          }
        }
      }

      /**
       * Get button position
       * @returns {object} button position
       */
      function getButtonPosition() {
        let buttonPosition = {
          top: -10000,
          left: -10000
        };
        // Find selected row
        let selectedRows = $ctrl.api.selection.getSelectedRows();
        if (selectedRows.length > 0) {
          let [firstRow] = selectedRows;
          let selectedRow = $ctrl.layers.content.find(`.ui-grid-row[id='${firstRow[ROW_IDENTIFIER]}']`).last();
          if (selectedRow.length > 0) {
            let headerHeight = $ctrl.layers.header.outerHeight(true);
            let viewport = $ctrl.layers.viewport;
            let buttonWidth = $ctrl.layers.save.outerWidth(true);
            let viewportMeasures = viewport.first().offset();
            let rowMeasures = selectedRow.offset();
            rowMeasures.height = selectedRow.outerHeight(true);
            rowMeasures.width = selectedRow.outerWidth();
            rowMeasures.left -= selectedRow.outerWidth(true) - rowMeasures.width;
            viewportMeasures.height = viewport.height();
            viewportMeasures.width = viewport.last().width();
            viewportMeasures.scrollLeft = viewport.last().scrollLeft();
            // Calculate new button position
            buttonPosition.left = (rowMeasures.left + viewportMeasures.scrollLeft + Math.min(rowMeasures.width, viewportMeasures.width) - buttonWidth) - viewportMeasures.left;
            buttonPosition.top = (rowMeasures.top + rowMeasures.height + headerHeight) - viewportMeasures.top;
            // If top position is offbound, move out
            buttonPosition.top = buttonPosition.top < headerHeight || buttonPosition.top > headerHeight + viewportMeasures.height + 2 ? -10000 : buttonPosition.top;
          }
        }

        // Return button position
        return buttonPosition;
      }

      // Watch pagination text
      $scope.$watch("$ctrl.gridOptions.totalItems + $ctrl.gridOptions.paginationCurrentPage + $ctrl.getTotalPages()", $ctrl.onPaginationTextChanged);
    }
  ]
});
