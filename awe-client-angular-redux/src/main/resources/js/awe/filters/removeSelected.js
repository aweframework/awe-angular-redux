import { aweApplication } from "../awe";

// Remove selected results
aweApplication.filter('removeSelected', () => (items = [], selected = []) =>
  // Fitrar por los elementos que cumplen el criterio de busqueda
  items.filter(item => !(selected || []).map(option => option.value).includes(item.value)));