import { aweApplication } from "../awe";
import "./component";
import { getComponentId, ROW_IDENTIFIER } from "../actions/components";

// Criterion services
aweApplication.factory('Column', function () {
  return {
    /**
     * Init as column component
     * @param $ctrl Controller
     * @param $scope Scope
     */
    init: function($ctrl, $scope) {
      let $gridScope = $scope.$parent;
      let $gridCtrl = $gridScope.col.grid.appScope.$ctrl;
      let row = $gridScope.row ? $gridScope.row.entity[ROW_IDENTIFIER] : "footer";

      // Get cell address
      let address = {
        ...$gridCtrl.address,
        row: row,
        column: $gridScope.col.name
      };

      // Generate controller identifier
      $ctrl.id = getComponentId(address);
    }
  };
});
