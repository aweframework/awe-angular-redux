import {aweApplication} from "../awe";

import {ComponentStatus, ComponentType, getComponentId, updateComponents} from "../actions/components";
import {updateMessages} from "../actions/messages";
import {setScreenView} from "../actions/screen";
// Import connection
import "./settings";
import "./connection";
import "./control";
import "./load";
import "../singletons/actionController";

const {COMPONENT_GRID, COMPONENT_OTHER, COMPONENT_RADIO, COMPONENT_BUTTON_RADIO} = ComponentType;
const {STATUS_DEFINED} = ComponentStatus;

/**
 * Server communication management service
 */
aweApplication.factory('ServerData',
  ['AweSettings', 'AweUtilities', 'Connection', '$log', 'ActionController', '$templateCache', 'Control', '$ngRedux', 'Validator', 'Load',
    function ($settings, $utilities, $connection, $log, $actionController, $templateCache, $control, $ngRedux, $validator, $load) {
      /**
       * Fixes component model
       * @param {object} model component model
       * @param {boolean} isGrid component is a grid
       * @return {object} Model fixed
       */
      function fixModel(model, isGrid) {
        let selected = $utilities.asArray(model.selected).map(value => String(value));
        let values = model.values;
        if (isGrid) {
          values = values.map(value => ({...value, selected: selected.includes(String(value.id))}));
        } else {
          values = values.map(value => ({...value, selected: selected.includes(String(value.value))}));
          if (selected.length > 0 && values.length === 0) {
            values = selected.map(value => ({selected: true, value: value}));
          }
        }
        return {
          ...model,
          values: values
        };
      }

      /**
       * Fix controller values
       * @param {object} controller Controller data
       * @param {boolean} isGrid Controller is from a grid
       * @return {object} Fixed controller values
       */
      function fixController(controller, isGrid) {
        // Init size and charSize
        let max = controller.max || $settings.get("recordsPerPage");
        let columnModel = isGrid ? {columnModel: controller.columnModel.map(column => fixController(column, false))} : {};
        return {
          ...controller,
          ...columnModel,
          size: controller.size || $settings.get('defaultComponentSize'),
          charSize: $settings.get('pixelsPerCharacter'),
          max: max,
          iconLoader: controller.iconLoading || "newSpinner",
          iconLoaderSmall: controller.iconLoading || "newSpinnerSmall",
          specificParameters: {
            max: controller.loadAll ? 0 : max
          }
        };
      }

      let cacheActive = false;
      let ServerData = {
        /**
         * Retrieve a screen template code
         * @param {string} screen Screen name
         * @param {string} view Screen view
         * @returns {string} Screen template
         */
        getScreenData: function (screen, view) {
          let parameters = {
            ...ServerData.getFormValues(),
            [$settings.get("serverActionKey")]: "screen-data",
            view: view
          };

          if (screen !== null) {
            parameters[$settings.get("optionKey")] = screen;
            $log.info("Retrieving screen data for " + screen);
          } else {
            $log.info("Retrieving screen data for initial screen");
          }

          // Generate server action
          let serverAction = {type: 'server', parameters: parameters};
          let actionList = [serverAction];

          // Add action to actions stack
          $actionController.addActionList(actionList, false, {});
        },
        /**
         * Store the loaded screen data
         * @param {object} data Screen data
         * @param {string} view Screen view
         */
        storeScreenData: function (data, view) {
          // Store data
          if (data !== null) {
            // Store components
            let context = $utilities.getCurrentContext(view);
            let components = data.components.reduce((components, component) => {
              let isGrid = "columnModel" in component.controller;
              let model = fixModel(component.model, isGrid);
              let controller = fixController(component.controller, isGrid);
              let validationRules = $validator.parseValidationRules(component.controller.validation, {view: view, component: component.id});
              return {
                ...components,
                [component.id]: {
                  uid: $utilities.getUID(),
                  context: context,
                  address: {view: view, component: component.id},
                  model: {...model},
                  storedModel: {...model},
                  attributes: {...controller},
                  storedAttributes: {...controller},
                  validationRules: {...validationRules},
                  storedValidationRules: {...validationRules},
                  actions: component.controller.actions || [],
                  dependencies: component.controller.dependencies || [],
                  contextMenu: component.controller.contextMenu || [],
                  status: STATUS_DEFINED
                }
              };
            }, {});

            $ngRedux.dispatch(updateComponents(view, components));

            // Store screen messages
            $ngRedux.dispatch(updateMessages(view, data.messages));

            // Generate screen data
            $ngRedux.dispatch(setScreenView(view, {...data.screen, context: context, visible: false, loaded: true}));

            // Start view load
            $load.start(view, data.screen.name);
          }
        },
        /**
         * Retrieve a screen template code
         * @param {string} screen Screen name
         * @param {string} view Screen view
         * @returns {string} Screen template
         */
        getTemplateUrl: function (screen, view) {
          let template = "";
          // Add option
          if (!$utilities.isEmpty(screen)) {
            template = $utilities.generateEndpointUrl("template", "screen", view, screen);
          } else {
            template = $utilities.generateEndpointUrl("template", "screen");
          }

          // Retrieve url
          return $connection.getRawUrl() + template;
        },
        /**
         * Retrieve a screen template code
         * @param {string} screen Screen name
         * @param {string} view Screen view
         * @returns {string} Screen template
         */
        getTaglistUrl: function (option, taglist) {
          // Retrieve url
          return $connection.getRawUrl() + $utilities.generateEndpointUrl("taglist", option, taglist);
        },
        /**
         * Retrieve the help url for a screen
         * @param {string} option Option identifier
         * @returns {string} Help template url
         */
        getHelpUrl: function (option) {
          // Retrieve url
          return $connection.getRawUrl() + $utilities.generateEndpointUrl("template", "help", option);
        },
        /**
         * Retrieves a generic file url
         * @param {string} action Action to launch
         * @param {string} target Action target
         * @returns {string} Help template url
         */
        getGenericFileUrl: function (action, target) {
          // Retrieve url
          return $connection.getRawUrl() + $utilities.generateEndpointUrl(action, target);
        },
        /**
         * Compose url and parameters to retrieve a file from server
         * @param {string} action Action name
         * @param {object} parameters Parameter list
         * @returns {object} File data
         */
        getFileData: function (action, parameters) {
          // Retrieve url
          return {url: ServerData.getFileUrl(action), data: parameters};
        },
        /**
         * Get encoded parameters
         * @param {object} parameters Parameter list
         * @returns {object} Encoded parameters
         */
        getEncodedParameters: function (parameters) {
          // Retrieve url
          return $connection.getEncodedParameters(parameters);
        },
        /**
         * Retrieve URL for one server file
         * @param {String} action
         * @returns {String} Server file URL
         */
        getFileUrl: function (action) {
          return $connection.getRawUrl() + $utilities.generateEndpointUrl("file", action);
        },
        /**
         * Retrieve an angular template code
         * @param {string} template Template to load
         * @returns {string} Angular template url
         */
        getAngularTemplateUrl: function (template) {
          return $connection.getRawUrl() + $utilities.generateEndpointUrl("template", "angular", template);
        },
        /**
         * Preload an angular template code
         * @param {object} template Template to load
         * @param {Function} onLoad on loaded method
         */
        preloadAngularTemplate: function (template, onLoad = null) {
          let templateUrl = ServerData.getAngularTemplateUrl(template.path);
          let templateId = template.name || template.path;
          ServerData.get(templateUrl).then(function (response) {
            if (response.data && response.status === 200) {
              $templateCache.put(templateId, response.data);
              onLoad && onLoad(response.data);
            }
          });
        },
        /**
         * Retrieve the criterion data
         * @param {object} criterion Criterion data
         * @returns {object} model data
         */
        getCriterionData: function (criterion) {
          let data;
          let selected = $control.getSelectedValues(criterion.address);
          switch (selected.length) {
            case 0:
              data = null;
              break;
            case 1:
              data = selected[0];
              break;
            default:
              data = selected;
              break;
          }
          return {
            [criterion.attributes.id]: data
          }
        },
        /**
         * Retrieve the criterion data
         * @param {object} criterion Criterion data
         * @returns {object} model data
         */
        getGridData: function (grid) {
          let values = grid.model.values || [];
          let selected = grid.attributes.sendAll ? values : values.filter((value) => value.selected);
          let columnNames = grid.attributes.columnModel
            .filter(column => column.sendable)
            .map(column => column.name);
          let columns = columnNames.reduce((columns, name) => ({...columns, [name]: selected.map(value => value[name])}), {});
          return {
            ...columns,
            [grid.attributes.id]: selected.map(value => value.id)
          };
        },
        /**
         * Retrieve the updated model
         * @param {object} component Component to check
         * @param {object} model model to update
         * @param {string} method Method to call
         * @param {string} args Method arguments
         * @returns {object} model updated
         */
        getComponentData: function (component) {
          switch ((component.attributes || {}).component) {
            case COMPONENT_GRID:
              return ServerData.getGridData(component);
              break;
            case COMPONENT_OTHER:
              return {};
              break;
            default:
              return ServerData.getCriterionData(component);
              break;
          }
        },
        /**
         * Retrieve the updated model
         */
        getFormValues: function () {
          // Retrieve action data
          return Object.values($ngRedux.getState().components)
            .filter(component => !("row" in component.address || "column" in component.address))
            .reduce((result, component) => {
              let values = ServerData.getComponentData(component);
              let componentId = getComponentId(component.address);
              if (componentId in result) {
                $log.warn("[WARNING] Overwriting '" + component.id + "' duplicated parameter", {'old': result[componentId], 'new': values});
              }
              return {
                ...result,
                ...values
              };
            }, {
              ...$settings.getTokenObject()
            });
        },
        /**
         * Retrieve the updated model for printing
         */
        getFormValuesForPrinting: function () {
          // Retrieve action data
          return Object.values($ngRedux.getState().components)
            .reduce((result, component) => {
              let values = ServerData.getComponentData(component);
              let componentId = getComponentId(component.address);
              if (componentId in result) {
                $log.warn("[WARNING] Overwriting '" + componentId + "' duplicated parameter", {'old': result[componentId], 'new': values});
              }
              return {
                ...result,
                ...values
              };
            }, {
              ...$settings.getTokenObject()
            });
        },
        /**
         * Retrieve parameters and send them to the server
         * @param {object} action Action received
         * @param {object} parameters Server action parameters
         */
        launchServerAction: function (action, parameters) {
          // Retrieve form values
          let message = {
            action: action,
            target: action.callbackTarget,
            values: {
              ...action.parameters || {},
              ...parameters
            }
          };

          // Retrieve target specific attributes for the server call
          if (message.target) {
            $control.changeAttribute(message.target, {loading: true});
          }

          // Launch server action
          let request = $connection.sendMessage(message);
          action.onCancel = function () {
            if (request && request.reject) {
              request.reject();
            }
          };
        },
        /**
         * Generate a server action
         * @param {string} address Target address
         * @param {object} custom Custom values
         * @param {boolean} async Async action
         * @param {boolean} silent Silent action
         */
        getServerAction: function (address, custom, async, silent) {
          // Define server and target action
          const SERVER_ACTION = $settings.get("serverActionKey");
          const TARGET_ACTION = $settings.get("targetActionKey");
          let state = $ngRedux.getState();

          // Generate action
          return {
            type: 'server',
            async: async,
            silent: silent,
            parameters: {
              ...custom,
              screen: state.screen[state.screen.view].name,
              address: address,
              component: address.component,
              [SERVER_ACTION]: custom.type || custom[SERVER_ACTION] || "data",
              [TARGET_ACTION]: custom[TARGET_ACTION]
            }
          };
        },
        /**
         * Send a query to the server
         * @param {object} address Target address
         * @param {object} custom Custom values
         * @param {boolean} async Async action
         * @param {boolean} silent Silent action
         */
        sendQuery: function (address, custom, async, silent) {
          // Generate server action
          var action = ServerData.getServerAction(address, custom, async, silent);
          // Add action to actions stack
          $actionController.addActionList([action], false, {address: address, context: ""});
        },
        /**
         * Send a query to the server
         * @param {object} custom Custom values
         * @param {boolean} async Async action
         * @param {boolean} silent Silent action
         */
        sendMaintain: function (custom, async, silent) {
          // Generate server action
          // Generate action
          var action = {
            type: 'server',
            async: async,
            silent: silent,
            parameters: {}
          };

          // Add server action
          action.parameters["server-action"] = custom.type;
          action.parameters["target-action"] = custom.maintain;

          // Add action to actions stack
          $actionController.addActionList([action], false, {address: {}, context: ""});
        },
        /**
         * Send a message to the server
         * @param {object} message Message to send
         * @return {object} Connection promise
         */
        send: function (message) {
          return $connection.send(message);
        },
        /**
         * Launch a get message to the server
         * @param {object} url Message to send
         * @return {object} Connection promise
         */
        get: function (url) {
          return $connection.get(url);
        },
        /**
         * Toggle cache status
         * @param {boolean} enable Enable cache
         */
        toggleCache: function (enable) {
          cacheActive = enable;
        },
        /**
         * Get connection ID
         * @return {string} Connection ID
         */
        getConnectionId: function () {
          let token = $settings.getTokenString();
          let tokenString = token != null ? `?${token}` : "";
          let avoidCache = !cacheActive ? `&r=${Math.random()}` : "";
          return `${tokenString}${avoidCache}`;
        }
      };
      return ServerData;
    }
  ]);
