import { aweApplication } from "../awe";
import "./criterion";
import {ROW_IDENTIFIER} from "../actions/components";

// Header template
const templateHeader =
`<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
   <awe-context-menu ng-cloak></awe-context-menu>
   <div ng-class="$ctrl.groupClass" ng-cloak>
     <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
       {{$ctrl.attributes.label| translateMultiple}}
       <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
     </label>
     <div class="validator input {{::$ctrl.validatorGroup}} focus-target" ng-class="{'input-group': $ctrl.attributes.unit}">
       <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>`;

// Footer template
const templateFooter =
`     <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
      <span ng-if="$ctrl.attributes.unit" class="input-group-addon add-on unit" translate-multiple="{{$ctrl.attributes.unit}}" ng-cloak></span>
    </div>
  </div>
</div>`;

// Header template
const templateHeaderColumn =
`<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate {{$ctrl.value.style}}" ng-cloak>
  <span class="visible-value" translate-multiple="{{$ctrl.textValue}}" ng-cloak></span>
  <span class="edition" ng-if="$ctrl.initializePlugin">
    <div class="input input-group-{{::$ctrl.attributes.size}} focus-target">
      <span ng-if="::$ctrl.attributes.icon" ng-class="::iconClass" ng-cloak></span>`;

// Footer template
const templateFooterColumn =
`     <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
    </div>
  </span>
</div>`;

// Multiple attribute template
const multiple = `multiple="multiple"`;

// Suggest attribute template
const suggest = `on-refresh="$ctrl.suggest"`;

/**
 * Retrieve template for selector
 * @param isMultiple Is multiple
 * @param isSuggest Is suggest
 * @returns {string} Template for selector
 */
function getTemplateSelector(isMultiple, isSuggest) {
  return  `<select ui-select
            ${isMultiple ? multiple : ""}
            ${isSuggest ? suggest : ""}
            ng-attr-id="{{::$ctrl.id}}"
            ng-change="$ctrl.onChange()"
            config="$ctrl.config"
            ng-disabled="$ctrl.attributes.readonly"
            ng-model="$ctrl.value"
            ng-options="$item as $item.text for $item in $ctrl.values track by $item.id">
          </select>`;
}

/**
 * Template selector
 * @param isMultiple Is multiple
 * @param isSuggest Is suggest
 * @returns {string} Template
 */
export function templateSelector(isMultiple, isSuggest) {
  return `${templateHeader}
     ${getTemplateSelector(isMultiple, isSuggest)}
     ${templateFooter}`;
}

/**
 * Template selector for columns
 * @param isMultiple Is multiple
 * @param isSuggest Is suggest
 * @returns {string} Template
 */
export function templateSelectorColumn(isMultiple, isSuggest) {
  return `${templateHeaderColumn}
     ${getTemplateSelector(isMultiple, isSuggest)}
     ${templateFooterColumn}`;
}

// Selector service
aweApplication.factory('Selector',
  ['Control', 'Criterion', 'AweUtilities', 'AweSettings', 'ActionController', 'ServerData', '$translate', '$ngRedux',
    function ($control, $criterion, $utilities, $settings, $actionController, $serverData, $translate, $ngRedux) {

      /**
       * Retrieve selected values
       */
      function getSelectedOptions(values = []) {
        return $utilities.asArray(values).filter(option => option.selected);
      }

      /**
       * Select values
       * @param {Array} values New model
       * @param {Array} selected Current values
       * @return {Array} Selected values marked
       */
      function markSelectedValues(values = [], selected = []) {
        let selectedValues = $utilities.asArray(selected).map(item => String(item.value));
        return values.map(option => ({ ...option, selected: selectedValues.includes(String(option.value))}));
      }

      /**
       * Filter current model
       * @param {Array} model New model
       * @param {Array} current Current values
       * @return {Array} Filtered model
       */
      function filterModel(model = [], current = []) {
        let selectedWithoutDupes = current.reduce((selected, option) => selected.map(item => String(item.value)).includes(String(option.value)) ? selected : [...selected, option], []);
        let selectedValues = selectedWithoutDupes.map(item => item.value);
        return [...model.filter(option => !selectedValues.includes(option.value)), ...selectedWithoutDupes];
      }

      const $selector = {
        /**
         * Initialize common methods
         * @param $ctrl Controller
         * @param $scope Scope
         */
        initCommons: function($ctrl, $scope) {
          // Initialize as criterion
          $criterion.init($ctrl, $scope);
          $ctrl.options = {};
          $ctrl.initialLoad = true;

          ////////////////////////////////////
          // COMMON API
          ////////////////////////////////////

          /**
           * Update model (REDUX updated model)
           */
          $ctrl.updateModel = function() {
            // Update ng-model
            $ctrl.values = $ctrl.model.values.map(option => ({...option, id: String(option.value), text: $translate.instant(option.label)}));
            $ctrl.updateValue();
            $ctrl.searching && $ctrl.searching({results: $ctrl.values.filter(option => !option.selected)});
            $ctrl.searching = null;

            // Trigger change
            $scope.$broadcast('trigger-change');

            // Update text value
            $ctrl.textValue = getSelectedOptions($ctrl.value).map(value => value.label).join(", ");
          };

          /**
           * Update attributes (REDUX updated attributes)
           */
          $ctrl.updateAttributes = function() {
            // Check validation errors
            $ctrl.checkValidationErrors();

            // Placeholder
            $ctrl.placeholder = $ctrl.attributes.placeholder || $ctrl.defaultPlaceholder;

            // Set refresh delay
            $ctrl.refreshDelay = parseInt($ctrl.attributes.timeout || $settings.get('suggestTimeout'), 10);

            // Update options
            $ctrl.config = {
              ...$ctrl.options,
              delay: $ctrl.refreshDelay,
              placeholder: $translate.instant($ctrl.placeholder),
              optional: $ctrl.options.optional || $ctrl.attributes.optional
            };
          };

          /**
           * Request full data
           * @param {Object} params Parameters
           */
          $ctrl.requestFullData = function(params) {
            // Define server and target action
            const SERVER_ACTION = $settings.get("serverActionKey");
            const TARGET_ACTION = $settings.get("targetActionKey");

            // Add action to actions stack
            let actionData = {
              ...($ctrl.attributes.specificParameters || {}),
              [SERVER_ACTION]: "value",
              [TARGET_ACTION]: $ctrl.attributes.checkTarget || $ctrl.attributes[TARGET_ACTION],
              suggest: params
            };

            // Generate server action
            let serverAction = $actionController.generateAction($serverData.getServerAction($ctrl.address, actionData, true, true), {address: $ctrl.address}, true, true);

            // Send action list
            $actionController.addActionList([serverAction], false, {address: $ctrl.address});
          };

          /**
           * Suggest
           * @param {Object} params Parameters
           * @param {Function} success On success function
           * @param {Function} failure On failure function
           */
          $ctrl.suggest = function(params, success, failure) {
            if (params && params.data && params.data.term && params.data.term.length > 0) {
              // Cancel previous action if launched
              $actionController.abortAction($ctrl.attributes.lastAction || {});

              // Update specific parameters
              $control.changeAttribute($ctrl.address, {
                specificParameters: {
                  ...$ctrl.attributes.specificParameters || {},
                  suggest: params.data.term
                }
              });

              // Reload data (from component)
              let filterAction = $ctrl.reload(false, false);

              // Update specific parameters
              $control.changeAttribute($ctrl.address, {lastAction: filterAction});

              // Set loading text
              $ctrl.noChoiceText = 'SELECT2_LOADING';

              // Set defer
              $ctrl.searching = success;
            } else {
              // Set loading text
              success({results:[]});
            }
            return {abort: () => null};
          };

          /**
           * On initialize component, check if is a column component to delay plugin initialization
           */
          $ctrl.onInitialize = function() {
            let disconnectGridModelUpdate;
            if (!$ctrl.initializePlugin) {
              let onModelUpdate = state => {
                let selectedRows = ((state.model || {}).values || []).filter(row => row.selected);
                if (selectedRows.length === 1 &&
                  selectedRows.filter(row => row[ROW_IDENTIFIER] === $ctrl.address.row).length === 1) {
                  disconnectGridModelUpdate && disconnectGridModelUpdate();
                  $ctrl.initializePlugin = true;
                }
              };

              let getModelUpdate = state => ({model: (state.components[$ctrl.address.component] || {}).model, uid: (state.components[$ctrl.id] || {}).uid});
              disconnectGridModelUpdate = $ngRedux.connect(getModelUpdate)(onModelUpdate);
              $ctrl.events.push(disconnectGridModelUpdate);
            }
          };
        },

        /**
         * Initialize as simple selector
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initSelect: function($ctrl, $scope) {
          $ctrl.defaultPlaceholder = 'SELECT2_SELECT_VALUE';

          // Initialize common methods
          $selector.initCommons($ctrl, $scope);

          // Set options as monoselection
          $ctrl.options = {
            maxItems: 1,
            minimumResultsForSearch: 5,
            optional: false
          };

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
           * On change method (From COMPONENT to REDUX)
           */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: markSelectedValues($ctrl.model.values, $ctrl.value) });
          };

          /**
           * Update value (From REDUX to COMPONENT)
           */
          $ctrl.updateValue = function() {
            $ctrl.value = getSelectedOptions($ctrl.values)
              .reduce((selected, option) => option, null);

            // Take first value if not optional
            if (!$ctrl.attributes.optional && $ctrl.value == null && $ctrl.values.length > 0) {
              $ctrl.value = $ctrl.values[0];
            }
          };
        },
        /**
         * Initialize as select multiple
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initSelectMultiple: function($ctrl, $scope) {
          $ctrl.defaultPlaceholder = 'SELECT2_SELECT_VALUE';

          // Initialize common methods
          $selector.initCommons($ctrl, $scope);

          // Select options
          $ctrl.options = {
            maxItems: 1,
            optional: false
          };

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
          * On change method (From COMPONENT to REDUX)
          */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: markSelectedValues($ctrl.model.values, $ctrl.value) });
          };

          /**
           * Update value (From REDUX to COMPONENT)
           */
          $ctrl.updateValue = function() {
            $ctrl.value = getSelectedOptions($ctrl.values);
          };
        },
        /**
         * Initialize as suggest
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initSuggest: function($ctrl, $scope) {
          $ctrl.defaultPlaceholder = 'SELECT2_SEARCH_VALUE';

          // Initialize common methods
          $selector.initCommons($ctrl, $scope);

          // Suggest options
          $ctrl.options = {
            maxItems: 1,
            optional: true,
            minimumInputLength: 1
          };

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
           * On change method (From COMPONENT to REDUX)
           */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: getSelectedOptions(markSelectedValues($ctrl.model.values, $ctrl.value)) });
          };

          /**
           * Update value (From REDUX to COMPONENT)
           */
          $ctrl.updateValue = function() {
            // Filter model if event is not reset or restore
            if (!$ctrl.initialLoad && $ctrl.model.event == null) {
              $ctrl.values = markSelectedValues(filterModel($ctrl.values, $utilities.asArray($ctrl.value)), [...$utilities.asArray($ctrl.value), ...getSelectedOptions($ctrl.model.values)]);
            }

            // Get selected value
            $ctrl.initialLoad = false;
            $ctrl.value = getSelectedOptions($ctrl.values)
              .reduce((selected, option) => selected || option, null);

            // If value has been inferred, check for full data
            if ($ctrl.value != null && !("label" in $ctrl.value)) {
              $ctrl.requestFullData($ctrl.value.value);
              $ctrl.initialLoad = true;
            }
          };
        },
        /**
         * Initialize as suggest multiple
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initSuggestMultiple: function($ctrl, $scope) {
          $ctrl.defaultPlaceholder = 'SELECT2_SEARCH_VALUE';

          // Initialize common methods
          $selector.initCommons($ctrl, $scope);

          // Add suggest check
          $ctrl.options = {
            optional: false,
            minimumInputLength: 1
          };

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
          * On change method (From COMPONENT to REDUX)
          */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: getSelectedOptions(markSelectedValues(filterModel($ctrl.model.values, $ctrl.value), $ctrl.value)) });
          };

          /**
           * Update value (From REDUX to COMPONENT)
           */
          $ctrl.updateValue = function() {
            // Filter model if event is not reset or restore
            if (!$ctrl.initialLoad && $ctrl.model.event == null) {
              $ctrl.values = markSelectedValues(filterModel($ctrl.values, $ctrl.value), [...$ctrl.value, ...getSelectedOptions($ctrl.model.values)]);
            }

            // Retrieve selected values
            $ctrl.initialLoad = false;
            $ctrl.value = getSelectedOptions($ctrl.values);

            // If value has been inferred, check for full data
            if ($ctrl.value.length > 0 && $ctrl.value.reduce((previous, option) => option.label || previous, null) == null) {
              $ctrl.requestFullData($ctrl.value.map(option => option.value));
              $ctrl.initialLoad = true;
            }
          };
        }
      };
      return $selector;
    }
  ]);
