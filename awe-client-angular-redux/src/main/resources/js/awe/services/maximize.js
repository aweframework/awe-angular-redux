import { aweApplication } from "../awe";

import "./position";

import _ from "lodash";

// Maximize service
aweApplication.factory('Maximize',
  ['Position', 'AweUtilities',
    function ($position, $utilities) {
      // Store heading and content panel
      let $body = $('body');
      /**
       * Remove animation node
       * @param {object} $node Animation node
       * @param {object} $ctrl Controller
       */
      function removeAnimationNode($node, $ctrl) {
        $utilities.timeout(function () {
          // Remove animation
          $body.removeClass("animationContainer");
          $node.remove();
          // End resizing
          //scope.$root.resizing = false;
          $ctrl.panelResizing = false;
          $ctrl.maximizing = false;
        });
      }
      /**
       * Launches maximize/restore animations
       * @param {object} node Resize node
       * @param {object} finalSize Final resize size
       * @param {function} onEndAnimation On end animation function (optional)
       * @param {object} $ctrl Controller
       */
      function launchAnimation(node, finalSize, onEndAnimation, $ctrl) {
        let $node = $(node);
        /**
         * Launch end animation methods
         */
        function endAnimation() {
          // Launch on end animation event (if defined)
          onEndAnimation && onEndAnimation();

          // Remove animation node
          removeAnimationNode($node, $ctrl);
        }

        if ($ctrl.useCSS3Animation) {
          $utilities.animateCSS($node, finalSize, $ctrl.animationTime, endAnimation);
        } else {
          $utilities.animateJavascript($node, finalSize, $ctrl.animationTime, endAnimation);
        }
      }
      /**
       * Generate layer cloned for animation
       */
      function generateAnimationClone($element) {
        // Get initial size
        let initialSize = $position.getOuterDimensions($element);
        // Add animate clone
        let resizing = $element.clone();
        resizing.css(initialSize);
        resizing.removeClass("expand");
        resizing.addClass("resizeAnimation");

        // Empty content layer
        resizing.find(".maximize-content").addClass("panel-awe").empty();


        $body.addClass("animationContainer");
        $body.append(resizing);
        // Return clone layer
        return resizing;
      };

      /**
       * Minimize parents and siblings
       * @param {type} maximizeTarget
       * @param {type} minimize
       * @param {type} $element
       * @returns {undefined}
       */
      function updateElementsToMinimize(maximizeTarget, minimize, $element) {
        minimize.targets = [].concat($element.siblings(":visible").toArray());
        minimize.parents = [];
        _.each($element.parentsUntil(maximizeTarget), function (parent) {
          minimize.targets = minimize.targets.concat($(parent).siblings(":visible").toArray());
          minimize.parents.push(parent);
        });
      };

      /**
       * Minimize parents and siblings
       * @returns {undefined}
       */
      function minimizeParentsAndSiblings(minimize, $element) {
        $(minimize.parents).addClass("maximizeParent");
        $(minimize.targets).addClass("minimized");
        $element.removeAttr('style');
      }

      const Maximize = {
        /**
         * Initialize dialog
         * @param {Object} $scope Component scope
         * @param {Object} $ctrl Component controller
         * @param {Object} $element Component node
         */
        initMaximize: function ($scope, $ctrl, $element) {
          // Panel text
          var panelText = {
            MAXIMIZE: 'SCREEN_TEXT_MAXIMIZE',
            RESTORE: 'SCREEN_TEXT_RESTORE'
          };

          // Variable initialization
          var maximizeTarget;
          var minimize = {
            targets: [],
            parents: []
          };

          $ctrl.animationTime = 300;
          $ctrl.useCSS3Animation = true;
          $ctrl.maximized = false;
          $ctrl.iconMaximized = false;
          $ctrl.togglePanelText = panelText.MAXIMIZE;
          $ctrl.panelResizing = false;
          $ctrl.maximizing = false;

          // Controller variables
          if ($ctrl.attributes) {
            $ctrl.maximize = $ctrl.attributes.maximize;
          } else {
            $ctrl.maximize = false;
          }

          /**
           * Calculate maximized size depending on maximize target
           * @returns {object} Resizing size
           */
          $ctrl.maximizeTargetLayer = function () {
            var finalSize = $position.getInnerDimensions(maximizeTarget);
            // Get margins
            var margins = {
              width: $element.outerWidth(true) - $element.outerWidth(false),
              height: $element.outerHeight(true) - $element.outerHeight(false)
            };
            finalSize.width -= margins.width;
            finalSize.height -= margins.height;
            // Apply maximize dimensions to target layer
            let elemSize = _.cloneDeep(finalSize);
            let offset = $element.offsetParent().offset();
            elemSize.top -= offset.top;
            elemSize.left -= offset.left;
            // Calculate final size for animation
            finalSize.top += parseInt($element.css('margin-top'));
            finalSize.left += parseInt($element.css('margin-left'));
            return {final: finalSize, element: elemSize};
          };
          /**
           * On resize screen
           *
           */
          $ctrl.onResize = function () {
            if ($ctrl.maximized) {
              var maximizeSizes = $ctrl.maximizeTargetLayer();
              $element.css(maximizeSizes.element);
            }
          };
          /**
           * Maximize the panel
           */
          $ctrl.maximizePanel = function () {
            // Generate animation clone
            let resizing = generateAnimationClone($element);
            // Set resizing and maximized
            //$ctrl.$root.resizing = true;
            $ctrl.panelResizing = true;
            $ctrl.maximizing = true;
            // Restore all maximized parents
            $scope.$emit("restore");
            // Find maximize target
            if (!maximizeTarget) {
              maximizeTarget = $element.parents(".maximize-target:first");
            }

            // Launch animation
            $utilities.timeout(function () {
              // Launch animation
              let maximizeSizes = $ctrl.maximizeTargetLayer();
              updateElementsToMinimize(maximizeTarget, minimize, $element);
              $(minimize.targets).fadeOut($ctrl.animationTime);
              launchAnimation(resizing, maximizeSizes.final, function () {
                $ctrl.maximized = true;
                $ctrl.iconMaximized = true;
                minimizeParentsAndSiblings(minimize, $element);
                $scope.$broadcast("resize");
              }, $ctrl);
            }, 200);
          };
          /**
           * Restore the window size
           */
          $ctrl.restorePanel = function () {
            // Generate animation clone
            let resizing = generateAnimationClone($element);
            // Remove minimize class
            $(minimize.targets).removeClass("minimized").fadeIn($ctrl.animationTime + 150);
            $(minimize.parents).removeClass("maximizeParent").fadeIn($ctrl.animationTime + 150);
            // Remove minimized class
            $element.removeAttr('style');
            // Set resizing
            //$ctrl.$root.resizing = true;
            $ctrl.panelResizing = true;
            $ctrl.maximized = false;
            $utilities.timeout(function () {
              // Launch animation
              let finalSize = $position.getOuterDimensions($element);
              launchAnimation(resizing, finalSize, function () {
                $ctrl.iconMaximized = false;
                $scope.$broadcast("resize");
              }, $ctrl);
            }, 150);
          };

          /**
           * Restore the window size
           */
          $ctrl.togglePanel = function () {
            if ($ctrl.maximized) {
              $ctrl.restorePanel();
              $ctrl.togglePanelText = panelText.RESTORE;
            } else {
              $ctrl.maximizePanel();
              $ctrl.togglePanelText = panelText.MAXIMIZE;
            }
          };

          /**
           * Event listeners
           */
          // Capture event for element resize
          $ctrl.events.push($scope.$on("resize", function (event, initialScope) {
            if ($scope !== initialScope && $ctrl.onResize) {
              $ctrl.onResize();
            }
          }));
          // Capture event for children maximizes
          $ctrl.events.push($scope.$on("restore", function () {
            if ($ctrl.maximized) {
              $ctrl.restorePanel();
            }
          }));
        }
      };
      return Maximize;
    }
  ]);