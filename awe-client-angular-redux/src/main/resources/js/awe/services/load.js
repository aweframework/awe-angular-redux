import { aweApplication } from "../awe";

import { updateScreenView } from "../actions/screen";
import { ComponentStatus } from "../actions/components";
const { STATUS_DEFINED, STATUS_COMPILED, STATUS_INITIALIZED } = ComponentStatus;

// Criterion service
aweApplication.factory('Load',
  ['AweUtilities', '$log', 'LoadingBar', 'AweSettings', '$ngRedux',
    function ($utilities, $log, $loadingBar, $settings, $ngRedux) {
      let $ctrl = this;
      $ctrl.finish = {};
      $ctrl.timers = {};
      $ctrl.status = {};
      let $load = {
        /**
         * Start loading view
         */
        start: function(view, screen) {
          // Connect status
          $ctrl.finish[view] = $ngRedux.connect((state) => ({
            startDate: new Date(),
            screen: screen,
            view: view,
            components: Object.values(state.components).filter(component => component.address.view === view)
          }))($load.update);

          // Set loading timer
          $utilities.timeout.cancel($ctrl.timers[view]);
          $ctrl.timers[view] = $utilities.timeout(() => $load.timeout(view, screen), $settings.get("loadingTimeout"));

          // Set loading status
          $ctrl.status[view] = STATUS_DEFINED;

          // Load loading bar
          $loadingBar.startTask();

          // First update
          let state = $ngRedux.getState();
          $load.update({
            startDate: new Date(),
            screen: screen,
            view: view,
            components: Object.values(state.components).filter(component => component.address.view === view)
          });
        },
        /**
         * Update load status
         */
        update: function(state) {
          let compiled = state.components.filter((component) => component.status === STATUS_COMPILED);
          let initialized = state.components.filter((component) => component.status === STATUS_INITIALIZED);

          // Finish compilation
          let dateDiff;
          if ($ctrl.status[state.view] === STATUS_DEFINED && ((compiled.length + initialized.length) === state.components.length)) {
            $ctrl.status[state.view] = STATUS_COMPILED;
            dateDiff = (new Date() - state.startDate) / 1000;
            $log.debug("[SCREEN LOAD PHASE] Screen has been COMPILED", {screen: state.screen, view: state.view, compilationTime: `${dateDiff}s`});
          }

          // Finish initialization
          if ($ctrl.status[state.view] === STATUS_COMPILED && initialized.length === state.components.length) {
            $ctrl.status[state.view] = STATUS_INITIALIZED;
            dateDiff = (new Date() - state.startDate) / 1000;
            $log.debug("[SCREEN LOAD PHASE] Screen has been INITIALIZED", {screen: state.screen, view: state.view, totalTime: `${dateDiff}s`});
            $load.end(state.view);
          }
        },
        /**
         * Loading timeout
         */
        timeout: function(view, screen) {
          let state = $ngRedux.getState();
          $log.warn("[SCREEN LOAD PHASE] Screen load TIMEOUT", {
            view: view,
            screen: screen,
            pending: Object.values(state.components)
              .filter((component) => (component.address.view === view && component.status !== STATUS_INITIALIZED))
          });

          // Finish view load
          $load.end(view);
        },
        /**
         * Finish view load
         */
        end: function(view) {
          // Disconnect from status
          $ctrl.finish[view]();

          // Cancel timeout
          $utilities.timeout.cancel($ctrl.timers[view]);

          // Send load
          $utilities.timeout(() => {
            $ngRedux.dispatch(updateScreenView(view, {visible: true}));

            // End loading bar
            $loadingBar.endTask();
          });
        }
      };
      return $load;
    }
  ]);