import { aweApplication } from "../awe";
import _ from "lodash";

// Import rules
import "./validationRules";

// Component validator
aweApplication.factory('Validator',
  ['Storage', 'AweUtilities', 'AweSettings', 'ValidationRules', '$log', '$ngRedux', 'Control',
    function ($storage, $utilities, $settings, $validationRules, $log, $ngRedux, $control) {
      // CONSTANTS
      const VALIDATOR = '.validator';
      const ERROR_CONTAINER = '.error-container';
      const FORM_GROUP = '.form-group';

      let Validator = {
        /**
         * Validate a component
         * @param {Object} controller
         * @param {Object} address
         */
        getRules: function (attributes, address) {
          let rules = attributes.validationRules;
          if (!("validationRules" in attributes)) {
            // Parse validation rules
            rules = Validator.parseValidationRules(attributes.validation, address);

            // Update rules
            $control.changeAttribute(address, {validationRules: rules, storedValidationRules: {...rules}});
          }
          return rules;
        },
        /**
         * Check component base node
         * @param {Object} component
         * @param {Object} baseNode
         */
        checkBaseNode: function (component, baseNode) {
          // Check if baseNode contains the current node
          let $element = $(`[uid="${component.uid}"]`);
          var $baseNode = baseNode ? $(baseNode) : $element;
          if ($baseNode.is($element) || $baseNode.find($element).length > 0) {
            return true;
          }
          return false
        },
        /**
         * Validate a component
         * @param {Object} component
         * @param {Array} errorList
         */
        validateComponent: function (component, errorList) {
          let rules = Validator.getRules(component, component.address);
          let validation = Validator.validate($control.getSelectedValues(component.address), rules, component.address);
          if (validation !== null) {
            // Generate error
            let error = {message: validation, show: errorList.length === 0, id: component.address.component, uid: component.uid};

            // Change component attribute
            $control.changeAttribute(component.address, {error: error});

            // Add error to list
            errorList.push(error);
          } else {
            // Change component attribute if error is not null
            if (component.attributes.error) {
              $control.changeAttribute(component.address, {error: null});
            }
          }
        },
        /**
         * Retrieve the validation nodes
         * @param {Object} baseNode base node
         */
        validateNode: function (baseNode) {
          // Retrieve action data
          let errorList = [];
          let components = $ngRedux.getState().components;

          // Validate components
          _.each(components, (component) => {
            if (Validator.checkBaseNode(component, baseNode)) {
              Validator.validateComponent(component, errorList)
            }
          });

          // Return data
          return errorList;
        },
        /**
         * Retrieve the validation nodes
         * @param {Array} components base node
         */
        validateComponents: function (components) {
          // Retrieve action data
          let errorList = [];

          // Validate components
          components.forEach((componentId) => Validator.validateComponent($control.getComponentById(componentId), errorList));

          // Return data
          return errorList;
        },
        /**
         * Validate a value with
         * @param {Mixed} value
         * @param {Object} rules
         * @param {Object} element
         * @param {Object} address
         * @returns {Array}
         */
        validate: function (value, rules, address) {
          let errorMessage = null;
          _.each(rules, function (rule, ruleMethod) {
            if (errorMessage === null) {
              errorMessage = $validationRules.validate(ruleMethod, value, rule, address);
            }
          });
          return errorMessage;
        },
        /**
         * Retrieve the validation nodes
         * @param {type} $form
         * @param {type} error
         * @returns {undefined}
         */
        showValidationError: function ($form, error) {
          let $errorContainer = $(ERROR_CONTAINER);
          let $element = $(`[uid="${error.uid}"]`);

          // Retrieve the element data
          $utilities.timeout.cancel($form.errorTimer);
          $utilities.timeout(function () {
            let target = $element;
            let classTarget = $element.find(FORM_GROUP);
            let targetPosition = target.offset();
            targetPosition.top += target.height();

            // Show validation message
            $errorContainer.css(targetPosition);
            $form.validationStyle = classTarget.length > 0 ? classTarget.attr("class") : "";
            $form.validationMessage = error.message;
            $log.info(`[VALIDATION] '${error.id}' - ${$form.validationMessage}`);
            $form.showValidation = true;

            // Hide validation after 2 seconds
            $form.errorTimer = $utilities.timeout(() => $form.showValidation = false, $settings.get("messageTimeout").validate);
          });
        },
        /**
         * Retrieve the validation nodes
         * @param {*} validationRules Rules
         * @param {object} address Component address
         * @returns {object} Rules as object
         */
        parseValidationRules: function (validationRules, address) {
          let rulesParsed = {};
          if (validationRules) {
            if (angular.isObject(validationRules)) {
              rulesParsed = validationRules;
            } else {
              let rulesList = validationRules.indexOf("{") > -1 ? [validationRules] : validationRules.split(" ");
              rulesParsed = rulesList.reduce((parsed, rule) => ({...parsed, ...Validator.parseRule(rule, address)}), {});
            }
          }
          return rulesParsed;
        },
        /**
         * Retrieve the validation nodes
         * @param {type} rule
         * @param {type} address
         * @returns {object} rule parsed
         */
        parseRule: function (rule, address) {
          let newRule;
          if (angular.isObject(rule)) {
            newRule = rule;
          } else if (rule.indexOf("{") > -1) {
            try {
              newRule = $utilities.evalJSON(rule);
            } catch (exc) {
              $log.error("[ERROR] Parsing validation rule to JSON", {rule: rule, address: address, exception: exc});
            }
          } else {
            newRule = {[rule]: true};
          }
          return {...newRule};
        }
      };
      return Validator;
    }
  ]);
