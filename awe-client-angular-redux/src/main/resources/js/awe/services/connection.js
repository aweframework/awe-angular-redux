import { aweApplication } from "../awe";

// Import connections
import "./comet";
import "./ajax";

/**
 * Service to launch server calls
 * @param {String} Service name
 * @param {Function} Connection methods
 */
aweApplication.factory('Connection',
  ['Comet', 'Ajax',
    /**
     *
     * @param {AweSettings} settings Application settings
     * @param {Comet} $comet Comet based connection
     * @param {Ajax} $ajax Ajax based connection
     * @returns {_L10.Anonym$0}
     */
    function ($comet, $ajax) {
      // Service variables;
      return {
        /**
         * Init Connection
         */
        init: function (serverPath, encodeTransmission, token) {
          // Initialize connection and backup
          $ajax.init(serverPath, encodeTransmission);

          // Return comet initialization
          return $comet.init(encodeTransmission, token);
        },
        /**
         * Send message via websocket (does not wait response)
         * @param {String} message Message to send
         */
        sendMessage: function (message) {
          $ajax.sendMessage(message);
        },
        /**
         * Send message with a promise
         * @param {String} message Message to send
         */
        send: function (message) {
          return $ajax.send(message);
        },
        /**
         * Send get message with a promise
         * @param {String} url Message to send
         * @param {String} expectedContent Content type expected
         */
        get: function (url, expectedContent) {
          return $ajax.get(url, expectedContent);
        },
        /**
         * Send post message with a promise
         * @param {String} url Message url
         * @param {Object} data Message data
         * @param {String} expectedContent Content type expected
         */
        post: function (url, data, expectedContent) {
          return $ajax.post(url, data, expectedContent);
        },
        /**
         * Retrieve message URL
         * @param {Object} message
         * @return {String} Message URL
         */
        getUrl: function (message) {
          return $ajax.getUrl(message);
        },
        /**
         * Retrieve message URL
         * @return {String} Raw URL
         */
        getRawUrl: function () {
          return $ajax.getRawUrl();
        },
        /**
         * Retrieve encoded parameters
         * @param {Object} message
         * @return {String} Message URL
         */
        getEncodedParameters: function (message) {
          return $ajax.getEncodedParameters(message);
        }
      };
    }
  ]);