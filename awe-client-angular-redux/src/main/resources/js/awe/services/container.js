import { aweApplication } from "../awe";
import "./component";
import "./utilities";

/**
 * Retrieve container template
 * @param {string} type Container type
 * @return {string} Container template
 */
export function getContainerTemplate(type) {
  return `<div ng-attr-id="{{::$ctrl.id}}" ng-class="{'active':$ctrl.attributes.active}" class="${type}-pane {{::$ctrl.attributes.style}} expand expandible-vertical" ng-transclude></div>`;
}

// Button services
aweApplication.factory('Container',
  ['AweUtilities', 'Component',
    /**
     * Container
     * @param $utilities Utilities
     * @param $component Component
     * @returns {{init: init}}
     */
  function ($utilities, $component) {
    const $container = {
      /**
       * Initialize as container
       * @param $ctrl Controller
       * @param $scope Scope
       * @param $element Element
       */
      init: function($ctrl, $scope, $element) {
        // Initialize as component
        $component.init($ctrl, $scope);

        // Select on children
        $ctrl.events.push($scope.$on("selectCurrentTab", () => $scope.$parentCtrl.selectTab($ctrl.id)));

        /**
         * On update attributes publish resize
         */
        $ctrl.updateAttributes = function() {
          if ($ctrl.attributes.active) {
            $utilities.executeWhenVisible($scope, $element[0], () => $utilities.publishFromScope('resize', {}, $scope));
          }
        };
      }
    };
    return $container;
  }
]);