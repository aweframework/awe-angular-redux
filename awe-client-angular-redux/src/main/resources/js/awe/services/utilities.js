import { aweApplication } from "../awe";
import { getUID } from "../actions/settings";
import _ from 'lodash';

// Application default utilities
aweApplication.factory('AweUtilities',
  ['$log', '$window', '$compile', '$timeout', '$interval', '$rootScope', '$q', '$location', '$ngRedux',
    function ($log, $window, $compile, $timeout, $interval, $rootScope, $q, $location, $ngRedux) {
      let pendingListeners = [];
      let clearPendingListenersDebounced;
      const $utilities = {
        /**
         * Indicates if parameter is a string
         * @public
         * @param  {Object} objeto Object to check
         * @return {Boolean} Object is a string
         * @example
         * alert(AweUtilities.isString(miVariable));
         */
        isString: function (objeto) {
          return angular.isString(objeto);
        },
        /**
         * Retrieve the browser
         */
        getBrowser: function () {
          let userAgent = $window.navigator.userAgent;
          let browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /msie|trident/i};
          for (let key in browsers) {
            if (browsers[key].test(userAgent)) {
              return key;
            }
          }
          return 'unknown';
        },
        /**
         * Converts a json into a string
         * @public
         * @param {Object} value
         * @return {String} Json String
         */
        stringifyJSON: function (value) {
          return JSON.stringify(value);
        },
        /**
         * Converts a string into a json object
         * @public
         * @param {string} json
         * @return {Object} JSON object
         */
        parseJSON: function (json) {
          let output;
          if (typeof json === "object") {
            output = json;
          } else {
            try {
              output = JSON.parse(json);
            } catch (exc) {
              $log.error("[JSON] Error parsing json string: '" + json + "'", exc);
              output = $utilities.evalJSON(json);
            }
          }
          return output;
        },
        /**
         * Returns a random number to avoid cache
         * @return {number} Cache number
         */
        cache: function () {
          return Math.floor(Math.random() * 1000000000);
        },
        /**
         * Unique id generation
         * @return {String} UID generated
         */
        getUID: () => getUID(),
        /**
         * Returns the number of occurrences of a substring inside a string
         * @public
         * @param {String} text Text to search occurrences
         * @param {String} sub  Substring to search for occurrences
         * @return {number} Number of occurrences of sub inside text
         */
        count: function (text, sub) {
          return text.split(sub).length - 1;
        },
        /**
         * Encodes a char to URL format
         * @param {String} c Char to encode
         * @return Char encoded
         */
        urlEncodeCharacter: function (c) {
          return '%' + c.charCodeAt(0).toString(16);
        },
        /**
         * Decodes a char from URL format
         * @param {String} c Char to decode
         * @return Char decoded
         */
        urlDecodeCharacter: function (c) {
          return String.fromCharCode(parseInt(c, 16));
        },
        /**
         * Encodes a string into URL format
         * @param {String} s String to encode
         * @return String encoded
         */
        urlEncode: function (s) {
          return encodeURIComponent(s).replace(/\%20/g, '+').replace(/[!'()*~]/g, function (e) {
            return $utilities.urlEncodeCharacter(e);
          });
        },
        /**
         * Decodes a string from URL format
         * @param {String} s String to decode
         * @return String decoded
         */
        urlDecode: function (s) {
          return decodeURIComponent(s.replace(/\+/g, '%20')).replace(/\%([0-9a-f]{2})/g, function (e) {
            return $utilities.urlDecodeCharacter(e);
          });
        },
        /**
         * Encodes a string
         * @param {String} s String to encode
         * @return String encoded
         */
        encodeSymetric: function (s) {
          var out = Base64.encode(s, false);
          return $utilities.urlEncode(out);
        },
        /**
         * Decodes a string
         * @param {String} s String to decode
         * @return String decoded
         */
        decodeSymetric: function (s) {
          var out = $utilities.urlDecode(s);
          return Base64.decode(out, false);
        },
        /**
         * Returns true if a variable is null
         * @param {String} n Variable to test
         * @return {boolean} String is null or undefined
         */
        isNull: function (n) {
          return n === null || n === undefined;
        },
        /**
         * Returns true if a variable is null or empty
         * @param {String} n Variable to test
         * @return {boolean} String is null or undefined
         */
        isEmpty: function (n) {
          return $utilities.isNull(n) || $.trim(n) === "";
        },
        /**
         * Removes blanks from string header and footer
         * @param {String} value String to trim
         * @return {String} String trimmed
         */
        trim: function (value) {
          if (!$utilities.isString(value)) {
            return value;
          }
          if (!String.prototype.trim) {
            return value.replace(/^\s*/, '').replace(/\s*$/, '');
          }
          return String.prototype.trim.apply(value);
        },
        /**
         * Evaluates a json string without key quotes
         * @param {string} jsonString JSON string
         * @returns {Object} json evaluated
         */
        evalJSON: function (jsonString) {
          return $utilities.eval("(" + jsonString + ")");
        },
        /**
         * Evaluates a expression
         * @param {string} expression string
         * @returns {Object} expression evaluated
         */
        eval: function (expression) {
          let ctor = "constructor";
          return $window.constructor[ctor][ctor]("return " + expression + ";")();
        },
        /**
         * Evaluates a formule and retrieves the result
         * @param {string} formule Formule string
         * @param {Object} values
         * @returns {Object} expression evaluated
         */
        formule: function (formule, values) {
          // Get formule
          let value = null;

          // Replace formule parameters
          _.each(values, function (value, valueId) {
            formule = formule.replace(new RegExp("\\[" + valueId + "\\]", "ig"), value);
          });
          formule = formule.replace(new RegExp("#", "ig"), "\"");

          // Eval formule
          try {
            value = $utilities.eval(formule);
          } catch (exc) {
            $log.error("[FORMULE] Formule: " + formule, {params: values, exception: exc});
          }

          return value;
        },
        /**
         * Evaluates a json string without key quotes
         * @param {string} booleanString Boolean string
         * @returns {boolean} boolean evaluated
         */
        parseBoolean: function (booleanString) {
          let booleanValue = false;
          let boolean = $utilities.trim(String(booleanString)).toLowerCase();
          if (boolean === "true" || boolean === "1") {
            booleanValue = true;
          }
          return booleanValue;
        },
        /**
         * Encode parameters
         * @param {Object} parameters Parameter map
         * @param {boolean} encode Encode parameters
         * @returns {String} Parameter list
         */
        encodeParameters: function (parameters, encode) {
          let parameterString = $utilities.stringifyJSON(parameters);
          return encode ? $utilities.encodeSymetric(parameterString) : parameterString;
        },
        /**
         * Decode data
         * @param {Object} data Data received
         * @param {boolean} decode Decode data
         * @returns {String} Parameter list
         */
        decodeData: function (data, decode) {
          return decode === "1" ? $utilities.parseJSON($utilities.decodeSymetric(data)) : $utilities.parseJSON(data);
        },
        /**
         * Sanitize settings
         * @param {object} unsanitized settings
         * @param {object} definition settings definition
         * @returns {object} settings sanitized
         */
        sanitize: function (unsanitized, definition) {
          let sanitized = {};
          _.each(unsanitized, function (value, setting) {
            if (typeof definition[setting] === 'string') {
              sanitized[setting] = $utilities.sanitizeSetting(value, definition[setting]);
            } else {
              // Define setting object
              if (!(setting in sanitized)) {
                sanitized[setting] = {};
              }
              // Store subsettings in setting
              _.each(definition[setting], function (sType, sSetting) {
                sanitized[setting][sSetting] = $utilities.sanitizeSetting(unsanitized[setting][sSetting], sType);
              });
            }
          });
          return sanitized;
        },
        /**
         * Sanitize the value
         * @param {mixed} value
         * @param {string} type
         * @returns {mixed} value sanitized
         */
        sanitizeSetting: function (value, type) {
          switch (type) {
            case 'int':
              return parseInt(value, 10);
            case 'boolean':
              return $utilities.parseBoolean(value);
            default:
              return value;
          }
        },
        /**
         * Define a list of action listeners
         * @param {array} listeners
         * @param {object} actions
         * @param {object} scope
         * @param {object} executor
         */
        defineActionListeners: function (listeners, actions, $scope, service) {
          Object.entries(actions).forEach(([key, value]) => {
            listeners.push($scope.$on("/action/" + key, (event, action) => {
              value["service"] = service;
              value["scope"] = $scope;
              $utilities.resolveAction(action, value);
            }));
          });
        },
        /**
         * Clear all the listeners
         * @param {array} newListeners
         */
        clearListeners: function (listeners) {
          // Destroy event listeners
          pendingListeners = (pendingListeners || []).concat(listeners);
          clearPendingListenersDebounced();
        },
        /**
         * Clear all pending listeners
         */
        clearPendingListeners: function () {
          // Destroy pending listeners
          pendingListeners.forEach(fn => fn && fn());
          pendingListeners.length = 0;
        },
        /**
         * Check two addresses
         * @param {type} address1
         * @param {type} address2
         * @param {array} check Elements to check in address (default is all)
         * @returns {boolean} Launch or not
         */
        checkAddress: function (address1, address2, check = ["view", "component", "row", "column"]) {
          return check.reduce((launch, check) => launch && (String(address1[check]) === String(address2[check])), true);
        },
        /**
         * Resolve the action if matches
         * @param {type} action
         * @param {object} parameters
         */
        resolveAction: function (action, parameters) {
          let scope = parameters.scope || {};
          let service = parameters.service || {};
          let check = parameters.check;

          // Launch action
          if ($utilities.checkAddress(action.callbackTarget, service.address || {}, check)) {
            // Launch method
            service[parameters.method](action.parameters, scope, action.callbackTarget);

            // Finish action
            angular.element(document).injector().get('ActionController').acceptAction(action);
          }
        },
        /**
         * Animate a
         * @param {object} node Noe
         * @param {object} animation Animation
         * @param {number} time Time
         * @param {function|null} endFunction Function on end
         */
        animateCSS: function (node, animation, time, endFunction = null) {
          // Css animation
          node.css({
            "-webkit-transition-duration": time + "ms !important",
            "-moz-transition-duration": time + "ms !important",
            "-ms-transition-duration": time + "ms !important",
            "-o-transition-duration": time + "ms !important",
            "transitionDuration": time + "ms !important"
          });
          node.addClass('animateEaseTransition');
          node.css(animation);
          endFunction && $timeout(endFunction, time);
        },
        /**
         * Animate a node with javascript
         * @param {object} node Noe
         * @param {object} animation Animation
         * @param {number} time Time
         * @param {function|null} endFunction Function on end
         */
        animateJavascript: function (node, animation, time, endFunction = null) {
          // Javascript animation
          endFunction ? node.animate(animation, time, endFunction) : node.animate(animation, time);
        },
        /**
         * Get size string
         * @param {number} size File size
         * @private
         */
        getSizeString: function (size) {
          let sizeString = "";
          // Bytes
          if (size < 1024) {
            sizeString = Math.floor(size) + "B";
            // KBytes
          } else if (size < Math.pow(1024, 2)) {
            sizeString = (Math.floor(size * 10 / 1024) / 10) + "KB";
            // MBytes
          } else if (size < Math.pow(1024, 3)) {
            sizeString = (Math.floor(size * 10 / Math.pow(1024, 2)) / 10) + "MB";
            // GBytes
          } else if (size < Math.pow(1024, 4)) {
            sizeString = (Math.floor(size * 10 / Math.pow(1024, 3)) / 10) + "GB";
            // TBytes
          } else if (size < Math.pow(1024, 5)) {
            sizeString = (Math.floor(size * 10 / Math.pow(1024, 4)) / 10) + "TB";
            // PBytes
          } else {
            sizeString = (Math.floor(size / Math.pow(1024, 5)) / 10) + "PB";
          }
          return sizeString;
        },
        /**
         * Download a file
         * @param {type} file
         */
        downloadFile: function (file) {
          $utilities.publish("download-file", file);
        },
        /**
         * Remove children (fast)
         * @param {object} node
         * @returns {undefined}
         */
        removeChildren: function (node) {
          var last;
          while (last = node.lastChild) {
            node.removeChild(last);
          }
        },
        /**
         * Manage parameter as array
         * @param {Mixed} value
         * @return {Array} value as array
         */
        asArray: function (value) {
          return $utilities.isEmpty(value) ? [] : [...(angular.isArray(value) ? value : [value])];
        },
        /**
         * Get component value depending on size: 0 -> null, 1 -> value, n -> list
         * @param {Array} list
         * @result {Mixed}
         */
        componentValue: function(list) {
          return list.reduce((items, item) => items == null ? item : angular.isArray(items) ? [...items, item] : [items, item], null);
        },
        /**
         * Get first defined value
         * @returns {*}
         */
        getFirstDefinedValue: function() {
          return [...arguments].reduce((items, item) => items !== undefined ? items : item, undefined);
        },
        /**
         * Publish into a channel
         * @param {string} channel
         * @param {object} parameters
         */
        publish: function (channel, parameters = {}) {
          $utilities.publishFromScope(channel, parameters, $rootScope);
        },
        /**
         * Publish into a channel from a specific scope
         * @param {String} channel
         * @param {Object} parameters
         * @param {Object} scope
         */
        publishFromScope: function (channel, parameters, scope) {
          scope.$broadcast(channel, parameters);
        },
        /**
         * Publish into a channel
         * @param {String} channel
         * @param {Object} parameters
         */
        publishDelayed: function (channel, parameters = {}) {
          $timeout(() => $utilities.publish(channel, parameters));
        },
        /**
         * $timeout wrapper
         */
        timeout: $timeout,
        /**
         * $interval wrapper
         */
        interval: $interval,
        /**
         * $q wrapper
         */
        q: $q,
        /**
         * Publish into a channel from a specific scope
         * @param {String} channel
         * @param {Object} parameters
         * @param {Object} scope
         */
        publishDelayedFromScope: function (channel, parameters, scope) {
          $timeout(() => $utilities.publishFromScope(channel, parameters, scope));
        },
        /**
         * Emit into a channel from a specific scope
         * @param {String} channel
         * @param {Object} parameters
         * @param {Object} scope
         */
        emitDelayedFromScope: function (channel, parameters, scope) {
          $timeout(function () {
            $utilities.emitFromScope(channel, parameters, scope);
          });
        },
        /**
         * Emit into a channel from a specific scope
         * @param {String} channel
         * @param {Object} parameters
         * @param {Object} scope
         */
        emitFromScope: function (channel, parameters, scope) {
          scope.$emit(channel, parameters);
        },
        /**
         * Check if node is visible
         * @param {Object} obj
         * @return Node is visible
         */
        isVisible: function (node) {
          return node.offsetParent && node.offsetHeight;
        },
        /**
         * Compare if two values are equal
         * @param {type} value1
         * @param {type} value2
         * @returns {Boolean}
         */
        compareEqualValues: function (value1, value2) {
          let equals;
          if (typeof value1 === typeof value2) {
            equals = value1 === value2;
          } else {
            equals = ($utilities.isEmpty(value1) && $utilities.isEmpty(value2)) ||
              String(value1) === String(value2);
          }
          return equals;
        },
        /**
         * Stop event propagation and default action
         * @param {Object} event
         * @return Object length
         */
        stopPropagation: function (event, preventDefault) {
          // Cancel event propagation
          if (event.stopPropagation) {
            event.stopPropagation();
          }
          // Cancel default action
          if (event.preventDefault && preventDefault) {
            event.preventDefault();
          }
          event.cancelBubble = true;
          event.returnValue = false;
        },
        /**
         * Retrieve cell attribute
         * @param {Object} mode
         * @param {String} attribute
         * @return {Mixed} value
         */
        getCellAttribute: function (model, attribute) {
          return angular.isObject(model) && model != null ? model[attribute] : model;
        },
        /**
         * Retrieve application context path
         * @return Context path
         */
        getContextPath: function() {
          return $location.absUrl().substr(0, $location.absUrl().lastIndexOf($location.path()));
        },
        /**
         * Retrieve application context path
         * @return Context path
         */
        getState: function(location, reload) {
          let state = {};
          let locationFixed = location.replace($utilities.getContextPath(), "");
          let locationData = locationFixed.split('/');
          let maxState = null;
          if (locationFixed.indexOf('/public') !== -1) {
            // Public state
            state.to = 'public.screen';
            state.parameters = {
              screenId: locationData[3],
              subScreenId: locationData[4]
            };
            maxState = "subScreenId";
          } else if (locationFixed.indexOf('/private') !== -1) {
            // Private state
            state.to = 'private.screen';
            state.parameters = {
              screenId: locationData[3],
              subScreenId: locationData[4]
            };
            maxState = "subScreenId";
          } else if (locationFixed.indexOf('/screen') !== -1) {
            // Global state
            state.to = 'global';
            state.parameters = {
              screenId: locationData[2]
            };
            maxState = "screenId";
          } else {
            // Index
            state.to = 'index';
            state.parameters = {};
          }

          if (reload && maxState != null) {
            state.parameters[maxState] += "?" + Math.floor(Math.random() * 100);
          }

          return state;
        },
        /**
         * Retrieve application context
         * @return Context
         */
        getCurrentContext(view) {
          switch (view) {
            case "base":
              return "";
            case "report":
              return $location.path()
                       .split("/")
                       .slice(1, 4)
                       .join("/");
          }
        },
        /**
         * Retrieve main view
         * @return Main view
         */
        getMainView(view) {
          let params = $ngRedux.getState().router.currentParams || {};
          return "subScreenId" in params ? "report" : "base";
        },
        /**
         * Debounce a function call
         * @param {function} func Function to launch
         * @param {number} wait Time to wait
         */
        debounce: function(func, wait) {
          return _.debounce(() => $timeout(func), wait);
        },
        /**
         * Execute when visible
         * @param $scope
         * @param node
         * @param func
         */
        executeWhenVisible($scope, node, func) {
          let endWatch = $scope.$watch(() => $utilities.isVisible(node), visible => visible && (endWatch() || $utilities.timeout(func)));
        },
        /**
         * Generate an endpoint url based on arguments
         */
        generateEndpointUrl: function() {
          let endpoint = "";
          Array.prototype.slice.call(arguments).filter(v => !$utilities.isEmpty(v)).forEach(v => endpoint += "/" + v);
          return endpoint;
        },
        /**
         * Check if both url's are the same regardless the parameters
         * @return Context path
         */
        sameUrl: function(url1, url2) {
          let firstUrl = url1 || "";
          let secondUrl = url2 || "";
          return firstUrl.split("?")[0] === secondUrl.split("?")[0];
        }
      };
      clearPendingListenersDebounced = _.debounce($utilities.clearPendingListeners, 10);
      return $utilities;
    }
  ]);
