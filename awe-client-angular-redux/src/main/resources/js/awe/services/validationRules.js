import { aweApplication } from "../awe";
import _ from "lodash";

// Application default utilities
aweApplication.factory('ValidationRules',
  ['AweSettings', '$translate', 'AweUtilities', 'Control',
    function ($settings, $translate, $utilities, $control) {
      // Retrieve default settings
      var patterns = {
        TEXT: /^[A-Za-z]+$/,
        TEXT_WHITESPACES: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
        NUMBER: /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/,
        INTEGER: /^-?\d+$/,
        DIGITS: /^\d+$/,
        EMAIL: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        DATE: /^\d{1,2}\/\d{1,2}\/\d{4}$/,
        TIME: /^([0-1]\d|2[0-3]):([0-5]\d):([0-5]\d)$/
      };

      /**
       * Retrieve external parameter values
       * @param {type} rule Rule to retrieve the parameters
       * @param {type} view
       */
      function retrieveExternalParameters(rule, view) {
        let parameterValue = null;
        if ("criterion" in rule) {
          parameterValue = $control.getSelectedValues({view: view, component: rule.criterion});
        } else if ("setting" in rule) {
          parameterValue = $settings.get(rule.setting);
        }
        return parameterValue;
      };

      /**
       * Retrieve a value or null if it is not valid
       * @param {*} value
       * @param {object} address
       * @return {object} Group name
       */
      function getRequiredValue(value, address) {
        // Get controller
        let requiredValue = null;
        let component = $control.getComponent(address);

        if ("group" in component.attributes) {
          // Retrieve group values (for radio buttons)
          requiredValue = $control.getGroupSelectedValues(component.attributes.group);
        } else if (angular.isArray(value)) {
          // Retrieve list values (for multiple)
          if (value.length !== 0) {
            requiredValue = value;
          }
          // Retrieve single values (default)
        } else {
          requiredValue = value;
        }
        return requiredValue;
      };
      /**
       * Extract the parameters to compare
       * @param {type} ruleMethod rule method
       * @param {type} value Value to compare
       * @param {type} rule Rule to retrieve the parameters
       * @param {type} address
       */
      function extractParameters(ruleMethod, value, rule, address) {
        // Get parameters from rule
        let parameters = getParameters(value, rule, address);

        // Format comparison values
        return formatParameters(ruleMethod, value, address, parameters);
      };

      /**
       * Get parameters
       */
      function getParameters(value, rule, address) {
        let parameters = {values: {value1: value}};
        if (typeof rule === "object") {
            parameters = {...parameters, rule};
            if ("from" in rule && "to" in rule) {
              parameters.values.from = retrieveExternalParameters(rule["from"], address.view);
              parameters.values.to = retrieveExternalParameters(rule["to"], address.view);
            } else if ("criterion" in rule || "setting" in rule) {
              parameters.values.value2 = retrieveExternalParameters(rule, address.view);
            }
        } else {
          parameters.values.value2 = rule;
        }
        return parameters;
      }

      /**
       * Format parameters
       */
      function formatParameters(method, value, address, parameters) {
        // Format comparison values
        switch (parameters.type) {
          case "float":
            _.each(parameters.values, (value, valueId) => {
              parameters.values[valueId] = !$utilities.isEmpty(value) ? parseFloat(value) : null;
            });
            break;
          case "integer":
            _.each(parameters.values, (value, valueId) => {
              parameters.values[valueId] = !$utilities.isEmpty(value) ? parseInt(value, 10) : null;
            });
            break;
          case "date":
            _.each(parameters.values, (value, valueId) => {
              parameters.values[valueId] = !$utilities.isEmpty(value) ? moment(value, "DD/MM/YYYY") : null;
            });
            break;
          default:
            resolveSpecificMethodParameters(method, value, address, parameters);
        }
        return parameters;
      }

      /**
       * Resolve parameters with specific methods
       */
      function resolveSpecificMethodParameters(method, value, address, parameters) {
        switch (method) {
          case "required":
            parameters.values.value1 = getRequiredValue(value, address);
            break;
          case "checkAtLeast":
            parameters.values.value1 = $control.getGroupSelectedValues(address).length;
            break;
          case "maxlength":
          case "minlength":
            parameters.values.value2 = !$utilities.isEmpty(parameters.values.value2) ? parseInt(parameters.values.value2, 10) : null;
            break;
          default:
        }
      }

      /**
       * Fornat a message with parameters
       * @param {type} message
       * @param {type} parameters
       * @returns {unresolved}
       */
      function formatMessage(message, parameters) {
        let messageFormatted = $translate.instant(message);
        _.each(parameters.values, function (value, key) {
          let formattedValue = value;
          if (!$utilities.isEmpty(value) && value._isAMomentObject) {
            formattedValue = formattedValue.format("DD/MM/YYYY");
          }
          messageFormatted = messageFormatted.replace("{" + key + "}", formattedValue);
        });
        return messageFormatted;
      };
      const ValidationRules = {
        validate: function (ruleMethod, value, rule, address) {
          let parameters = extractParameters(ruleMethod, value, rule, address);
          return ValidationRules[ruleMethod](parameters);
        },
        required: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_REQUIRED", {});
          return $utilities.isEmpty(parameters.values.value1) && parameters.values.value2 ? error : null;
        },
        text: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_TEXT", {});
          let passed = new RegExp(patterns.TEXT).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        textWithSpaces: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_TEXT_WHITESPACES", {});
          let passed = new RegExp(patterns.TEXT_WHITESPACES).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        number: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_NUMBER", {});
          let passed = new RegExp(patterns.NUMBER).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        integer: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_INTEGER", {});
          let passed = new RegExp(patterns.INTEGER).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        digits: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_DIGITS", {});
          let passed = new RegExp(patterns.DIGITS).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        email: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_EMAIL", {});
          let passed = new RegExp(patterns.EMAIL).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        date: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_DATE", {});
          let passed = new RegExp(patterns.DATE).test(String(parameters.values.value1));
          if (passed) {
            let d = moment(parameters.values.value1, 'DD/MM/YYYY');
            passed = d.isValid();
          }
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        time: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_TIME", {});
          let passed = new RegExp(patterns.TIME).test(String(parameters.values.value1));
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        eq: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_EQUAL", parameters);
          let passed = parameters.values.value1 === parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        ne: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_NOT_EQUAL", parameters);
          let passed = parameters.values.value1 !== parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        lt: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_LESS_THAN", parameters);
          let passed = parameters.values.value1 < parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        le: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_LESS_OR_EQUAL", parameters);
          let passed = parameters.values.value1 <= parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        gt: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_GREATER_THAN", parameters);
          let passed = parameters.values.value1 > parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        ge: function (parameters) {
          var error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_GREATER_OR_EQUAL", parameters);
          var passed = parameters.values.value1 >= parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        mod: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_DIVISIBLE_BY", parameters);
          let passed = parameters.values.value1 % parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        range: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_RANGE", parameters);
          let passed = parameters.values.value1 >= parameters.values.from && parameters.values.value1 <= parameters.values.to;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        equallength: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_EQUAL_LENGTH", parameters);
          let passed = String(parameters.values.value1).length = parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        maxlength: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_MAXLENGTH", parameters);
          let passed = String(parameters.values.value1).length <= parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        minlength: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_MINLENGTH", parameters);
          let passed = String(parameters.values.value1).length >= parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        pattern: function (parameters) {
          let pattern = parameters.values.value2 || $settings.get("passwordPattern");
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_PATTERN", {});
          let passed = new RegExp(pattern).test(parameters.values.value1);
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        checkAtLeast: function (parameters) {
          let error = formatMessage(parameters.message || "VALIDATOR_MESSAGE_CHECK_AT_LEAST", parameters);
          let passed = parameters.values.value1 >= parameters.values.value2;
          return $utilities.isEmpty(parameters.values.value1) || passed ? null : error;
        },
        invalid: function (parameters) {
          return parameters.message;
        }
      };
      return ValidationRules;
    }
  ]);
