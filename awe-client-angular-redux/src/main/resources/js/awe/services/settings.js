import { aweApplication } from "../awe";
import { updateSettings, DefaultSettings } from "../actions/settings";

// Imports
import "./storage";
import "./utilities";
import "./locals";
import { stateGo } from 'redux-ui-router';

// Application default settings
aweApplication.factory('AweSettings', ['Storage', '$translate', '$log', 'AweUtilities', '$http', '$location', '$rootScope', '$ngRedux', '$injector', '$state',
  function ($storage, $translate, $log, $utilities, $http, $location, $scope, $ngRedux, $injector, $state) {
    const tokenKey = "token";
    const initialize = $utilities.q.defer();
    const currentSettings = DefaultSettings;
    let $connection = null;
    const $settings = {
      /**
       * Initialize settings from server
       */
      init: function () {
        // Connect settings from redux
        $ngRedux.connect((state) => state.settings)(currentSettings);

        // Initialize storage
        $storage.init();

        // Set initial settings
        $settings.update(DefaultSettings);

        // Load settings from server
        $http.defaults.headers.common = {...$http.defaults.headers.common, ...$settings.getAuthenticationHeaders()};
        $http({
          method: 'POST',
          url: `${$settings.get("pathServer")}settings`
        }).then((response) => {
          let newSettings = angular.fromJson(response.data);
          $settings.update(newSettings);
          $settings.settingsLoaded(newSettings);
        }).catch(function (error) {
          $log.error("FATAL ERROR: Application settings retrieval failure", error);
        });
        return initialize.promise;
      },
      /**
       * Initialize settings from server
       * @param {object} settings Application settings
       */
      settingsLoaded: function (settings) {
        // Retrieve settings and set initial language
        let language = $settings.getLanguage();

        // Start (or reconnect) server connection
        $connection = $injector.get('Connection');

        $connection.init(settings.pathServer, settings.encodeTransmission, settings.cometUID)
          .then(() => {
            // Store token
            $settings.setToken(settings.cometUID);

            // Store updated and language
            $storage.putRoot("language", language);

            // Set language
            $settings.changeLanguage(language, true);

            // Store connection
            $storage.putRoot("connection", settings.connectionProtocol);

            // Load current state
            let initialState = $utilities.getState(settings.reloadCurrentScreen ? settings.initialURL : location.href);
            $settings.update({reloadCurrentScreen: false});

            // Go to initial state
            $ngRedux.dispatch(stateGo(initialState.to, initialState.parameters));

            // Resolve initialization
            initialize.resolve();
          });
      },
      /**
       * Retrieve setting
       * @param {string} setting name
       * @returns {object} settings
       */
      get: function (setting) {
        return currentSettings[setting];
      },
      /**
       * Update setting with a value
       * @param {string} setting
       * @param {string|number|object|array} value
       */
      set: function (setting, value) {
        return $ngRedux.dispatch(updateSettings({[setting]: value}));
      },
      /**
       * Update settings
       * @param {object} settings
       */
      update: function (settings) {
        return $ngRedux.dispatch(updateSettings(settings));
      },
      /**
       * Retrieve authentication headers
       * @returns {{Authorization: (*|token)}}
       */
      getAuthenticationHeaders: function() {
        return {
          'Authorization': $settings.getToken(),
          'Content-Type': 'application/json'
        };
      },
      /**
       * Store session token
       * @param {String} token Token to set
       */
      setToken: function (token) {
        $storage.putSession(tokenKey, token);
      },
      /**
       * Store session token
       * @return token
       */
      getToken: function () {
        if ($storage.hasSession(tokenKey)) {
          return $storage.getSession(tokenKey);
        }
        return $settings.get("cometUID");
      },
      /**
       * Clear session token
       * @return token
       */
      clearToken: function () {
        $storage.removeSession(tokenKey);
      },
      /**
       * Retrieve session token as object
       * @returns {Object} Session token
       */
      getTokenObject: function () {
        return {
          [$settings.get("connectionId")]: $settings.getToken()
        };
      },
      /**
       * Retrieve session token as string
       * @returns {Object} Session token as string
       */
      getTokenString: function () {
        return $settings.get("connectionId") + "=" + $settings.getToken();
      },
      /**
       * Retrieve session token
       * @returns {Object} Session token
       */
      getLanguage: function () {
        return $settings.get("language") === null ? null : $settings.get("language").toLowerCase();
      },
      /**
       * Change current language;
       * @param {string} language Language to set
       * @param {boolean} forceChange Force change language
       */
      changeLanguage: function (language, forceChange) {
        if (language !== null) {
          let newLanguage = language.toLowerCase();
          if (newLanguage !== $settings.getLanguage() || forceChange) {
            // Change language settings
            $ngRedux.dispatch(updateSettings({language: newLanguage}));

            // Change locals
            $translate.use($settings.get("language")).then(function () {
              // Launch local functions
              $utilities.publish("launchLocalFunctions");

              // Report language changed
              $log.info("Language changed to " + newLanguage);

              // Apply changes
              $utilities.publish("languageChanged", newLanguage);
            });
          }
        }
      },
      /**
       * Preload angular templates
       */
      preloadTemplates: function () {
        // Preload templates
        let $serverData = $injector.get('ServerData');
        [ {path: 'grid/header'},
          {path: 'grid/cell'},
          {path: 'grid/footer'},
          {path: 'grid/row'},
          {path: 'grid/pagination'},
          {path: 'grid/viewport', name: 'ui-grid/uiGridViewport'},
          {path: 'grid/renderContainer', name: 'ui-grid/uiGridRenderContainer'},
          {path: 'grid/headerFilter'},
          {path: 'grid/cellRowNumber'},
          {path: 'grid/cellCheckbox'},
          {path: 'grid/cellTreeIcons', name: "ui-grid/treeBaseRowHeaderButtons"},
          {path: 'grid/headerTreeIcons', name: "ui-grid/treeBaseExpandAllButtons"},
          {path: 'grid/selectionHeaderCell', name: "ui-grid/selectionHeaderCell"},
          {path: 'grid/headerCell', name: "ui-grid/ui-grid-header"},
          {path: 'grid/headerCheckbox'}
        ].forEach(template => $serverData.preloadAngularTemplate(template));
      }
    };
    return $settings;
  }]);