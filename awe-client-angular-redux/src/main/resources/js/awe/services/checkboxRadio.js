import { aweApplication } from "../awe";
import { ComponentStatus } from "../actions/components";
const { STATUS_INITIALIZED } = ComponentStatus;
import "./criterion";

// DateTime service
aweApplication.factory('CheckboxRadio',
  ['Control', 'Criterion', '$ngRedux',
    function ($control, $criterion, $ngRedux) {
      const $checkboxRadio = {
        /**
         * Initialize as checkbox
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initCheckbox: function($ctrl, $scope) {
          // Initialize as criterion
          $criterion.init($ctrl, $scope);

          /**
          * On change method (From COMPONENT to REDUX)
          */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: $ctrl.model.values.map(option => ({...option, selected: !!($ctrl.value || {}).selected})) });
          };
        },
        /**
         * Initialize as radio
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        initRadio: function($ctrl, $scope) {
          // Initialize as criterion
          $criterion.init($ctrl, $scope);

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
           * Post initialization (Generate model component for shared data)
           */
          $ctrl.onInitialize = function() {
            // Generate new model address
            let modelAddress = {view: $ctrl.address.view, component: $ctrl.attributes.group};

            // Get component model if defined
            let componentValues = (($control.getComponent(modelAddress).model || {}).values || []);

            // Generate a new component
            $control.changeComponentDebounced(modelAddress, {status: STATUS_INITIALIZED, attributes: {id: $ctrl.attributes.group}, address: modelAddress, model: {values: [...componentValues, ...$ctrl.model.values]}});

            let getModelUpdate = (state => ({model: (state.components[$ctrl.attributes.group] || {}).model, uid: state.components[$ctrl.attributes.group]}));
            $ctrl.events.push($ngRedux.connect(getModelUpdate)(onModelUpdate));
          }

          /**
          * On change method (From COMPONENT to REDUX)
          */
          $ctrl.onChange = function() {
            let value = $ctrl.model.values.map(value => value.value);

            // Update model
            $control.changeModel($ctrl.address, { selected: value });

            // Update global model
            $control.changeModel({view: $ctrl.address.view, component: $ctrl.attributes.group}, { selected: value });
          }

          /**
           * On update model and attributes
           * @param {Object} state New state
           */
          function onModelUpdate(state) {
            if (!state.uid) return;

            // Model bindings
            let value = state.model.values
                          .filter(option => option.selected)
                          .reduce((selected, option) => option.value, null);

            // Map selected model
            $ctrl.model.values
              .map(item => ({...item, selected: value === item.value}));

            // Update state
            $ctrl.updateModel && $ctrl.updateModel(state);
          }
        }
      };
      return $checkboxRadio;
    }
  ]);
