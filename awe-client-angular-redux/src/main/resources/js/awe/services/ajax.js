import { aweApplication } from "../awe";

/**
 * Service to launch server calls
 * @param {String} Service name
 * @param {Array} Injection call
 */
aweApplication.factory('Ajax',
  ['AweUtilities', '$log', '$http', 'ActionController', 'AweSettings',
    /**
     * Retrieve the comet connection object
     * @param {AweUtilities} $utilities Awe Utilities service
     * @param {$log} $log Log service
     * @param {$http} $http Http request service
     * @param {ActionController} $actionController Action controller
     * @param {AweSettings} $settings Awe Settings
     * @returns {Object} Ajax connection
     */
    function ($utilities, $log, $http, $actionController, $settings) {

      // Service variables;
      let connectionType = 'Ajax';
      let connected = true;
      let encodeTransmission = false;
      let serverPath = "";

      const $ajax = {
        /**
         * Retrieve if connection is active
         * @private
         */
        isConnected: function () {
          return connected;
        },
        /**
         * Init connection
         * @param path Server path
         * @param encode Encode transmission
         */
        init: function (path, encode) {
          serverPath = path;
          encodeTransmission = encode;
        },
        /**
         * Send message
         * @param {String} message Message received
         */
        sendMessage: function (message) {
          let action = message.action;
          let target = message.target ? message.target : {};
          action.callbackTarget = target;

          // Log message
          $log.info("[" + connectionType + "] Sending message", {message: message});

          // Send message
          let promise = $ajax.send(message)
            .then(function (response) {
              // Manage message and hide loading bar
              $ajax.manageMessage(response, action);
            })
            .catch(function (response) {
              // Manage message and hide loading bar
              $ajax.manageError(response, action);
            })
            .finally(function () {
              //$loadingBar.endTask();
            });

          // Show loading bar
          if (!action.silent) {
            //$loadingBar.startTask();
          }

          return promise;
        },
        /**
         * Send message
         * @param {Object} message Send message parameters
         */
        send: function (message) {
          // Send data
          let url = $ajax.getActionUrl(message.values.serverAction, message.values.targetAction);
          $log.info(`Sending POST request to ${url}`)
          return $ajax.httpRequest({
            method: 'POST',
            url: url,
            data: message.values
          });
        },
        /**
         * Send get message
         * @param {Object} url Send message url
         * @param {Object} expectedContent Content type expected
         */
        get: function (url, expectedContent) {
          return $ajax.httpRequest({
            method: 'GET',
            url: url
          }, expectedContent);
        },
        /**
         * Send post message
         * @param {String} url Message url
         * @param {Object} data Post message data
         * @param {Object} expectedContent Content type expected
         */
        post: function (url, data, expectedContent) {
          return $ajax.httpRequest({
            method: 'POST',
            url: url,
            data: data
          }, expectedContent);
        },
        /**
         * Send request
         * @param {String} parameters Request parameters
         * @param {Object} expectedContent Content type expected
         */
        httpRequest: function (parameters, expectedContent) {
          // Set content-type if defined
          if (expectedContent) {
            parameters["headers"] = {
              "Accept": expectedContent
            };
          }

          // Send data
          return $http(parameters);
        },
        /**
         * Retrieve message url
         * @param {Object} message Send message parameters
         * @return {String} Message url
         */
        getUrl: function (message) {
          return $ajax.getRawUrl() + "?p=" + $utilities.encodeParameters(message, false);
        },
        /**
         * Retrieve raw url
         * @return {String} context path + server path
         */
        getRawUrl: function () {
          return $utilities.getContextPath() + serverPath;
        },
        /**
         * Retrieve action's url
         * @param {String} actionId Action name
         * @param {String} targetId Action identifier
         * @return {String} Action url
         */
        getActionUrl: function (actionId, targetId) {
          return $ajax.getRawUrl() + "/action/" + actionId + (($utilities.isEmpty(targetId)) ? "" : "/" + targetId);
        },
        /**
         * Retrieve serialized parameters
         * @param {Object} message Send message parameters
         * @return {Object} serialized parameters
         */
        getSerializedParameters: function (message) {
          return message;
        },
        /**
         * Retrieve encoded parameters
         * @param {Object} message Send message parameters
         * @return {Object} Encoded parameters
         */
        getEncodedParameters: function (message) {
          return {
            [$settings.get("encodeKey")]:  $utilities.encodeParameters(message, false),
            [$settings.get("connectionId")]: $settings.getToken()
          };
        },
        /**
         * Receive message
         * @param {Object} message Message received
         * @param {Object} action Action launched
         */
        manageMessage: function (message, action) {
          // Decode data
          var data = $utilities.decodeData(message.data, false);

          // Only accept action if is alive
          if ($actionController.isAlive(action)) {

            // Add actions attributes to retrieved data if not defined previously
            data.silent = data.silent || action.silent;
            data.async = data.async || action.async;

            // Finish call action
            $actionController.acceptAction(action);

            // Add action list
            if (angular.isArray(data)) {
              $actionController.addActionList(data, false, {address: action.callbackTarget, context: action.context});
            }
          }
        },
        /**
         * Receive error
         * @param {Object} error Error received
         * @param {Object} action Action launched
         */
        manageError: function (error, action) {
          var target = action.callbackTarget;

          // Finish call action (cancel)
          $actionController.closeAllActions();

          // Generate message title
          var title = "Connection error";

          // Launch message
          var endLoad = {type: 'end-load',
            callbackTarget: target,
            parameters: {}
          };
          var actions = [endLoad];
          if (error.data !== null) {
            var message = {type: 'message', parameters: {
                type: "error",
                title: title,
                message: "[" + connectionType + "] " + error.data
              }};
            actions.push(message);
          }
          // Log error output
          $log.error("[" + connectionType + "] " + title, error);
          $actionController.addActionList(actions, false, {address: target, context: action.context});
        }
      };
      return $ajax;
    }
  ]);