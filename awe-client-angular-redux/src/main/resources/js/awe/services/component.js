import { aweApplication } from "../awe";

import { ComponentStatus } from "../actions/components";
const { STATUS_DEFINED, STATUS_COMPILED, STATUS_INITIALIZED } = ComponentStatus;

import _ from 'lodash';

// Criterion services
aweApplication.factory('Component',
  ['Control', '$ngRedux', 'AweSettings', 'AweUtilities', 'ActionController',
    /**
     * Criterion
     */
    function ($control, $ngRedux, $settings, $utilities, $actionController) {
      return {
        init: function($ctrl, $scope) {
          let initialUpdate = state => ({component: state.components[$ctrl.id], uid: (state.components[$ctrl.id] || {}).uid});
          let getModelUpdate = state => ({model: (state.components[$ctrl.id] || {}).model, uid: (state.components[$ctrl.id] || {}).uid});
          let getAttributesUpdate = state => ({attributes: (state.components[$ctrl.id] || {}).attributes, uid: (state.components[$ctrl.id] || {}).uid});
          let getValidationUpdate = state => ({validationRules: (state.components[$ctrl.id] || {}).validationRules, uid: (state.components[$ctrl.id] || {}).uid});

          // Controller initial values
          $ctrl.address = {};
          $ctrl.model = {};
          $ctrl.attributes = {};
          $ctrl.validationRules = {};
          $ctrl.actions = [];
          $ctrl.dependencies = [];
          $ctrl.contextMenu = [];
          $ctrl.status = null;
          $ctrl.initialized = false;
          $ctrl.events = [];
          let disconnectInitialUpdate;

          ////////////////////////////////////
          // COMPONENT API
          ////////////////////////////////////

          /**
           * Initialization method
           */
          $ctrl.$onInit = function() {
            // Connect redux component update
            disconnectInitialUpdate = $ngRedux.connect(initialUpdate)(onInitialUpdate);
          };

          /**
           * Initialize component
           */
          $ctrl.initialize = function() {
            if (!$ctrl.initialized && $ctrl.address != null) {
              $ctrl.initialized = true;

              // Define variables
              $ctrl.isExpanded = ($ctrl.attributes.style || "").includes("expand");
              $ctrl.expandDirection = $ctrl.attributes.expandChildren || "vertical";

              // Add status bindings
              $ctrl.events.push($ngRedux.connect(getAttributesUpdate)(onAttributesUpdate));
              $ctrl.events.push($ngRedux.connect(getValidationUpdate)(onValidationUpdate));
              $ctrl.events.push($ngRedux.connect(getModelUpdate)(onModelUpdate));

              // Initialize classes
              $ctrl.setStaticValues && $ctrl.setStaticValues();

              // Update classes
              $ctrl.updateClasses && $ctrl.updateClasses();

              // Specific initialization
              $ctrl.onInitialize && $ctrl.onInitialize();

              // Manage events
              $ctrl.events.push($scope.$on("resize", onResize));
              $ctrl.events.push($scope.$on("restore", onRestore));
              $ctrl.events.push($scope.$on("reset", onReset));
              $ctrl.events.push($scope.$on("$destroy", onDisconnect));

              // Check autoload
              $ctrl.attributes.autoload && $ctrl.reload(false, true);

              // Change component status
              if ($ctrl.status === STATUS_DEFINED) {
                // Set status as compiled
                $control.changeComponentDebounced($ctrl.address, {
                  status:  $ctrl.attributes.dependencies.length > 0 ? STATUS_COMPILED : STATUS_INITIALIZED
                });
              }
            }
            return $ctrl.initialized;
          };

          /**
           *  Reload grid
           *  @param {boolean} silent Reload without loading bars
           *  @param {boolean} async Reload on async stack
           */
          $ctrl.reload = function(silent, async) {
            // Generate filter action
            let filterAction = $actionController.generateAction({type: "filter"}, {address: $ctrl.address}, silent, async);

            // Add filter action
            $actionController.addActionList([filterAction], false, {address: $ctrl.address});

            // Return filter action
            return filterAction;
          };

          /**
           * Check if autorefresh is active and reload if true
           */
          $ctrl.autorefresh = function() {
            if ($ctrl.attributes.autorefreshEnabled && $ctrl.attributes.autorefresh) {
              $utilities.timeout.cancel($ctrl.autorefreshTimeout);
              $ctrl.autorefreshTimeout = $utilities.timeout(() => {
                $ctrl.reload(true, true);
                $ctrl.autorefresh();
              }, parseInt($ctrl.attributes.autorefresh, 10) * 1000);
            }
          };

          /**
           * Update model
           */
          $ctrl.updateModel = function() {
            // Component value
            $ctrl.value = $ctrl.getValue();
          };

          /**
           * Retrieve value
           */
          $ctrl.getValue = function() {
            return $ctrl.model.values
              .filter(value => value.selected)
              .reduce((result, option) => option, null);
          };

          /**
           * Update validation
           */
          $ctrl.updateValidation = function() {
            // Check validation errors
            $ctrl.checkValidationErrors();
          };

          /**
           * Update component classes
           */
          $ctrl.checkValidationErrors = function () {
            if ($ctrl.attributes.error && $ctrl.attributes.error.show) {
              $scope.$emit("select-current-tab");
            }
          };

          ////////////////////////////////////
          // PRIVATE METHODS
          ////////////////////////////////////

          /**
           * On update model and attributes
           * @param {Object} state New state
           */
          function onModelUpdate(state) {
            // Avoid fake updates
            if (!state.uid || _.isEqual($ctrl.model, state.model)) return;

            // Model bindings
            Object.assign($ctrl.model, state.model);

            // Update state
            $ctrl.updateModel && $ctrl.updateModel(state);
          }

          /**
           * On update attributes
           * @param {Object} state New state
           */
          function onAttributesUpdate(state) {
            // Avoid fake updates
            if (!state.uid || _.isEqual($ctrl.attributes, state.attributes)) return;

            // Model bindings
            Object.assign($ctrl.attributes, state.attributes);

            // Update classes
            $ctrl.updateClasses && $ctrl.updateClasses();

            // Update state
            $ctrl.updateAttributes && $ctrl.updateAttributes(state);

            // Check autorefresh
            $ctrl.autorefresh();
          }

          /**
           * On state update
           * @param {Object} state New state
           */
          function onValidationUpdate(state) {
            // Avoid fake updates
            if (!state.uid || _.isEqual($ctrl.validationRules, state.validationRules)) return;

            // Model bindings
            Object.assign($ctrl.validationRules, state.validationRules);

            // Update classes
            $ctrl.updateClasses && $ctrl.updateClasses();

            // Update state
            $ctrl.updateValidation && $ctrl.updateValidation(state);
          }

          /**
           * On state update
           * @param {Object} state New state
           */
          function onInitialUpdate(state) {
            // Avoid fake updates
            if (!state.uid) return;

            // Disconnect from state updates
            disconnectInitialUpdate && disconnectInitialUpdate();

            // Model bindings
            Object.assign($ctrl, state.component);

            // Update state
            $ctrl.updateModel && $ctrl.updateModel(state);

            // Update classes
            $ctrl.updateClasses && $ctrl.updateClasses();

            // Update state
            $ctrl.updateAttributes && $ctrl.updateAttributes(state);

            // Update state
            $ctrl.updateValidation && $ctrl.updateValidation(state);

            // Check autorefresh
            $ctrl.autorefresh();

            // Initialize if not done
            $ctrl.initialized = $ctrl.initialize();
          }

          /**
           * Resize event
           */
          function onResize() {
            // On resize
            $ctrl.onResize && $ctrl.onResize();
          }

          /**
           * Restore event
           */
          function onRestore() {
            // On restore
            $ctrl.onRestore && $ctrl.onRestore();
          }

          /**
           * Reset event
           */
          function onReset() {
            // On reset
            $ctrl.onReset && $ctrl.onReset();
          }

          /**
           * On disconnect event
           */
          function onDisconnect() {
            // Remove events
            $utilities.clearListeners($ctrl.events);
          }
        }
      };
    }
  ]);
