import { aweApplication } from "../awe";
import "./criterion";
import AutoNumeric from "autonumeric";
import { ROW_IDENTIFIER } from "../actions/components";
import _ from "lodash";

// Date template
export const templateNumeric = `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
     <awe-context-menu ng-cloak></awe-context-menu>
     <div ng-class="::$ctrl.groupClass" ng-cloak>
       <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>{{$ctrl.attributes.label| translateMultiple}}
         <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
       </label>
       <div class="validator input {{::$ctrl.validatorGroup}} focus-target" ng-class="{'input-group': $ctrl.attributes.unit}">
         <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
         <input ui-numeric="$ctrl.config" class="form-control text-right {{$ctrl.inputClass}}" autocomplete="off"
           ng-click="$ctrl.onClick($event)" ng-attr-id="{{::$ctrl.id}}" ng-attr-name="{{::$ctrl.id}}" ng-disabled="$ctrl.attributes.readonly"
           ng-press-enter="$ctrl.onSubmit($event)" placeholder="{{$ctrl.attributes.placeholder| translateMultiple}}"
           ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"/>
         <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
         <span ng-if="$ctrl.attributes.unit" class="input-group-addon unit" translate-multiple="{{$ctrl.attributes.unit}}" ng-cloak></span>
       </div>
     </div>
     <input ng-if="::$ctrl.attributes.showSlider" ui-slider="$ctrl.configSlider" ng-cloak/>
   </div>`;

// Date template for columns
export const templateNumericColumn =
`<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
    <span class="visible-value text-right" ng-cloak>{{$ctrl.value.formatted ? $ctrl.value.label : ""}}</span>
    <div class="edition input input-group-{{::$ctrl.attributes.size}} focus-target" ng-if="$ctrl.initializePlugin">
      <input ui-numeric="$ctrl.config" class="form-control text-right col-xs-12 {{$ctrl.inputClass}} {{$ctrl.value.style}}" autocomplete="off"
        ng-disabled="$ctrl.attributes.readonly" ng-click="$ctrl.onClick($event)" ng-press-enter="$ctrl.onSaveRow($event)" ng-focus="$ctrl.onFocus()"
        ng-blur="$ctrl.onBlur()"/>
      <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
    </div>
    <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
  </div>`;

// DateTime service
aweApplication.factory('Numeric',
  ['Control', 'Criterion', 'AweUtilities', 'AweSettings', '$filter', '$ngRedux',
    function ($control, $criterion, $utilities, $settings, $filter, $ngRedux) {

      /**
       * Process numeric options
       * @param {object} options Object
       * @param {object} defaultValues Default values
       * @return {object} Options processed
       */
      function processNumericOptions(options, defaultValues = {}) {
        return {
          "roundingMethod": $utilities.getFirstDefinedValue(options.mRound, options.roundingMethod, defaultValues.roundingMethod),
          "emptyInputBehavior": $utilities.getFirstDefinedValue(options.wEmpty, options.emptyInputBehavior, defaultValues.emptyInputBehavior),
          "currencySymbol": $utilities.getFirstDefinedValue(options.aSign, options.currencySymbol, defaultValues.currencySymbol),
          "currencySymbolPlacement": $utilities.getFirstDefinedValue(options.pSign, options.currencySymbolPlacement, defaultValues.currencySymbolPlacement),
          "digitGroupSeparator": $utilities.getFirstDefinedValue(options.aSep, options.digitGroupSeparator, defaultValues.digitGroupSeparator),
          "digitalGroupSpacing": $utilities.getFirstDefinedValue(options.dGroup, options.digitalGroupSpacing, defaultValues.digitalGroupSpacing),
          "decimalCharacter": $utilities.getFirstDefinedValue(options.aDec, options.decimalCharacter, defaultValues.decimalCharacter),
          //"allowDecimalPadding": $utilities.getFirstDefinedValue(options.aPad, options.allowDecimalPadding, ((options.mDec || options.decimalPlaces) != null), true),
          "minimumValue": $utilities.getFirstDefinedValue(options.min, options.vMin, options.minimumValue, defaultValues.minimumValue),
          "maximumValue": $utilities.getFirstDefinedValue(options.max, options.vMax, options.maximumValue, defaultValues.maximumValue),
          "decimalPlaces": $utilities.getFirstDefinedValue(options.mDec, options.precision, options.decimalPlaces, defaultValues.decimalPlaces),
          "negativePositiveSignPlacement": $utilities.getFirstDefinedValue(options.signPlacement, defaultValues.negativePositiveSignPlacement, "p"),
          "modifyValueOnWheel": false
        };
      }

      /**
       * Process slider options
       * @param {object} options Object
       * @param {object} defaultValues Default values
       * @return {object} Options processed
       */
      function processSliderOptions(options, defaultValues = {}) {
        return {
          "ticks": $utilities.getFirstDefinedValue(options.ticks, defaultValues.ticks, []),
          "ticks_labels": $utilities.getFirstDefinedValue(options.ticks_labels, defaultValues.ticks_labels, []),
          "step": $utilities.getFirstDefinedValue(options.step, defaultValues.step, 1),
          "min": $utilities.getFirstDefinedValue(options.min, options.vMin, options.minimumValue, defaultValues.min),
          "max": $utilities.getFirstDefinedValue(options.max, options.vMax, options.maximumValue, defaultValues.max),
          "precision": $utilities.getFirstDefinedValue(options.mDec, options.precision, options.decimalPlaces, defaultValues.precision),
          "readonly": options.readonly
        };
      }

      return {
        /**
         * Initialize numeric methods
         * @param $ctrl Controller
         * @param $scope Scope
         */
        init: function($ctrl, $scope) {
          // Initialize as criterion
          $criterion.init($ctrl, $scope);
          $ctrl.numericOptions = processNumericOptions($settings.get("numericOptions"));
          $ctrl.sliderOptions = processSliderOptions($settings.get("numericOptions"));

          ////////////////////////////////////
          // COMMON API
          ////////////////////////////////////

          /**
           * Update model (REDUX update model)
           */
          $ctrl.updateModel = function() {
            // Component value
            $ctrl.value = $ctrl.getValue();
            $ctrl.updateSliderModel && $ctrl.updateSliderModel();
            $ctrl.updateNumericModel && $ctrl.updateNumericModel();

            // Format if not done yet
            $ctrl.value && !("formatted" in $ctrl.value) && $ctrl.onChange();
          };

          /**
           * On change method
           */
          $ctrl.onChange = function() {
            let label = $ctrl.value ? AutoNumeric.format($ctrl.value.value, $ctrl.config) : "";
            let value = label !== "" ? Number(AutoNumeric.unformat(label, $ctrl.config)) : null;
            // Update model
            let model = {
              ...$ctrl.value,
              label: label,
              value: value,
              formatted: true,
              selected: true
            };
            $control.changeModelDeferred($ctrl.address, {values: [model]});
          };

          /**
           * Update attributes (REDUX update attributes)
           */
          $ctrl.updateAttributes = function() {
            // Check validation errors
            $ctrl.checkValidationErrors();

            // Parse numeric options
            let numericOptions = angular.isString($ctrl.attributes.numberFormat) ? $utilities.evalJSON($ctrl.attributes.numberFormat) : $ctrl.attributes.numberFormat || {};

            // Update options
            $ctrl.config = processNumericOptions(numericOptions, $ctrl.numericOptions);

            // Report numeric
            if (!_.isEqual($ctrl.config, $ctrl.previousConfig)) {
              // Report the component about the changes
              $ctrl.onNumericChange && $ctrl.onNumericChange($ctrl.config);
              $ctrl.previousConfig = $ctrl.config;
              $ctrl.value && $ctrl.onChange();
            }

            // Update slider options
            $ctrl.configSlider = processSliderOptions({...numericOptions, readonly: $ctrl.attributes.readonly}, $ctrl.sliderOptions);

            // Report slider
            if (!_.isEqual($ctrl.configSlider, $ctrl.previousConfigSlider)) {
              // Report the component about the changes
              $ctrl.onSliderChange && $ctrl.onSliderChange($ctrl.configSlider);
              $ctrl.previousConfigSlider = $ctrl.configSlider;
            }
          };

          /**
           * On initialize component, check if is a column component to delay plugin initialization
           */
          $ctrl.onInitialize = function() {
            let disconnectGridModelUpdate;
            if (!$ctrl.initializePlugin) {
              let onModelUpdate = state => {
                let selectedRows = ((state.model || {}).values || []).filter(row => row.selected);
                if (selectedRows.length === 1 &&
                  selectedRows.filter(row => row[ROW_IDENTIFIER] === $ctrl.address.row).length === 1) {
                  disconnectGridModelUpdate && disconnectGridModelUpdate();
                  $ctrl.initializePlugin = true;
                }
              };

              let getModelUpdate = state => ({model: (state.components[$ctrl.address.component] || {}).model, uid: (state.components[$ctrl.id] || {}).uid});
              disconnectGridModelUpdate = $ngRedux.connect(getModelUpdate)(onModelUpdate);
              $ctrl.events.push(disconnectGridModelUpdate);
            }
          };
        }
      };
    }
  ]);
