import { aweApplication } from "../awe";

import {
  updateComponent,
  updateMultipleComponents,
  generateCellComponents,
  updateAttributes,
  updateMultipleAttributes,
  updateModel,
  updateMultipleModels,
  updateValidation,
  updateRowModel,
  keepAttribute,
  restoreAttribute,
  keepValidation,
  restoreValidation,
  keepModel,
  restoreModel,
  keepRowModel,
  restoreRowModel,
  resetModel,
  getComponentId
} from "../actions/components";

import _ from "lodash";

/**
 * Service to manage controller and model changes
 */
aweApplication.factory('Control',
  ['$ngRedux',
    /**
     * General control methods
     */
    function ($ngRedux) {
      let $ctrl = this;
      $ctrl.pendingComponents = [];
      $ctrl.pendingModels = [];
      $ctrl.pendingAttributes = [];
      let launchDebouncedComponent = _.debounce(updateDebounced, 0);
      let launchDebouncedModel = _.debounce(updateDebounced, 0);
      let launchDebouncedAttributes = _.debounce(updateDebounced, 0);

      /**
       * Update components with redux
       * @param {object} $ctrl Controller
       * @param {string} data Data to update
       * @param {function} action Action to call
       */
      function updateDebounced($ctrl, data, action) {
        let pending = [...$ctrl[data]];
        if (pending.length > 0) {
          _.pullAll($ctrl[data], pending);
          $ngRedux.dispatch(action(pending));
        }
      }

      let $control = {

        /**
         * Change a component data
         * @param {object} address Component address
         * @param {object} data Data to set
         */
        changeComponent: function (address, data) {
          $ngRedux.dispatch(updateComponent(address, data));
        },

        /**
         * Change a component data debounced (for multiple initializations at the same time)
         * @param {object} address Component address
         * @param {object} data Data to set
         */
        changeComponentDebounced: function (address, data) {
          $ctrl.pendingComponents.push({id: getComponentId(address), data: data});
          launchDebouncedComponent($ctrl, "pendingComponents", updateMultipleComponents);
        },

        /**
         * Flush the debounced components
         */
        flushDebouncedComponents: function () {
          updateDebounced($ctrl, "pendingComponents", updateMultipleComponents);
        },

        /**
         * Change some components
         * @param {object} data Data to set
         */
        changeComponents: function (data) {
          $ngRedux.dispatch(updateMultipleComponents(data));
        },
        /**
         * Generate a component for a cell address
         * @param {object} address Component address
         * @param {object} rows Visible rows
         */
        generateCellComponents: function (address, rows) {
          $ngRedux.dispatch(generateCellComponents(address, rows));
        },
        /**
         * Change a component attribute
         * @param {object} address Component address
         * @param {object} attributes Attributes to set
         */
        changeAttribute: function (address, attributes) {
          $ngRedux.dispatch(updateAttributes(address, attributes));
        },
        /**
         * Change attributes value
         * @param {object} address Component address
         * @param {object} attributes Model attributes
         */
        changeAttributesDeferred: function (address, attributes) {
          $ctrl.pendingAttributes.push({address: address, data: attributes});
          launchDebouncedAttributes($ctrl, "pendingAttributes", updateMultipleAttributes);
        },
        /**
         * Flush the debounced attributes
         */
        flushDebouncedAttributes: function () {
          updateDebounced($ctrl, "pendingAttributes", updateMultipleAttributes);
        },
        /**
         * Change components attributes
         * @param {object} componentList Component list
         */
        changeAttributes: function (componentList) {
          $ngRedux.dispatch(updateMultipleAttributes(componentList));
        },
        /**
         * Change a controller validation
         * @param {Object} address Component address
         * @param {Object} validation Validation to set
         */
        changeValidation: function (address, validation) {
          $ngRedux.dispatch(updateValidation(address, validation));
        },
        /**
         * Retrieve component values
         * @param {string} componentId Component id
         */
        getComponentValue: function (componentId) {
          let component = $control.getComponentById(componentId);
          if (component) {
            return (component.model.values || [])
              .filter(value => value.selected)
              .map(value => value.value)
              .reduce((lastValue, value) => value, null);
          }
          return null;
        },
        /**
         * Retrieve component values
         * @param {object} address Component address
         */
        getSelectedValues: function (address) {
          let component = $control.getComponent(address);
          if (component) {
            return (component.model.values || [])
              .filter((value) => value.selected)
              .map((value) => value.value);
          }
          return [];
        },
        /**
         * Retrieve group selected values
         * @param {object} group Group
         */
        getGroupSelectedValues: function (group) {
          let componentGroup = $control.getComponentGroup(group);
          return componentGroup.reduce((result, component) => [
            ...result,
            ...(component.model.values || []).filter(value => value.selected).map(value => value.value)
          ], []);
        },
        /**
         * Retrieve a component by id
         * @param {object} address Component address
         */
        getComponentById: function (componentId) {
          return $ngRedux.getState().components[componentId] || null;
        },
        /**
         * Retrieve a component by address
         * @param {object} address Component address
         */
        getComponent: function (address) {
          return Object.values($ngRedux.getState().components).filter(component => _.isEqual(component.address, address))[0] || [];
        },
        /**
         * Retrieve a component by group
         * @param {object} group Component group
         */
        getComponentGroup: function (group) {
          return Object.values($ngRedux.getState().components).filter(component => component.attributes.group === group);
        },
        /**
         * Store a component attribute
         * @param {object} address Component address
         * @param {object} attribute Attributes to store
         */
        keepAttribute: function (address, attribute) {
          $ngRedux.dispatch(keepAttribute(address, attribute));
        },
        /**
         * Restore a component attribute
         * @param {object} address Component address
         * @param {object} attributes Attribute to restore
         */
        restoreAttribute: function (address, attribute) {
          $ngRedux.dispatch(restoreAttribute(address, attribute));
        },
        /**
         * Store a component validation
         * @param {object} address Component address
         */
        keepValidation: function (address) {
          $ngRedux.dispatch(keepValidation(address));
        },
        /**
         * Restore a component validation
         * @param {object} address Component address
         */
        restoreValidation: function (address) {
          $ngRedux.dispatch(restoreValidation(address));
        },
        /**
         * Store a component model
         * @param {object} address Component address
         */
        keepModel: function (address) {
          $ngRedux.dispatch(keepModel(address));
        },
        /**
         * Restore a component model
         * @param {object} address Component address
         */
        restoreModel: function (address) {
          $ngRedux.dispatch(restoreModel(address));
        },
        /**
         * Store a row model
         * @param {object} address Component address
         */
        keepRowModel: function (address) {
          $ngRedux.dispatch(keepRowModel(address));
        },
        /**
         * Restore a row model
         * @param {object} address Component address
         */
        restoreRowModel: function (address) {
          $ngRedux.dispatch(restoreRowModel(address));
        },
        /**
         * Reset a component model
         * @param {object} address Component address
         */
        resetModel: function (address) {
          $ngRedux.dispatch(resetModel(address));
        },
        /**
         * Change model value
         * @param {object} address Component address
         * @param {object} attributes Model attributes
         */
        changeModel: function (address, attributes) {
          $ngRedux.dispatch(updateModel(address, attributes));
        },
        /**
         * Change model value
         * @param {object} address Component address
         * @param {object} attributes Model attributes
         */
        changeModelDeferred: function (address, attributes) {
          $ctrl.pendingModels.push({address: address, data: attributes});
          launchDebouncedModel($ctrl, "pendingModels", updateMultipleModels);
        },
        /**
         * Flush the debounced model
         */
        flushDebouncedModel: function () {
          updateDebounced($ctrl, "pendingModels", updateMultipleModels);
        },
        /**
         * Change model value
         * @param {object} address Component address
         * @param {object} attributes Model attributes
         */
        changeRowModel: function (address, attributes) {
          $ngRedux.dispatch(updateRowModel(address, attributes));
        },
        /**
         * Change view model
         * @param {String} view Scope view
         * @param {object} data New model
         * @param {object} publish Publish the change
         */
        changeViewModel: function (view, data, publish) {
          $ngRedux.dispatch(changeViewModel(view, data));
        },
        /**
         * Change view model
         * @param {String} view Scope view
         * @param {object} data New attributes
         * @param {object} publish Publish the change
         */
        changeViewAttributes: function (view, data, publish) {
          $ngRedux.dispatch(changeViewAttributes(view, data));
        },
        /**
         * Get message
         * @param {string} messageId
         */
        getMessage: function (view, messageId) {
          return $ngRedux.getState().messages[view][messageId] || {};
        },
        /**
         * Get view context
         * @param {string} view
         */
        getContext: function (view) {
          return $ngRedux.getState().screen[view].context || "";
        },
        /**
         * Check model changed
         * @param {string} view
         */
        checkModelChanged: function (view) {
          return Object.values($ngRedux.getState().components)
                   .filter(component => component.address.view === view && component.model.changed)
                   .length > 0;
        },
        /**
         * Check model empty
         * @param {string} view
         */
        checkModelEmpty: function (view) {
          return Object.values($ngRedux.getState().components)
                   .filter(component => component.address.view === view)
                   .filter(component => component.model.values.filter(value => value.selected).length !== 0)
                   .length === 0;
        }
      };
      return $control;
    }
  ]);
