import { aweApplication } from "../awe";
import "../singletons/dependencyController";
import { stateGo, stateReload } from 'redux-ui-router';
import { updateScreen }  from "../actions/screen";

/**
 * Service to manage screen requests
 */
aweApplication.factory('Screen',
 ['$rootScope', '$location', 'AweSettings', 'AweUtilities', 'ActionController', 'DependencyController', 'Control', 'ServerData', '$ngRedux', '$window',
  function ($rootScope, $location, $settings, $utilities, $actionController, $dependencyController, $control, $serverData, $ngRedux, $window) {
      const $screen = {
        /**
         * Retrieve parameters and send them to the server
         * @param {object} action Action received
         */
        screen: function (action) {
          // Retrieve action parameters
          let parameters = action.parameters;
          let context = action.context;
          let reload = parameters.reload || false;

          // If token has been received, update it
          if ("token" in parameters) {
            $settings.setToken(parameters.token, true);
          }

          // Define target screen
          let target = context ? "/" + context + "/" : "";
          if ("screen" in parameters) {
            target += parameters.screen;
          } else if ("target" in parameters) {
            target += parameters.target;
          } else {
            target += action.target;
          }

          // Location is not the same
          if (!$utilities.sameUrl(target, $location.url()) || reload) {
            // Retrieve action parameters
            let state = $utilities.getState(target, reload);
            $ngRedux.dispatch(stateGo(state.to, state.parameters));

            // Finish screen action
            $actionController.acceptAction(action);
          } else if ($settings.get("reloadCurrentScreen")) {
            // Location is the same: reload
            $screen.reload(action);
          } else {
            // Finish action
            $actionController.acceptAction(action);
          }
        },
        /**
        * Reload the current state
        * @param {object} action Action received
        */
        reload: function (action) {
          // Retrieve action parameters
          $ngRedux.dispatch(stateReload());

          // Finish screen action
          $actionController.acceptAction(action);
        },
        /**
        * Return to the previous screen
        * @param {object} action Action received
        */
        back: function (action) {
          // Finish screen action
          $actionController.acceptAction(action);

          // Go to the previous state
          let router = $ngRedux.getState().router;
          $ngRedux.dispatch(stateGo(router.prevState.name, router.prevParams));
        },
        /**
        * Change the language of the interface
        * @param {object} action Action received
        */
        changeLanguage: function (action) {
          // Retrieve action parameters
          let language = action.parameters.language || $control.getComponentValue(action.target);

          // If language has been received, update it
          if (language) {
            $settings.changeLanguage(language);
          }

          // Finish screen action
          $actionController.acceptAction(action);
        },
        /**
        * Wait x milliseconds
        * @param {object} action Action received
        */
        wait: function (action) {
          // Retrieve action parameters
          let time = action.parameters.target || 1;
          $utilities.timeout(() => $actionController.acceptAction(action), time);
        },
        /**
        * Change the theme of the interface
        * @param {object} action Action received
        */
        changeTheme: function (action) {
          // Retrieve action parameters
          let theme = action.parameters.theme || $control.getComponentValue(action.target);

          // If language has been received, update it
          if (theme) {
            $settings.update({theme: theme});
          }

          // Finish screen action
          $actionController.acceptAction(action);
        },
        /**
        * Load screen data
        * @param {object} action Action received
        */
        screenData: function (action) {
          // Get parameters
          let parameters = action.parameters;

          // Wait until initial load has finished
          if ($rootScope.firstLoad) {
            // Store parameters in view scope
            $serverData.storeScreenData(parameters.screenData, parameters.view);
          } else {
            // Delay store parameters
            let waitForFirstLoad = $rootScope.$watch('firstLoad', (value) => {
              if (value) {
                waitForFirstLoad();
                // Store parameters in view scope
                $serverData.storeScreenData(parameters.screenData, parameters.view);
              }
            });
          }

          // Set main view
          $ngRedux.dispatch(updateScreen({view: $utilities.getMainView()}));

          // Send messages
          let actions = parameters.screenData.actions;
          if (actions.length > 0) {
            $actionController.addActionList(actions, false, {});
          }

          // Close action
          $actionController.acceptAction(action);
        },
        /**
        * Open screen dialog
        * @param {object} action Action received
        */
        openDialog: function (action) {
          // Change controller
          $control.changeAttribute(action.callbackTarget, {opened: true, openAction: action});
        },
        /**
        * Close screen dialog
        * @param {object} action Action received
        */
        closeDialog: function (action) {
          // Close action
          $actionController.acceptAction(action);

          // Close dialog
          $control.changeAttribute(action.callbackTarget, {opened: false, accept: true});
        },
        /**
        * Close screen dialog and cancel action
        * @param {object} action Action received
        */
        closeDialogAndCancel: function (action) {
          // Close action
          $actionController.acceptAction(action);

          // Close dialog
          $control.changeAttribute(action.callbackTarget, {opened: false, accept: false});
        },
        /**
        * Get file from server
        * @param {object} action get file from server
        */
        getFile: function (action) {
          // Variable definition
          let params = {
            ...action.parameters,
            ...$settings.getTokenObject(),
            [$settings.get("serverActionKey")]: "get-file",
          };

          // Generate url parameter
          let fileData = $serverData.getFileData("download", params);
          fileData.action = action;

          // Download file
          $utilities.downloadFile(fileData);
        },
        /**
        * Enable dependencies
        * @param {object} action Action received
        */
        enableDependencies: function (action) {
          $screen.toggleDependencies(action, true);
        },
        /**
        * Disable dependencies
        * @param {object} action Action received
        */
        disableDependencies: function (action) {
          $screen.toggleDependencies(action, false);
        },
        /**
        * Enable/Disable dependencies
        * @param {object} action Action received
        * @param {Boolean} enabled Enable/disable dependencies
        */
        toggleDependencies: function (action, enabled) {
          // Retrieve action address
          $dependencyController.toggleDependencies(enabled);

          // Close action
          $actionController.acceptAction(action);
        },
        /**
        * Add Class
        * @param {object} action Action received
        */
        addClass: function (action) {
          $screen.toggleClass(action, true);
        },
        /**
        * Remove Class
        * @param {object} action Action received
        */
        removeClass: function (action) {
          $screen.toggleClass(action, false);
        },
        /**
        * Add/Remove a class to a tag
        * @param {object} action Action received
        * @param {object} add Add/Remove a class
        */
        toggleClass: function (action, add) {
          // Variable definition
          let tagSelector = action.target;
          let parameters = action.parameters;
          let targetClass = parameters[$settings.get("targetActionKey")];
          let method = add ? "addClass" : "removeClass";

          // Add/remove the class/classes
          $(tagSelector)[method](targetClass);

          // Close action
          $actionController.acceptAction(action);
        },
        /**
        * Print the current screen
        * @param {object} action Action received
        */
        screenPrint: function (action) {
          $window.print();

          // Close action
          $actionController.acceptAction(action);
        },
        /**
        * Close the current window
        * @param {object} action Action received
        */
        closeWindow: function (action) {
          // Close action
          $actionController.acceptAction(action);

          // Call window close
          $window.close();
          console.info("window closed");
        },
        /**
        * Finish an action dependency
        * @param {object} action Action received
        */
        endDependency: function (action) {
          // Close action
          $actionController.acceptAction(action);
        }
      };
    return $screen;
  }
]);