import { aweApplication } from "../awe";
import "./criterion";
import { ROW_IDENTIFIER } from "../actions/components";

// Date template
export const templateDate = `<div ng-show="$ctrl.attributes.visible" class="criterion {{$ctrl.criterionClass}}" ng-attr-criterion-id="{{::$ctrl.id}}" ng-cloak>
  <awe-context-menu ng-cloak></awe-context-menu>
  <div ng-class="$ctrl.groupClass" ng-cloak>
    <label ng-attr-for="{{::$ctrl.id}}" ng-class="::$ctrl.labelClass" ng-style="::$ctrl.labelStyle" ng-cloak>
      {{$ctrl.attributes.label| translateMultiple}}
      <i ng-if="::$ctrl.attributes.help" class="help-target fa fa-fw fa-question-circle"></i>
    </label>
    <div class="validator input-group input-append date {{::$ctrl.validatorGroup}} focus-target" ui-date="$ctrl.config" initialize="$ctrl.initializePlugin"
      ng-readonly="$ctrl.attributes.readonly">
      <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
      <input type="text"
        class="form-control {{$ctrl.inputClass}}"
        ng-disabled="$ctrl.attributes.readonly"
        ng-model="$ctrl.value.value"
        ng-change="$ctrl.onChange()"
        ng-click="$ctrl.onClick($event)"
        ng-attr-id="{{::$ctrl.id}}"
        ng-attr-name="{{::$ctrl.id}}"
        placeholder="{{$ctrl.attributes.placeholder | translateMultiple}}"
        ng-model-options="{updateOn: 'change'}"
        ng-focus="$ctrl.onFocus()"
        ng-blur="$ctrl.onBlur()"
        autocomplete="off"
        ng-press-enter="$ctrl.onSubmit($event)"/>
      <awe-loader class="loader" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
      <span class="input-group-addon add-on">
        <i class="fa fa-calendar"></i>
      </span>
    </div>
  </div>
</div>`;

// Date template for columns
export const templateDateColumn =
`<div ng-show="$ctrl.attributes.visible" class="validator column-input criterion text-{{::$ctrl.attributes.align}} no-animate" ng-cloak>
  <span class="visible-value" ng-cloak>{{$ctrl.value.value}}</span>
  <span class="edition" ng-if="$ctrl.initializePlugin">
    <div class="input-group input-append date input-group-{{::$ctrl.attributes.size}} focus-target" ui-date="$ctrl.config">
      <input type="text" class="form-control col-xs-12 {{$ctrl.inputClass}} {{$ctrl.value.style}}" ng-disabled="$ctrl.attributes.readonly" 
        ng-model="$ctrl.value.value" ng-model-options="{updateOn: 'change'}" ng-click="$ctrl.onClick($event)" ng-change="$ctrl.onChange()" 
        ng-press-enter="$ctrl.onSaveRow($event)" autocomplete="off" ng-focus="$ctrl.onFocus()" ng-blur="$ctrl.onBlur()"/>
      <span class="input-group-addon add-on">
        <i class="fa fa-calendar"></i>
      </span>
    </div>
    <span ng-if="::$ctrl.attributes.icon" ng-class="::$ctrl.iconClass" ng-cloak></span>
  </span>
  <awe-loader class="loader no-animate" ng-if="$ctrl.attributes.loading" icon-loader="{{::$ctrl.attributes.iconLoaderSmall}}" ng-cloak></awe-loader>
</div>`;

// DateTime service
aweApplication.factory('DateTime',
  ['Control', 'Criterion', 'AweUtilities', 'AweSettings', '$filter', '$ngRedux',
    function ($control, $criterion, $utilities, $settings, $filter, $ngRedux) {

      /**
       * Select values
       * @param {Array} values New model
       * @param {Array} selected Current values
       * @return {Array} Selected values marked
       */
      function markSelectedValues(values = [], selected = []) {
        return values.map(option => ({...option, selected: $utilities.asArray(selected).map(item => item.value).includes(option.value)}));
      }

      const $dateTime = {
        /**
         * Initialize common methods
         * @param $ctrl Controller
         * @param $scope Scope
         */
        initCommons: function($ctrl, $scope) {
          // Initialize as criterion
          $criterion.init($ctrl, $scope);
          $ctrl.options = {};

          ////////////////////////////////////
          // COMMON API
          ////////////////////////////////////

          /**
           * Update model (REDUX update model)
           */
          $ctrl.updateModel = function() {
            // Component value
            $ctrl.value = $ctrl.getValue();

            // Report the component about the changes
            $scope.$broadcast('trigger-change');
          };

          /**
           * Update attributes (REDUX update attributes)
           */
          $ctrl.updateAttributes = function() {
            // Check validation errors
            $ctrl.checkValidationErrors();

            // Update options
            $ctrl.config = {
              ...$ctrl.attributes,
              ...$ctrl.options
            };
          };

          /**
           * On initialize component, check if is a column component to delay plugin initialization
           */
          $ctrl.onInitialize = function() {
            let disconnectGridModelUpdate;
            if (!$ctrl.initializePlugin) {
              let onModelUpdate = state => {
                let selectedRows = ((state.model || {}).values || []).filter(row => row.selected);
                if (selectedRows.length === 1 &&
                  selectedRows.filter(row => row[ROW_IDENTIFIER] === $ctrl.address.row).length === 1) {
                  disconnectGridModelUpdate && disconnectGridModelUpdate();
                  $ctrl.initializePlugin = true;
                }
              };

              let getModelUpdate = state => ({model: (state.components[$ctrl.address.component] || {}).model, uid: (state.components[$ctrl.id] || {}).uid});
              disconnectGridModelUpdate = $ngRedux.connect(getModelUpdate)(onModelUpdate);
              $ctrl.events.push(disconnectGridModelUpdate);
            }
          };
        },

        /**
         * Initialize as simple selector
         * @param {object} $ctrl Controller
         * @param {object} $scope Scope
         */
        initDate: function($ctrl, $scope) {
          // Initialize common methods
          $dateTime.initCommons($ctrl, $scope);

          // Set options as monoselection
          $ctrl.options = {
            container: "body",
            format: "dd/mm/yyyy",
            todayHighlight: true,
            todayBtn: 'linked',
            autoclose: true,
            enableOnReadonly: false,
            maxViewMode: 2,
            weekStart: 1,
            language: $settings.get('language')
          };
        },
        /**
         * Initialize as filtered calendar
         * @param {object} $ctrl Controller
         * @param {object} $scope Scope
         */
        initFilteredCalendar: function($ctrl, $scope) {
          // Initialize common methods
          $dateTime.initCommons($ctrl, $scope);

          // Select options
          $ctrl.options = {
            container: "body",
            format: "dd/mm/yyyy",
            todayHighlight: true,
            todayBtn: 'linked',
            autoclose: true,
            enableOnReadonly: false,
            maxViewMode: 2,
            weekStart: 1,
            language: $settings.get('language'),
            beforeShowDay: (date) => $ctrl.model.values.map(o => o.value).includes($filter('date')(date, 'dd/MM/yyyy')),
            beforeShowMonth: (date) => $ctrl.model.values.map(o => String(o.value).substr(3, 10)).includes($filter('date')(date, 'MM/yyyy')),
            beforeShowYear: (date) => $ctrl.model.values.map(o => String(o.value).substr(6, 10)).includes($filter('date')(date, 'yyyy'))
          };

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
           * On change method (From COMPONENT to REDUX)
           */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, { values: markSelectedValues($ctrl.model.values, $ctrl.value) });
          };
        },
        /**
         * Initialize as suggest
         * @param {object} $ctrl Controller
         * @param {object} $scope Scope
         */
        initTime: function($ctrl, $scope) {
          // Initialize common methods
          $dateTime.initCommons($ctrl, $scope);

          // Suggest options
          $ctrl.options = {
            minuteStep: 1,
            showSeconds: true,
            secondStep: 1,
            showInputs: false,
            showMeridian: false,
            defaultTime: false
          };
        }
      };
      return $dateTime;
    }
  ]);
