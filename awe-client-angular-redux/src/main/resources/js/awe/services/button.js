import { aweApplication } from "../awe";
import "./component";

// Button services
aweApplication.factory('Button',
  ['AweSettings', 'ActionController', 'AweUtilities', 'Component', 'Control', '$ngRedux',
    /**
     * Criterion
     */
    function ($settings, $actionController, $utilities, $component, $control, $ngRedux) {
      return {
        init: function($ctrl, $scope) {
          // Initialize as component
          $component.init($ctrl, $scope);

          /**
           * Disable global buttons
           * @param {Object} state Disabled state
           */
          function onChangeDisabled(state) {
            $ctrl.disabled = state.disabled;
          }

          // Define stateCheck function
          let stateCheck = (state) => ({disabled: state.actions.running});

          // Connect redux store updates
          $ctrl.events.push($ngRedux.connect(stateCheck)(onChangeDisabled));

          // Controller initial values
          $ctrl.actions = [];

          ////////////////////////////////////
          // BUTTON API
          ////////////////////////////////////

          /**
           * Check if button is disabled
           */
          $ctrl.isDisabled = function() {
            return $ctrl.disabled || $ctrl.attributes.disabled;
          };

          /**
           * On mouse down
           */
          $ctrl.onMouseDown = function($event) {
          };

          /**
           * On click
           */
          $ctrl.onClick = function($event) {
            // Cancel event bubble
            $event.preventDefault && $event.preventDefault();

            // Add click event
            $control.changeModel($ctrl.address, {event: "click"});

            // Add action list (if not null)
            if ($ctrl.actions.length > 0) {
              $actionController.addActionList($ctrl.actions, false, {address: $ctrl.address, context: $ctrl.context});
            } else if ($ctrl.buttonType === "reset") {
              $actionController.addActionList([{type: "restore"}], false, {address: $ctrl.address, context: $ctrl.context});
            }
          };

          /**
           *  Initialize static values
           */
          $ctrl.setStaticValues = function() {
            // Component class
            let buttonStyle = $ctrl.attributes.style || "";
            $ctrl.componentClass = [
              buttonStyle.includes("no-class") ? "" : "btn-group",
              $ctrl.attributes.hidden ? "hidden" : "",
              $ctrl.attributes.invisible ? "invisible" : ""
              ].join(" ").trim();

            // Button type
            $ctrl.buttonType = $ctrl.attributes.buttonType || 'button';

            // Button class
            $ctrl.buttonStyle = $ctrl.buttonType === "submit" ? "primary" : null;
          };

          /**
           * Update component classes
           */
          $ctrl.updateClasses = function() {
            let buttonStyle = $ctrl.attributes.style || "";
            let buttonClass = $ctrl.buttonStyle ? ` btn-${$ctrl.buttonStyle}` : "";
            // Button class
            $ctrl.buttonClass = [
              buttonStyle,
              !buttonStyle.includes("no-class") ? `btn btn-awe btn-${$ctrl.attributes.size}${buttonClass}` : "",
              $ctrl.attributes.help ? "help-target" : ""
              ].join(" ").trim();
          };
        }
      };
    }
  ]);