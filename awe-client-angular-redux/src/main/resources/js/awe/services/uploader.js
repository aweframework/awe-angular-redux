import { aweApplication } from "../awe";
import { ClientActions } from "../actions/clientActions";
import "./criterion";
import "ng-file-upload";

// Require file upload
aweApplication.requires.push("ngFileUpload");

// DateTime service
aweApplication.factory('Uploader',
  ['Control', 'Criterion', 'AweUtilities', 'ServerData', 'AweSettings', 'Upload', 'ActionController', '$translate',
    function ($control, $criterion, $utilities, $serverData, $settings, $upload, $actionController, $translate) {
      const $uploader = {
        /**
         * Initialize as uploader
         * @param {Object} $ctrl Controller
         * @param {Object} $scope Scope
         */
        init: function($ctrl, $scope) {
          $ctrl.uploaderUID = 0;

          // Initialize as criterion
          $criterion.init($ctrl, $scope);

          ////////////////////////////////////
          // API
          ////////////////////////////////////

          /**
           * Update value (From REDUX to COMPONENT)
           */
          $ctrl.getValue = function() {
            let value = $ctrl.model.values
              .filter(option => option.selected)
              .reduce((selected, option) => option, null);

            // Reset uploader if value is null
            if (value === null) {
              $ctrl.onReset && $ctrl.onReset();
            } else if (value && !("size" in value)) {
              // Get file info if not defined
              let fileInfo = {
                type: "file-info",
                filename: value.value
              };

              let serverAction = $serverData.getServerAction($ctrl.address, fileInfo, true, true);
              $actionController.addActionList([serverAction], false, {address: $ctrl.address, context: $ctrl.context});
              return null;
            }

            return value;
          };

          /**
           * Validate the file
           */
          $ctrl.onValidate = function(file) {
            let uploadMaxSize = $settings.get("uploadMaxSize");
            if (file.size > uploadMaxSize) {
              // Send error message
              $actionController.sendMessage($scope, 'error', 'ERROR_TITLE_FILE_UPLOAD', $translate.instant('ERROR_MESSAGE_SIZE_LIMIT', { elementSize: $utilities.getSizeString(file.size), maxSize: $utilities.getSizeString(uploadMaxSize)}));

              // Return not valid
              return false;
            }

            // Return ok
            return true;
          };

          /**
           * Click to choose a file
           * @param {type} files
           */
          $ctrl.onChooseFile = function (files) {
            if (files && files.length > 0) {
              // Call upload
              $ctrl.uploaderUID++;
              $ctrl.uploaderIdentifier = ["uploader", $ctrl.id, $ctrl.uploaderUID].join("-");

              // Generate upload parameters
              let uploadParameters = {
                url: $serverData.getFileUrl("upload"),
                fields: $serverData.getEncodedParameters({
                  ...$settings.getTokenObject(),
                  [$settings.get("uploadIdentifier")]: $ctrl.uploaderIdentifier,
                  [$settings.get("addressIdentifier")]: $ctrl.address,
                  "destination": $ctrl.attributes.destination || ""
                }),
                file: files[0]
              };

              // Upload the file
              let uploader = $upload.upload(uploadParameters);

              // On start upload, manage success, error and progress
              uploader.then(
                // On success
                () => null,
                // On error
                error => $ctrl.onReset(),
                // On progress
                event => $ctrl.onFileStatus(parseInt(100.0 * event.loaded / event.total))
              );

              // Change uploader status
              $ctrl.uploading = true;
              $ctrl.uploadProgress = 0;
            }
          };

          /**
           * Clear a uploaded file
           * @param {Event} event
           */
          $ctrl.onClearFile = function (event) {
            $utilities.stopPropagation(event, true);

            // Launch clear file action
            $ctrl.deleting = true;
            let deleteValues = {
              type: "delete-file",
              filename: $ctrl.value.value,
              destination: $ctrl.attributes.destination || ""
            };

            let serverAction = $serverData.getServerAction($ctrl.address, deleteValues, true, true);
            $actionController.addActionList([serverAction], false, {address: $ctrl.address, context: $ctrl.context});
          };

          /**
           * Download the file
           * @param {Event} event
           */
          $ctrl.onDownloadFile = function (event) {
            $utilities.stopPropagation(event, true);

            // Download file
            $utilities.downloadFile($serverData.getFileData("download", {
              ...$settings.getTokenObject(),
              filename: $ctrl.value.value,
              destination: $ctrl.attributes.destination || ""
            }));
          };

          /**
           * On start upload event
           * @param {object} parameters parameters sent
           */
          $ctrl.onFileStatus = function (percent) {
            if (!$ctrl.value) {
              // Change uploading percent
              $ctrl.uploading = true;
              $ctrl.uploadProgress = Math.floor(percent) + "%";
            }
          };

          /**
           * On start upload event
           * @param {object} parameters parameters sent
           */
          $ctrl.onFileUploaded = function (parameters) {
            // Fill progress bar
            $ctrl.updatingStatus = true;
            $ctrl.uploadProgress = "100%";

            // Finish file upload
            $ctrl.uploading = false;
            $ctrl.updateTimer = null;

            // Update model
            $control.changeModelDeferred($ctrl.address, { values: [{value:parameters.path, label: parameters.name, size: parameters.size, selected: true}] });
          };

          /**
           * Reset criterion
           */
          $ctrl.onReset = function () {
            // Reset status
            $ctrl.deleting = false;
            $ctrl.uploading = false;
          };

          // Define client actions for each uploader
          $utilities.defineActionListeners($ctrl.events, ClientActions.uploader, $scope, $ctrl);
        }
      };
      return $uploader;
    }
  ]);
