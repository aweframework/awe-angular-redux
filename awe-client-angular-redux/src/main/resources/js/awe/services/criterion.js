import { aweApplication } from "../awe";
import "./component";

// Criterion services
aweApplication.factory('Criterion',
  ['Control', '$ngRedux', 'AweSettings', 'AweUtilities', 'Component',
    /**
     * Criterion
     */
    function ($control, $ngRedux, $settings, $utilities, $component) {
      return {
        init: function($ctrl, $scope) {
          // Initialize as component
          $component.init($ctrl, $scope);

          ////////////////////////////////////
          // CRITERION API
          ////////////////////////////////////

          /**
           * On focus method
           */
          $ctrl.onFocus = function() {
            // Remove error
            if ($ctrl.attributes.error) {
              $control.changeAttributesDeferred($ctrl.address, {focused: true, error: null});
            } else {
              $control.changeAttributesDeferred($ctrl.address, {focused: true});
            }
          };

          /**
           * On blur method
           */
          $ctrl.onBlur = function() {
            $control.changeAttributesDeferred($ctrl.address, {focused: false});
            $control.flushDebouncedModel();
          };

          /**
           * On click method
           */
          $ctrl.onClick = function($event) {
            $utilities.stopPropagation($event);
          };

          /**
           * On submit method
           */
          $ctrl.onSubmit = function() {
          };

          /**
           * On change method
           */
          $ctrl.onChange = function() {
            // Update model
            $control.changeModel($ctrl.address, {values: [{...$ctrl.value, selected:true}]});
          };

          /**
           * Initialize static values
           */
          $ctrl.setStaticValues = function() {
            // Icon class
            let label = "style" in $ctrl.attributes && $ctrl.attributes.style.includes('no-label') ? "" : " w-label";
            $ctrl.iconClass = `criterion-icon-${$ctrl.attributes.size} form-icon fa fa-${$ctrl.attributes.icon}${label}`;

            // Group class
            let icon = $ctrl.attributes.icon ? " w-icon" : "";
            $ctrl.groupClass = `form-group group-${$ctrl.attributes.size}${icon}`;

            let labelExtra = '';
            let validatorExtra = '';
            if ("leftLabel" in $ctrl.attributes) {
              // Label class
              let charlength = parseInt($ctrl.attributes.leftLabel, 10);
              $ctrl.labelStyle = {width: (charlength * $ctrl.attributes.charSize) + "px"};
              labelExtra = ' label-left';
              validatorExtra = ' expand';
            }

            // Label class
            $ctrl.labelClass = `control-label label-${$ctrl.attributes.size}${labelExtra}`;

            // Validator group
            $ctrl.validatorGroup = `input-group-${$ctrl.attributes.size}${validatorExtra}`;
          };

          /**
           * Update component classes
           */
          $ctrl.updateClasses = function() {

            // Input class
            $ctrl.inputClass = [
              "no-animate",
              $ctrl.validationRules.required ? "required": "",
              $ctrl.attributes.size ? "input-" + $ctrl.attributes.size : "",
              $ctrl.attributes.specialClass ? $ctrl.attributes.specialClass : "",
              $ctrl.attributes.columnClass ? $ctrl.attributes.columnClass : ""
              ].join(" ").trim();

            // Criterion class
            $ctrl.criterionClass = [
              $ctrl.attributes.style,
              $ctrl.attributes.hidden ? "hidden" : "",
              $ctrl.attributes.invisible ? "invisible" : "",
              $ctrl.attributes.error ? "has-error dark" : ""
              ].join(" ").trim();
          };
        }
      };
    }
  ]);
