import $ from "jquery";

// Angular
import angular from "angular";
import ngCookies from "angular-cookies";
import ngSanitize from "angular-sanitize";
import ngAnimate from "angular-animate";
import ngTranslate from "angular-translate";
import ngLoadingBar from "angular-loading-bar";
import uiBootstrap from "angular-ui-bootstrap";
import uiRouter from "@uirouter/angularjs";
import {devToolsEnhancer} from "redux-devtools-extension"

// Redux
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import "redux";
import ngRedux from 'ng-redux';

// Bootstrap
import 'bootstrap';

import {combineReducers} from 'redux';

// Initial state
import {InitialState} from './actions/initial';

// Route methods
import {states} from './actions/route';

// Reducers
import ngReduxUiRouter, {router} from 'redux-ui-router';
import {actions} from './reducers/actionsReducer';
import {settings} from './reducers/settingsReducer';
import {screen} from './reducers/screenReducer';
import {messages} from './reducers/messagesReducer';
import {components} from './reducers/componentsReducer';
import {menu} from './reducers/menuReducer';

// Fix for jquery special events
function fixSpecialEvents(events) {
  events.forEach(name => {
    $.event.special[name] = {
      setup: function (_, ns, handle) {
        if (ns.includes("noPreventDefault")) {
          this.addEventListener(name, handle, {passive: false});
        } else {
          this.addEventListener(name, handle, {passive: true});
        }
      }
    };
  });
}

// Add special events to fix here
fixSpecialEvents(["touchstart", "touchmove", "touchend"]);

// Fix async load issue
$.ajaxPrefilter(options => options.async = true);

/**
 * Application definition
 */
export const aweApplication = angular.module("aweApplication", [
  uiRouter,
  ngRedux,
  ngReduxUiRouter,
  ngCookies,
  ngSanitize,
  ngAnimate,
  ngTranslate,
  ngLoadingBar,
  uiBootstrap])
  // Config state router
  .config(["$stateProvider", ($stateProvider) => states.forEach((state) => $stateProvider.state(state))])
  // Filter animate provider
  .config(["$animateProvider", $animate => $animate.classNameFilter(/^((?!no-animate).)*$/)])
  // For any unmatched url, redirect to /
  .config(["$urlRouterProvider", $urlRouter => $urlRouter.otherwise("/")])
  // Activate HTML5 Mode (doing this we remove the hash (#) in the url
  .config(["$locationProvider", $location => $location.html5Mode(true)])
  // Apply async
  .config(["$httpProvider", $http => $http.useApplyAsync(true)])
  // Config translate
  .config(["$translateProvider", $translate => {
    $translate.useLoader('AweLocals');
    $translate.useSanitizeValueStrategy(null);
  }])
  .config(['$ngReduxProvider', $ngRedux => {
    // Redux configuration
    const logger = createLogger({level: 'info', collapsed: true});
    const reducers = combineReducers({actions, settings, screen, messages, components, menu, router});
    let loggerMiddleware = [logger];
    console.info("Environment is: " + process.env.NODE_ENV);
    if (process.env.NODE_ENV !== "production") {
      loggerMiddleware = [logger];
    }
    $ngRedux.createStoreWith(reducers, ['ngUiRouterMiddleware', thunk,...loggerMiddleware], [devToolsEnhancer({})], InitialState);
  }])
  //.run(runDevTools)
  .run(['AweSettings', $settings => $settings.init().then(() => $settings.preloadTemplates())]);

// Export jquery
export const jQuery = $;

// Put aweApplication on self to be accessed by the external tools, as fileManager
self.aweApplication = aweApplication;
