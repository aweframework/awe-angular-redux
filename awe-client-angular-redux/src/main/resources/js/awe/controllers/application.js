import { aweApplication } from "../awe";
import { updateSettings } from "../actions/settings";

// Application controller
aweApplication.controller('AppController',
  ['$scope', '$log', '$ngRedux', 'LoadingBar', 'AweUtilities',
    /**
     * Control the base application behaviour
     * @param {object} $scope
     * @param {object} $log
     * @param {object} $ngRedux
     * @param {object} $loadingBar
     * @param {object} $utilities
     */
    function ($scope, $log, $ngRedux, $loadingBar, $utilities) {
      let $ctrl = this;

      $ctrl.isIE = function() {
        let browser = $utilities.getBrowser();
        return browser.includes("ie") ? browser : `not-ie ${browser}`;
      };

      /**
       * Manage keypress event
       * @param {Object} $event jQuery Event
       */
      $ctrl.onKeydown = function($event) {
        // On Alt + Shift + 0-9, toggle actions to 0-9 secs
        let DIGITS = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
        if ($event.altKey && $event.shiftKey && DIGITS.includes($event.which)) {
          // Toggle stack
          $ngRedux.dispatch(updateSettings({actionsStack: DIGITS.indexOf($event.which) * 1000}));
        }
      };

      // Connect redux store updates
      $ngRedux.connect(state => ({
        screen: {
          ...state.screen[state.screen.view],
          view: state.screen.view,
          visible: state.screen.visible
        },
        actions: state.actions,
        settings: state.settings
      }))($ctrl);
    }
  ]);