import { aweApplication } from "../awe";
import { ClientActions } from "../actions/clientActions";
import "../services/screen";
import _ from "lodash";

/**
 * Manages the screen load
 * @param {String} Controller name
 * @param {Function} Controller function
 */
aweApplication.controller("ScreenController",
 ['$scope', 'Screen', 'AweSettings', 'AweUtilities', 'ActionController', '$window',
  function ($scope, $screen, $settings, $utilities, $actionController, window) {
    let $ctrl = this;

    /**
     * Whether to show actions or not
     * @returns {Boolean}
     */
    $ctrl.showActions = function () {
      return $settings.get("actionsStack") > 0;
    };

    /**
     * Show action info
     * @returns {Boolean}
     */
    $ctrl.showInfo = function (action) {
      console.info(action);
    };

    /**
     * Whether to show actions or not
     * @returns {Boolean}
     */
    $ctrl.isRunning = function (action) {
      return $actionController.isRunning(action);
    };

    // Define listeners
    _.each(ClientActions.screen, function (actionOptions, actionId) {
      $scope.$on("/action/" + actionId, function (event, action) {
        return $screen[actionOptions.method](action);
      });
    });

    // Send resize event on window resize
    $(window).on("resize", () => $scope.$broadcast("resize"));
  }]
);