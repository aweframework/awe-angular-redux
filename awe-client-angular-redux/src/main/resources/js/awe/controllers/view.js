import { aweApplication } from "../awe";

// Services
import "../services/serverData";
import "../services/loadingBar";

// Update screen action
import { updateScreenView } from "../actions/screen";

// Clear view components action
import { clearComponents } from "../actions/components";

/**
 * Manages the screen load
 * @param {String} Controller name
 * @param {Function} Controller function
 */
export const viewController = aweApplication.controller("ViewController",
 ['$scope', '$element', '$ngRedux', 'LoadingBar',
   function($scope, $element, $ngRedux, $loadingBar) {
     // Controller
     let $ctrl = this;
     let stateCheck = (state) => ({screen: state.screen[$ctrl.view]});

     // Set started as false
     $ctrl.started = false;

     // Initialize loading bar
     $loadingBar.startTask();

     // Hack to hide screen in IE before dirty change
     $scope.$on("$destroy", function ($event) {
       // Disconnect on unload
       $ctrl.disconnect && $ctrl.disconnect();

       // Clear view components (to force reload)
       $ngRedux.dispatch(clearComponents($ctrl.view));

       // Clear view
       $ngRedux.dispatch(updateScreenView($ctrl.view, {loaded: false}));
     });

     /**
      * Component initialization
      */
     $ctrl.$onInit = function() {
       // Connect redux store updates
       $ctrl.disconnect = $ngRedux.connect(stateCheck)(onUpdate);

       // Add expand expandible classes
       $element.addClass("expand expandible-vertical");
     };

     /**
      * On data update
      */
     function onUpdate(data) {
       if (data.screen.loaded) {
         $ctrl.screen = data.screen;

         // Start loading screen
         if (!$ctrl.started) {
           // End loading bar
           $loadingBar.endTask();
           $ctrl.started = true;
         }
       }
     }
 }]
);