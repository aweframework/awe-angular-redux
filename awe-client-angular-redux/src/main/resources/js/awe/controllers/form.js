import { aweApplication } from "../awe";
import { ClientActions } from "../actions/clientActions";

import "../services/validator";

import _ from "lodash";

// Form directive
aweApplication.controller('FormController',
  ['$scope', 'ServerData', 'ActionController', 'AweSettings', 'AweUtilities', 'Validator', 'Control',
    /**
     * Form controller. Manages application form data
     * @param {object} $scope Scope
     * @param {object} $serverData Server data service
     * @param {object} $actionController Action controller
     * @param {object} $settings Settings service
     * @param {object} $utilities Utilities service
     * @param {object} $validator Validation service
     * @param {object} $control Control service
     */
    function ($scope, $serverData, $actionController, $settings, $utilities, $validator, $control) {
      let $ctrl = this;

      /**
       * Retrieve DOM target
       * @param {type} target
       * @param {type} scope
       * @returns {Array}
       */
      function getDOMTarget(view, target) {
        let finalTarget = $(`[ui-view='${view}']`);
        if (target) {
          // Target as id
          let targetNode = $(`#${target} [component-id]`);
          let componentNode = $(`[component-id="${target}"]`)
          if (targetNode.length && targetNode.scope) {
            finalTarget = targetNode;
          } else if (componentNode.scope) {
            finalTarget = componentNode;
          }
        }
        return finalTarget;
      }

      $ctrl.FormActions = {
        /**
         * Validate the form
         * @param {Action} action Action received
         */
        validate: function (action) {
          // Reset validator errors
          $ctrl.showValidation = false;

          // Check if action is for a specific target
          let target = getDOMTarget(action.view, action.target);

          // Get components in scope
          let components = [
            ...$utilities.asArray(target.attr("component-id")),
            ...[...target.find("[component-id]")].map(node => $(node).attr("component-id"))
          ];

          // Launch validation
          let errorList = $validator.validateComponents(components);

          // Check if validation has been successful
          if (errorList.length === 0) {
            // If is ok, accept the action
            $actionController.acceptAction(action);
            // If validation is not ok, reject the action
          } else {
            $validator.showValidationError($ctrl, errorList[0]);
            $actionController.rejectAction(action);
          }
        },
        /**
         * Set a criterion as valid
         * @param {Action} action Action received
         */
        setValid: function (action) {
          $control.changeAttribute(action.callbackTarget, {error: null} );

          // Accept the current action
          $actionController.acceptAction(action);
        },
        /**
         * Set a criterion as invalid
         * @param {Action} action Action received
         */
        setInvalid: function (action) {
          $control.changeAttribute(action.callbackTarget, {error: action.parameters.message} );

          // Accept the current action
          $actionController.acceptAction(action);
        },
        /**
         * Retrieve parameters and send them to the server
         * @param {Action} action Action received
         */
        server: function (action) {
          // Launch server action with form values
          $serverData.launchServerAction(action, $serverData.getFormValues(), false);
        },
        /**
         * Retrieve parameters and send them to the server for printing actions (send images and text)
         * @param {Action} action Action received
         */
        serverPrint: function (action) {
          // Launch server action for printing
          $serverData.launchServerAction(action, $serverData.getFormValuesForPrinting(), true);
        },
        /**
         * Retrieve parameters and send them to the server for printing actions (send images and text)
         * @param {Action} action Action received
         * @param {Object} scope Scope
         */
        serverDownload: function (action, scope) {
          // Get component
          let component = $control.getComponent(action.callbackTarget);
          let specificParameters = component.attributes.specificParameters || {};

          // Store parameters
          let parameters = {
            ...action.parameters,
            ...$serverData.getFormValues(),
            ...specificParameters
          };

          // Get target action
          let targetAction = parameters[$settings.get("targetActionKey")];

          // Generate url parameter
          let fileData = $serverData.getFileData(`download/maintain/${targetAction}`, parameters);
          fileData.action = action;

          // Download file
          $utilities.downloadFile(fileData);
        },
        /**
         * Update model with action values
         * @param {Action} action Action received
         */
        fill: function (action) {
          // Retrieve parameters
          let data = action.parameters.datalist;

          // Generate model
          let model = {...data, values: [...data.rows]};
          delete model.rows;

          // Publish model change
          $control.changeModel(action.callbackTarget, model);

          // Finish action
          $actionController.acceptAction(action);
        },
        /**
         * Update controller with action values
         * @param {object} action Action received
         */
        updateController: function (action) {
          const values = [...((action.parameters.datalist || {}).rows || [{}])];

          // Change controller
          $control.changeAttribute(action.callbackTarget, {[action.parameters.attribute]: action.parameters.value || values[0].value});

          // Finish action
          $actionController.acceptAction(action);
        },
        /**
         * Update model with action values
         * @param {object} action Action received
         */
        select: function (action) {
          // Retrieve parameters
          let values = [...action.parameters.values];

          // Call the method update seleted value from API
          $control.changeModel(action.callbackTarget, {selected: values});

          // Finish action
          $actionController.acceptAction(action);
        },
        /**
         * Reset view selected values
         * @param {object} action
         */
        reset: function (action) {
          // Check reset target
          let target = getDOMTarget(action.view, action.target);

          // Get components in scope
          let components = [
            ...$utilities.asArray(target.attr("component-id")),
            ...[...target.find("[component-id]")].map(node => $(node).attr("component-id"))
          ];

          // Reset found components
          components.forEach(component => $control.resetModel({view: action.view, component: component}));

          // Finish action
          $actionController.acceptAction(action);
        },
        /**
         * Restore view selected values
         * @param {object} action
         */
        restore: function (action) {
          // Check restore target
          let target = getDOMTarget(action.view, action.target);

          // Get components in scope
          let components = [
            ...$utilities.asArray(target.attr("component-id")),
            ...[...target.find("[component-id]")].map(node => $(node).attr("component-id"))
          ];

          // Restore found components
          components.forEach(component => $control.restoreModel({view: action.view, component: component}));

          // Finish action
          $actionController.acceptAction(action);
        },
        /**
         * Restore view selected values with target
         * @param {object} action
         */
        restoreTarget: function (action) {
          return $ctrl.FormActions.restore(action);
        },
        /**
         * Destroy all views
         */
        logout: function () {
          // Close following actions
          $actionController.deleteStack();

          // Create confirm action
          let logoutAction = {
            type: 'server',
            parameters: {
              [$settings.get("serverActionKey")]: "logout"
            }
          };

          // Send action confirm
          $actionController.addActionList([logoutAction], false, {});
        },
        /**
         * Check if model has been modified
         * @param {object} action
         */
        checkModelUpdated: function (action) {
          // Check if model is different than initial model
          if ($control.checkModelChanged(action.view)) {
            // Create confirm action
            var confirmAction = {
              type: 'confirm',
              parameters: {
                title: 'CONFIRM_TITLE_UPDATED_DATA',
                message: 'CONFIRM_MESSAGE_UPDATED_DATA'
              }
            };

            // Send action confirm
            $actionController.addActionList([confirmAction], false, {address: action.callbackTarget, context: action.context});
          }

          // Accept action
          $actionController.acceptAction(action);
        },
        /**
         * Check if model hasn't been modified
         * @param {object} action
         */
        checkModelNoUpdated: function (action) {
          // Check if model is equal than initial model
          if (!$control.checkModelChanged(action.view)) {
            // Create confirm action
            var confirmAction = {
              type: 'confirm',
              parameters: {
                title: 'CONFIRM_TITLE_NOT_UPDATED_DATA',
                message: 'CONFIRM_MESSAGE_NOT_UPDATED_DATA'
              }
            };

            // Send action confirm
            $actionController.addActionList([confirmAction], false, {address: action.callbackTarget, context: action.context});
          }

          // Accept action
          $actionController.acceptAction(action);
        },
        /**
         * Check if model has empty data
         * @param {object} action
         */
        checkModelEmpty: function (action) {
          // Check if model is different than initial model
          if ($control.checkModelEmpty(action.view)) {
            // Create confirm action
            var confirmAction = {
              type: 'confirm',
              parameters: {
                title: 'CONFIRM_TITLE_EMPTY_DATA',
                message: 'CONFIRM_MESSAGE_EMPTY_DATA'
              }
            };

            // Send action confirm
            $actionController.addActionList([confirmAction], false, {address: action.callbackTarget, context: action.context});
          }
          // Accept action
          $actionController.acceptAction(action);
        },
        /**
         * Set a static value for an element
         * @param {object} action
         */
        value: function (action) {
          // Retrieve parameters
          action.parameters.values = $utilities.asArray(action.value);
          $ctrl.FormActions.select(action);
        },
        /**
         * Cancel all actions of the current stack
         */
        cancel: function () {
          $actionController.deleteStack();
        },
        /**
         * Filter a component data
         */
        filter: function (action) {
          // Define server and target action
          const SERVER_ACTION = $settings.get("serverActionKey");
          const TARGET_ACTION = $settings.get("targetActionKey");

          // Start loading
          let component = $control.getComponent(action.callbackTarget);

          // Add action to actions stack
          let actionData = {
            ...(component.attributes.specificParameters || {}),
            [SERVER_ACTION]: component.attributes[SERVER_ACTION] || "data",
            [TARGET_ACTION]: component.attributes[TARGET_ACTION]
          };

          // Generate server action
          let serverAction = $actionController.generateAction($serverData.getServerAction(action.callbackTarget, actionData, action.async, action.silent), {address: action.callbackTarget}, action.silent, action.async);

          // Send action list
          $actionController.addActionList([serverAction], false, {address: action.callbackTarget});

          // Store server action in component (to be cancelled)
          $control.changeAttribute(action.callbackTarget, {lastAction: serverAction});

          // Accept action
          $actionController.acceptAction(action);
        },
        /**
         * Cancel all actions of the current stack
         */
        startLoad: function (action) {
          // Start loading
          $control.changeAttributesDeferred(action.callbackTarget, {loading: true});

          // Accept action
          $actionController.acceptAction(action);
        },
        /**
         * Finish loading
         * @param {object} action Action received
         */
        endLoad: function (action) {
          // Finish loading
          $control.changeAttributesDeferred(action.callbackTarget, {loading: false});

          // Close action
          $actionController.acceptAction(action);
        }
      };

      // Define listeners
      _.each(ClientActions.form, function (actionOptions, actionId) {
        $scope.$on("/action/" + actionId, function (event, action) {
          return $ctrl.FormActions[actionOptions.method](action, $scope);
        });
      });
    }
  ]
);
