import { aweApplication } from "../awe";
import { addAction, removeAction, addActions, addActionsTop, addStack, removeStack, deleteStack, startAction, runAction,
         acceptAction, rejectAction, abortAction, closeAllActions, toggleActionsRunning, ActionStatus } from "../actions/actions";

const { STATUS_INITIAL, STATUS_RUNNING, STATUS_STARTED, STATUS_ACCEPTED, STATUS_REJECTED, STATUS_ABORTED } = ActionStatus;

// Action Controller singleton
aweApplication.service('ActionController',
  ['AweUtilities', 'AweSettings', '$ngRedux', '$log',
    function ($utilities, $settings, $ngRedux, $log) {

      // Action number
      let $ctrl = this;
      $ctrl.actionNumber = 0;
      $ctrl.actions = null;

      /**
       * Generate an action
       * @param {object} actionData Action data
       * @param {object} addressData Address data
       * @param {boolean} silent Action doesn't show wait messages
       * @param {boolean} async Action is async
       * @returns {{id: number, callbackTarget: {object}, silent: (boolean), async: (boolean), context: (string), view: string, status}}
       */
      $ctrl.generateAction = function(actionData, addressData, silent, async) {
        let view = addressData.address && addressData.address.view ? addressData.address.view : "base";
        let actionTarget = addressData.address;
        if ("address" in actionData) {
          actionTarget = actionData.address;
        } else if ("target" in actionData) {
          actionTarget = {view: view, component: actionData.target};
        }

        // Update actionData with extra parameters
        return {
          ...actionData,
          id: $ctrl.actionNumber++,
          callbackTarget: actionTarget,
          silent: actionData.silent || silent || false,
          async: actionData.async || async || false,
          context: actionData.context || addressData.context || "",
          view: view,
          status: STATUS_INITIAL
        };
      };

      /**
       * Launch an action list
       * @param {array} data Action list
       * @param {boolean} addAtBottom Add the action at the bottom of the pile
       * @param {object} addressData Launcher address and context
       * @public
       */
      $ctrl.addActionList = function (data, addAtBottom, addressData) {
        // Variable definition
        let actionList = [];

        // If data is empty, exit
        if ($utilities.isNull(data)) {
          return;
        }

        // For each action,
        data.forEach((actionData) => {
          let action = actionData;
          if (!("id" in actionData)) {
            action = $ctrl.generateAction(actionData, addressData, data.silent, data.async);
          }

          // Store action
          actionList.push(action);
        });

        if (!addAtBottom) {
          $ngRedux.dispatch(addActionsTop(actionList));
        } else {
          $ngRedux.dispatch(addActions(actionList));
        }

        // Run first action
        $ctrl.runNext();
      };

      /**
       * Launches the last stack action and removes it from the stack
       * @public
       */
      $ctrl.runNext = function () {
        // Variable definition
        let stack = $ctrl.actions.syncStack;
        if (stack.length === 0) {
          $ctrl.enableButtons();
        } else {
          // Retrieve and launch action
          let action = stack[0];
          if (action.status === STATUS_INITIAL) {
            // If action is async, move the action to the other stack and call next action
            if (action.async) {
              // Remove from sync list
              $ngRedux.dispatch(removeAction(action));

              // Add to async list
              $ngRedux.dispatch(addAction(action));

              // Run action
              $ctrl.runAction(action);

              // Call next action
              $ctrl.runNext();
            } else {
              // Run action
              $ctrl.runAction(action);
            }
          }
        }
      };

      /**
       * Relaunch last action and continue with stack
       * @public
       */
      $ctrl.relaunch = function () {
        $ctrl.runNext();
      };

      /**
       * Finishes all actions
       * @public
       */
      $ctrl.closeAllActions = function () {
        // Remove from sync list
        $ngRedux.dispatch(closeAllActions());

        // Enable all buttons
        $ctrl.enableButtons();
      };

      /**
       * Enables all buttons in screen if actions stack is empty
       * @public
       */
      $ctrl.enableButtons = function () {
        // Run the action
        if ($ctrl.actions.running) {
          $ngRedux.dispatch(toggleActionsRunning(false));
        }
      };

      /**
       * Disables all buttons in screen if actions stack is not empty
       */
      $ctrl.disableButtons = function () {
        // Run the action
        if (!$ctrl.actions.running) {
          $ngRedux.dispatch(toggleActionsRunning(true));
        }
      };

      /**
       * Launches the action
       * @param {object} action Action to launch
       * @public
       */
      $ctrl.runAction = function (action) {
        // Disable all buttons
        if (!action.silent) {
          $ctrl.disableButtons();
        }

        // Launch the action
        let actionStack = $settings.get("actionsStack");
        if (actionStack) {
          // Run the action
          $ngRedux.dispatch(runAction(action));
          $utilities.timeout(() => $ctrl.launchAction(action), actionStack);
        } else {
          $ctrl.launchAction(action);
        }
      };

      /**
       * Adds a new stack to separate the stack context
       * @param {object} action Action to launch
       */
      $ctrl.launchAction = function (action) {
        if ($ctrl.isAlive(action)) {
          $log.info(`[Action] Action started (${action.type})`, action);

          // Start action
          $ngRedux.dispatch(startAction(action));

          // Launch call
          $utilities.publish(`/action/${action.type}`, action);
        }
      };

      /**
       * Adds a new stack to separate the stack context
       * @param {object} action Action
       */
      $ctrl.acceptAction = function (action) {
        // Accept action
        $ngRedux.dispatch(acceptAction(action));
      };

      /**
       * Adds a new stack to separate the stack context
       * @param {object} action Action
       */
      $ctrl.rejectAction = function (action) {
        // Reject action
        $ngRedux.dispatch(rejectAction(action));
      };

      /**
       * Adds a new stack to separate the stack context
       * @param {object} action Action
       */
      $ctrl.abortAction = function (action) {
        // Abort action
        $ngRedux.dispatch(abortAction(action));
      };

      /**
       * Finishes an action
       * @param {Object} action Action
       */
      $ctrl.closeAction = function (action) {
        // Remove from sync list
        $ngRedux.dispatch(removeAction(action));
      };

      /**
       * Check if action is alive
       * @param {object} action Action
       * @return {boolean} Action is alive
       */
      $ctrl.isAlive = function (action) {
        let currentAction = getCurrentAction(action);
        return currentAction && [STATUS_INITIAL, STATUS_RUNNING, STATUS_STARTED].includes(currentAction.status);
      };

      /**
       * Check if action is running
       * @public
       * @param {object} action Action
       * @return {boolean} Action is running
       */
      $ctrl.isRunning = function(action) {
        return [STATUS_RUNNING, STATUS_STARTED].includes(action.status);
      };

      /**
       * Deletes the action stack removing all actions
       * @public
       */
      $ctrl.deleteStack = function () {
        // Delete stack
        $ngRedux.dispatch(deleteStack());

        // Enable buttons
        $ctrl.enableButtons();
      };

      /**
       * Adds a new stack to separate the stack context
       * @public
       */
      $ctrl.addStack = function () {
        // Change state to new stack
        $ngRedux.dispatch(addStack());

        // Run new stack next option (should be empty)
        $ctrl.runNext();
      };

      /**
       * Removes last stack
       * @public
       */
      $ctrl.removeStack = function () {
        // Change state to new stack
        $ngRedux.dispatch(removeStack());
      };

      /**
       * Send a message
       * @param {object} $scope Component scope
       * @param {string} type Message type
       * @param {string} title Message title
       * @param {string} content Message content
       */
      $ctrl.sendMessage = function ($scope, type, title, content) {
        // Send message
        let messageAction = {type: 'message', silent: false};
        // Add message to action
        messageAction.parameters = {
          type: type,
          title: title,
          message: content
        };
        // Send action send message
        $ctrl.addActionList([messageAction], false, {address: {view: $scope.view}, context: $scope.context});
      };

      //
      // Private functions
      //

      /**
       * Retrieve action at last state
       * @param {object} action Action
       * @return {object} Action
       */
      function getCurrentAction(action) {
        return [...$ctrl.actions.syncStack, ...$ctrl.actions.asyncStack]
          .filter(element => element.id === action.id)
          .reduce((prev, current) => current, action);
      }

      /**
       * On update actions
       * @param {object} actions Actions
       */
      function onUpdate(actions) {
        $ctrl.actions = actions;
        let filterFinished = action => [STATUS_ACCEPTED, STATUS_ABORTED, STATUS_REJECTED].includes(action.status);

        // Get all finished actions
        let allFinishedActions = [
          ...$ctrl.actions.syncStack.filter(filterFinished),
          ...$ctrl.actions.asyncStack.filter(filterFinished)
        ];

        // Launch event depending on status
        allFinishedActions.forEach(action => action.status === STATUS_REJECTED ? onReject(action) : onAccept(action));
      }

      /**
       * On action accept
       * @param {object} action Action
       */
      function onAccept(action) {
        let dateDiff = (new Date() - action.startDate) / 1000;
        $log.debug(`[Action] Action finished (${action.type}) in ${dateDiff} seconds`, action);

         // Accept
         $ctrl.closeAction(action);

         // Run next action
        $ctrl.runNext();
      }

      /**
       * On action reject
       * @param {object} action Action
       */
       function onReject(action) {
        let dateDiff = (new Date() - action.startDate) / 1000;
        $log.debug(`[Action] Action rejected (${action.type}) in ${dateDiff} seconds`, action);

         if (typeof action.onReject === "function") {
           action.onReject();
         }

         // Accept
        $ctrl.deleteStack();
      }

      // Connect redux store updates
      $ngRedux.connect(state => ({
          syncStack: state.actions.sync[state.actions.sync.length - 1],
          asyncStack: state.actions.async,
          running: state.actions.running
        })
      )(onUpdate);
    }
  ]);
