import { aweApplication } from "../awe";
import * as ComponentActions from "../actions/components";
import { ComponentStatus, getComponentId } from "../actions/components";
const { STATUS_COMPILED, STATUS_INITIALIZED } = ComponentStatus;
import _ from "lodash";

// $dependency Controller singleton
aweApplication.service('DependencyController',
  ['$log', 'AweUtilities', '$ngRedux', 'Control', 'ActionController', '$translate', 'AweSettings', 'ServerData',
    function ($log, $utilities, $ngRedux, $control, $actionController, $translate, $settings, $serverData) {

      // Globals
      const VALUE_DEFERRED = "[[ DEFERRED ]]";
      const VALUE_NONE = "[[ NONE ]]";

      // Dependencies are active
      let $ctrl = this;
      $ctrl.state = {};
      $ctrl.activeDependencies = true;
      $ctrl.triggers = {};
      $ctrl.pendingInitialization = [];

      // Condition tests
      $ctrl.ConditionTest = {
        "eq": (v1, v2, def) => ({"test": $utilities.compareEqualValues(v1, v2), "string": def}),
        "ne": (v1, v2, def) => ({"test": !$utilities.compareEqualValues(v1, v2), "string": def}),
        "ge": (v1, v2, def) => ({"test": v1 >= v2, "string": def}),
        "le": (v1, v2, def) => ({"test": v1 <= v2, "string": def}),
        "gt": (v1, v2, def) => ({"test": v1 > v2, "string": def}),
        "lt": (v1, v2, def) => ({"test": v1 < v2, "string": def}),
        "in": (v1, v2, def) => ({"test": angular.isString(v2) ? v2.split(",").includes(v1) : $utilities.asArray(v2).includes(v1), "string": def}),
        "is not false": (v1) => ({"test": Boolean(v1), "string": `'${v1}' is not false`}),
        "is empty": (v1) => ({"test": $utilities.isEmpty(v1), "string": `'${v1}' is empty`}),
        "is not empty": (v1) => ({"test": !$utilities.isEmpty(v1), "string": `'${v1}' is not empty`})
      };

      /**
       * Register dependencies
       * @param {array} dependencies Dependency list
       * @param {object} component Dependency component
       * @return {object} Component with all dependencies
       */
      $ctrl.register = function (dependencies, component) {
        // Retrieve grid columns without component dependencies
        let gridDependencies = _.flatten((component.attributes.columnModel || [])
          .filter(column => !("component" in column) && column.dependencies.length > 0)
          .map(column => column.dependencies
            .map(dependency => ({...dependency, column: column.id, target: ["show", "hide"].includes(dependency.target) ? dependency.target + "-column" : dependency.target})))
          );

        // Connect to dependencies trigger
        component.dependencies = [...(dependencies || []),...gridDependencies];
        component.dependencies.forEach(dependency => {
          dependency.elements.forEach(element => {
            $ctrl.addTriggers(element, dependency, component);
          });
        });
        return component;
      };

      /**
       * Initialize dependencies
       * @param {object} dependencies Dependency list
       * @param {object} component Dependency component
       */
      $ctrl.initialize = function (dependencies, component) {
        // Check initial dependencies
        (dependencies || []).forEach(dependency => {
          // Launch if initial
          if (dependency.initial) {
            $log.info(`Initial check => Checking ${getComponentId(component.address)} - ${dependency.source || "none"} => ${dependency.target || "none"}`);
            $ctrl.checkDependency(dependency, component);
          }
        });
      };

      /**
       * Unregister dependencies
       * @param {string} componentId Dependency component identifier
       */
      $ctrl.unregister = function (componentId) {
        if (componentId in $ctrl.triggers) {
          $ctrl.triggers[componentId].unregister();
          delete $ctrl.triggers[componentId];
        }
      };

      /**
       * Add triggers for the element
       * @param {object} element Element to get the triggers from
       * @param {object} dependency Element dependency
       * @param {object} component Dependency component
       */
      $ctrl.addTriggers = function (element, dependency, component) {
        $ctrl.getTriggers(element, component)
          .map(trigger => getComponentId(trigger.address))
          .forEach(trigger => {
          if (trigger in $ctrl.triggers) {
            // Add dependency to trigger (if it doesn't exist)
            let newTriggerElement = {dependency: dependency, component: component, element: element};
            if ($ctrl.triggers[trigger].checkList.findIndex(triggerElement => _.isEqual(triggerElement, newTriggerElement)) === -1) {
              $ctrl.triggers[trigger].checkList = [
                ...$ctrl.triggers[trigger].checkList,
                newTriggerElement
              ];
            }
          } else {
            // Connect trigger
            $ctrl.triggers[trigger] = {
              unregister: $ngRedux.connect(state => ({ component: (state.components[trigger] || {}).model}))((state) => $ctrl.onTrigger({...state, trigger: trigger})),
              checkList: [{dependency: dependency, component: component, element: element}]
            };
          }
        });
      };

      /**
       * Retrieve element triggers
       * @param {object} element Element to get the triggers from
       * @param {object} component Component where the dependency is
       */
      $ctrl.getTriggers = function (element, component) {
        // Calculate row
        let row = element.row || component.address.row || undefined;

        // Add first trigger
        let triggers = [];

        // Don't check changes if not defined
        if (!element.checkChanges) {
          return triggers;
        }

        // Add first trigger
        triggers.push({
          address: generateAddress(element.view1 || component.address.view, element.id, element.column1 || undefined, row),
          attribute: element.attribute1 || "value",
          event: element.event || undefined
        });

        // Add second trigger if it exists
        if ("id2" in element) {
          triggers.push({
            address: generateAddress(element.view2 || component.address.view, element.id2, element.column2 || undefined, row),
            attribute: element.attribute2 || "value"
          });
        }

        return triggers;
      };

      /* *****************************
       *  Phase 1: Check
       * **************************** */

      /**
       * Check trigger launched
       * @param {object} state Triggered state
       */
      $ctrl.onTrigger = function (state = {}) {
        // If component has disappeared, unregister it
        if (!state.component) {
          $ctrl.unregister(state.trigger);
        } else {
          // Else check trigger
          if (state.trigger in $ctrl.triggers && !_.isEqual($ctrl.triggers[state.trigger].model, state.component)) {
            $ctrl.triggers[state.trigger].model = {...state.component};

            // Launch model checklist
            $ctrl.triggers[state.trigger].checkList.forEach(check => {
              $log.info(`Triggered ${state.trigger} from ${getComponentId(check.component.address)} => Checking '${getComparisonString(check.element)}' - '${check.dependency.source || "none"} => ${check.dependency.target || "none"}'`);
              $ctrl.checkDependency(check.dependency, check.component);
            });
          }
        }
      };

      /**
       * Check trigger launched
       * @param {object} dependency Dependency
       * @param {object} component Component
       */
      $ctrl.checkDependency = function (dependency, component) {
        let result = $ctrl.evaluateDependency(dependency, component);
        result.launch = dependency.invert ? !result.launch : result.launch;
        if ($ctrl.activeDependencies) {
          $ctrl.executeDependency(dependency, component, result);
        }
      };

      /**
       * Check full dependency
       * @param dependency Dependency
       * @param component Component
       * @returns {{launch: boolean, values: {}, string: Array}}
       */
      $ctrl.evaluateDependency = function (dependency, component) {
        // Initialization
        let check = dependency.type === "and" ? (prev, current) => prev && current : (prev, current) => prev || current;
        let result = {
          launch: dependency.type === "and",
          values: {},
          string: []
        };

        // Lazy evaluation. On first failed check of and/or evaluation, return
        for (let trigger of dependency.elements) {
          let triggerResult = $ctrl.evaluateTrigger(trigger, component);
          result.launch = check(result.launch, triggerResult.test);
          result.values[trigger.alias || trigger.id] = triggerResult.value;
          result.string.push(triggerResult.string);

          // Don`t trigger if a high priority trigger is not achieved
          if (trigger.cancel && !triggerResult.test) return {...result, launch: false};

          // Lazy evaluation
          if (!result.launch && dependency.type === "and") return result;
          if (result.launch  && dependency.type !== "and") return result;
        }

        // Return full evaluation
        return result;
      };

      /**
       * Check launched trigger
       * @param {object} trigger Trigger
       * @param {object} component Component
       * @returns {{test: boolean, value: *, string: string}} Evaluation result
       */
      $ctrl.evaluateTrigger = function (trigger, component) {
        // Get triggers
        let triggers = $ctrl.getTriggers(trigger, component);

        // Get triggers attributes
        let [v1, v2] = triggers.map(item => $ctrl.getAttribute(item));
        v2 = $utilities.getFirstDefinedValue(v2, trigger.value);

        // Evaluate trigger
        let condition = trigger.condition || ("event" in trigger ? "is not false" : "is not empty");
        if (condition in $ctrl.ConditionTest) {
          let test = $ctrl.ConditionTest[condition](v1, v2, `'${v1}' ${condition} '${v2}'`);
          test.value = v1;

          // Check optional
          if (trigger.optional) {
            test.test = true;
            test.string += " (optional)";
          }
          return test;
        } else {
          return {test: false, string: `invalid condition: ${condition}`};
        }
      };

      /**
       * Check trigger launched
       * @param {object} trigger Triggered state
       * @return {*} Trigger attribute
       */
      $ctrl.getAttribute = function (trigger) {
        let componentId = trigger.address.component;
        let state = $ngRedux.getState();

        // Check if component is defined
        if (!(componentId in state.components)) {
          $log.warn("[Dependency] WARNING! " + componentId + " is not defined!");
          return null;
        }

        // Check if component exists
        let component = state.components[componentId];

        // First, check event
        if (trigger.event) {
          return trigger.event === component.model.event;
        }

        // Else, check attributes
        switch (trigger.attribute) {
          // Common attributes
          case "visible":
          case "unit":
          case "label":

          // Chart attributes
          case "xMin":
          case "xMax":
          case "yMin":
          case "yMax":
          case "x":
          case "y":
            return component.attributes[trigger.attribute];

          case "editable":
            return !component.attributes.readonly;

          case "required":
            return component.validationRules.required;

          case "totalValues":
          case "totalRows":
            return component.model.values.length;

          case "selectedValues":
          case "selectedRows":
            return component.model.values.filter(item => item.selected).length;

          case "currentRow":
            return getRowIndex(component.model.values, trigger.address.row);

          case "prevCurrentRow":
            return Math.max(getRowIndex(component.model.values, trigger.address.row) - 1, 0);

          case "nextCurrentRow":
            return Math.min(getRowIndex(component.model.values, trigger.address.row) + 1, component.model.values.length);

          case "prevRowValue":
            return getCellValue(component.model.values, Math.max(getSelectedRowIndex(component.model.values) - 1, 0), trigger.address.column);

          case "nextRowValue":
            return getCellValue(component.model.values, Math.min(getSelectedRowIndex(component.model.values) + 1, component.model.values.length), trigger.address.column);

          case "selectedRowValue":
            return getCellValue(component.model.values, getSelectedRowIndex(component.model.values), trigger.address.column);

          case "footerValue":
          case "selectedRow":
            let index = getSelectedRowIndex(component.model.values);
            return index < 0 ? null : index;

          case "prevRow":
            return Math.max(getSelectedRowIndex(component.model.values) - 1, 0);

          case "nextRow":
            return Math.min(getSelectedRowIndex(component.model.values) + 1, component.model.values.length);

          case "hasDataColumn":
            return component.model.values.filter(row => !$utilities.isEmpty(row[trigger.address.column])).length > 0 ? true : null;

          case "emptyDataColumn":
            return component.model.values.filter(row => !$utilities.isEmpty(row[trigger.address.column])).length === 0 ? true : null;

          case "fullDataColumn":
            return component.model.values.filter(row => !$utilities.isEmpty(row[trigger.address.column])).length === component.model.values.length ? true : null;

          case "currentRowValue":
            return getCellValue(component.model.values, getRowIndex(component.model.values, trigger.address.row), trigger.address.column);

          case "prevCurrentRowValue":
            return getCellValue(component.model.values, Math.max(getRowIndex(component.model.values, trigger.address.row) - 1, 0), trigger.address.column);

          case "nextCurrentRowValue":
            return getCellValue(component.model.values, Math.min(getRowIndex(component.model.values, trigger.address.row) + 1, component.model.values.length), trigger.address.column);

          case "modifiedRows":
            return component.model.values.filter(row => !$utilities.isEmpty(row["ROW_TYPE"])).length;

          case "value":
          case "text":
          default:
            return getTextAttribute(component, trigger);
        }
      };

      /**
       * Get text attribute from a component
       * @param {object} component
       * @param {object} trigger
       * @return {string|array|number} text value
       */
      function getTextAttribute(component, trigger) {
        let modelAttribute = trigger.attribute === "value" ? "value" : "label";
        let rows;

        // Filter rows
        if (trigger.address.column && trigger.address.row) {
          // Grid cell attribute (defined row)
          rows = (component.model.values || []).filter(row => String(row.id) === String(trigger.address.row));
        } else {
          // Selected rows
          rows = (component.model.values || []).filter(row => row.selected);
        }

        // Extract results
        if (trigger.address.column) {
          // Grid attributes
          return $utilities.componentValue(rows.map(row => $utilities.getCellAttribute(row[trigger.address.column], modelAttribute)));
        } else if (component.attributes.columnModel) {
          // Grid rows length
          return rows.length;
        } else {
          // Criterion attribute
          return $utilities.componentValue(rows.map(item => item[modelAttribute]));
        }
      }

      /* *****************************
       *  Phase 2: Execution
       * **************************** */

      /**
       * Execute the dependency
       */
      $ctrl.executeDependency = function(dependency, component, result) {
        // Log executing dependency
        let dependencyString = result.string.join(` ${dependency.type || "and"} `);
        dependencyString = dependency.invert ? `!(${dependencyString})` : dependencyString;
        $log.info(`Executing dependency (${result.launch}) => ${dependencyString}`);

        // Launch dependency actions
        if (dependency.actions.length > 0 && result.launch) {
          $actionController.addActionList(dependency.actions, true, {address: component.address});
        }

        // Check force by target
        let target = dependency.target || "none";
        let force = false;

        switch (target) {
          case "label":
          case "columnLabel":
          case "unit":
          case "specific":
          case "input":
            // Do not force update
            break;
          default:
            // Force update
            force = true;
            break;
        }

        // Retrieve dependency source
        let value = $ctrl.retrieveSource(dependency, component, result, force);

        // Set target dependency if value has been defined
        switch (value) {
          case VALUE_DEFERRED:
          case VALUE_NONE:
            break;
          default:
            $ctrl.applyTarget(dependency, component, value, result);
        }
      };

      /**
       * Retrieve dependency source
       * @param {Object} dependency Dependency
       * @param {Object} component Component
       * @param {Object} result Condition result
       * @param {Boolean} force Force check
       */
      $ctrl.retrieveSource = function (dependency, component, result, force) {
        let source = $utilities.getFirstDefinedValue(dependency.source, "none");
        let target = $utilities.getFirstDefinedValue(dependency.target, "none");
        let launch = result.launch || force;
        let value = VALUE_NONE;
        // Search for source
        switch (source) {
          // Update model with query output
          case "query":
            value = retrieveQuerySource(result, target, dependency, component);
            break;
            // Update value with criteria value
          case "criteria-value":
          case "criteria-text":
          case "launcher":
            if (launch) {
              value = result.values[dependency.query];
            }
            break;
            // Update value with plain text
          case "value":
            if (launch) {
              value = dependency.value;
            }
            break;
            // Update value with label text
          case "label":
            if (launch) {
              value = $translate.instant(dependency.label);
            }
            break;
            // Update value with formule
          case "formule":
            if (launch) {
              value = $utilities.formule(dependency.formule, result.values);
            }
            break;
            // Reset value
          case "reset":
            if (launch) {
              value = null;
            }
            break;
            // Update value without values
          default:
            value = launch;
            break;
        }

        return value;
      };

      /**
       * Retrieve query source
       * @param {Object} result Condition result
       * @param {Object} target Dependency target
       * @param {Object} dependency Dependency
       * @param {Object} component Component
       */
      function retrieveQuerySource(result, target, dependency, component) {
        let values = {...result.values};
        if (result.launch) {
          switch (target) {
            case "label":
              values.controllerAttribute = "label";
              values.type = "update-controller";
              break;
            case "unit":
              values.controllerAttribute = "unit";
              values.type = "update-controller";
              break;
            case "format-number":
              values.controllerAttribute = "numberFormat";
              values.type = "update-controller";
              break;
            case "validate":
              values.controllerAttribute = "validation";
              values.type = "update-controller";
              break;
            case "input":
            default:
              values.type = dependency[$settings.get("serverActionKey")];
          }

          // Add component identifier and target action
          values.componentId = component.address.component;
          values[$settings.get("targetActionKey")] = dependency[$settings.get("targetActionKey")];
          // Launch end dependency
          let endDependency = {type: 'end-dependency',
            parameters: {
              dependency: dependency
            }
          };
          // Generate server action
          let serverAction = $serverData.getServerAction(component.address, values, dependency.async, dependency.silent);

          // Launch action list
          $actionController.addActionList([serverAction, endDependency], true, {address: component.address});
          return VALUE_DEFERRED;
        } else {
          switch (target) {
            case "format-number":
              // Restore numberFormat
              $control.restoreAttribute(component.address, "numberFormat");
              break;
            case "validate":
              // Restore validation
              $control.restoreValidation(component.address);
              break;
            default:
          }
        }
        return VALUE_NONE;
      }

      /**
       * Generate a grid action
       * @param {object} dependency Dependency
       * @param {object} address Component address
       * @param {string} name Action name
       * @param {string} attribute Action attribute
       * @param {string} value Action value
       */
      function generateGridAction(dependency, address, name, attribute, value) {
        let gridAddress = {view: address.view, component: address.component};
        let action = $actionController
          .generateAction({type: name, parameters: {[attribute]: value, columns: address.column || dependency.column}}, {address: gridAddress}, true, true);
        $actionController.addActionList([action], true, {address: gridAddress});
      }

      /**
       * Apply target type
       * @param {object} dependency Dependency
       * @param {object} component Component
       * @param {string} value Dependency value
       * @param {object} result Condition result
       */
      $ctrl.applyTarget = function (dependency, component, value, result) {
        let target = $utilities.getFirstDefinedValue(dependency.target, "none");

        // Target launch when launch is true
        if (result.launch) {
          switch (target) {
            case "unit":
            case "icon":
              $control.changeAttributesDeferred(component.address, {[target]: value});
              return;

            case "label":
              if ("column" in component.address) {
                generateGridAction(dependency, component.address, "change-column-label", "label", value);
              } else {
                $control.changeAttributesDeferred(component.address, {[target]: value});
              }
              return;

            case "chart-options":
              $control.changeAttributesDeferred(component.address, {chartOptions: value});
              return;

            case "attribute":
              $control.changeAttributesDeferred(component.address, { [dependency.query]: value });
              return;

            case "input":
              $control.changeModelDeferred(component.address, {selected: value});
              return;

            case "format-number":
              $control.changeAttributesDeferred(component.address, {numberFormat: value});
              return;

            case "validate":
              $control.changeValidation(component.address, value);
              return;
          }
        }

        // Launch can be true or false
        switch (target) {
          case "format-number":
            $control.restoreAttribute(component.address, "numberFormat");
            return;

          case "validate":
            $control.restoreValidation(component.address);
            return;

          case "set-required":
            $control.changeValidation(component.address, {required: result.launch});
            return;
          case "set-optional":
            $control.changeValidation(component.address, {required: !result.launch});
            return;

          case "show":
            $control.changeAttributesDeferred(component.address, {visible: result.launch});
            return;
          case "hide":
            $control.changeAttributesDeferred(component.address, {visible: !result.launch});
            return;

          case "show-column":
          case "hide-column":
            generateGridAction(dependency, component.address, "toggle-columns-visibility", "show", target === "show-column" ? result.launch : !result.launch);
            return;

          case "set-visible":
            $control.changeAttributesDeferred(component.address, {invisible: !result.launch});
            return;
          case "set-invisible":
            $control.changeAttributesDeferred(component.address, {invisible: result.launch});
            return;

          case "enable":
            $control.changeAttributesDeferred(component.address, {disabled: !result.launch});
            return;
          case "disable":
            $control.changeAttributesDeferred(component.address, {disabled: result.launch});
            return;

          case "set-editable":
            $control.changeAttributesDeferred(component.address, {readonly: !result.launch});
            return;
          case "set-readonly":
            $control.changeAttributesDeferred(component.address, {readonly: result.launch});
            return;

          case "enable-autorefresh":
            $control.changeAttributesDeferred(component.address, {autorefreshEnabled: result.launch, autorefresh: result.launch ? value : 0});
            return;
          case "disable-autorefresh":
            $control.changeAttributesDeferred(component.address, {autorefreshEnabled: !result.launch, autorefresh: result.launch ? value : 0});
            return;

          case "none":
            return;

          default:
            $log.warn(`Dependency target "${target}" not defined`);
        }
      };

      /**
       * Enable/Disable dependencies
       * @param {boolean} enabled Enable/disable dependencies
       */
      $ctrl.toggleDependencies = function (enabled) {
        $ctrl.activeDependencies = enabled;
      };

      /**
       * Get row index
       * @param {array} values Values
       * @param {number|string} rowId Row id
       * @returns {number} Row index
       */
      function getRowIndex(values, rowId) {
        return values.findIndex(row => String(row.id) === String(rowId));
      }

      /**
       * Get selected row index
       * @param {array} values Values
       * @returns {number} Row index
       */
      function getSelectedRowIndex(values) {
        return values.findIndex(row => row.selected);
      }

      /**
       * Get cell
       * @param {array} values Values
       * @param {number} rowIndex Row index
       * @param {string} columnId Column id
       */
      function getCell(values, rowIndex, columnId) {
        return rowIndex !== -1 ? values[rowIndex][columnId] || null : null;
      }

      /**
       * Get cell value
       * @param values
       * @param rowIndex
       * @param columnId
       */
      function getCellValue(values, rowIndex, columnId) {
        let value = getCell(values, rowIndex, columnId);
        if (angular.isArray(value)) {
          return value
            .filter(data => data.selected)
            .map(data => data.value).join(", ");
        } else if (angular.isObject(value)) {
          return value.value;
        } else {
          return value;
        }
      }

      /**
       * Generate an address
       * @param {string} view View
       * @param {string} component Component
       * @param {string} column Column
       * @param {string} row Row
       */
      function generateAddress(view, component, column, row) {
        let address = {view: view, component: component};
        address = column !== undefined ? {...address, column: column} : address;
        return row !== undefined ? {...address, row: row} : address;
      }

      /**
       * Retrieve comparison string
       * @param {object} element
       * @return {string} Comparison string
       */
      function getComparisonString(element) {
        let {id, column1, row1, attribute1, condition, id2, column2, row2, attribute2, value, alias, event, optional} = element;
        let component1 = getComponentId({component: id,  column: column1, row: row1});
        let component2 = getComponentId({component: id2, column: column2, row: row2});
        let part1 = attribute1 ? `${attribute1} from ${component1}` : `${component1}`;
        let launchesEvent = event ? ` is launching [${event}] event?` : "";
        let part2 = attribute2 ? `${attribute2} from ${component2}` : component2 || value;
        let part3 = alias ? ` as ${alias}` : "";
        let isOptional = optional ? " (optional)" :  "";
        return `${part1}${condition ? " " + condition : ""}${launchesEvent}${part2 ? " " + part2 : ""}${part3}${isOptional}`;
      }

      /**
       * On update changes
       */
      function onUpdate(components) {
        $ctrl.state = components;
        // Initialize compiled components
        if (components.pending && components.pending.length) {
          // Register all components
          let registered = components.pending.map(component => $ctrl.register(component.dependencies, component));

          // Change component status
          $control.changeComponents(registered.map(component => ({id: getComponentId(component.address), data: {status: STATUS_INITIALIZED}})));

          // Initialize them
          registered.forEach(component => $ctrl.initialize(component.dependencies, component));
        }
      }

      // Connect redux store updates
      $ngRedux.connect((state) => ({
          pending: Object.values(state.components).filter(component => component.status === STATUS_COMPILED)
        }))(onUpdate);
    }
  ]);
