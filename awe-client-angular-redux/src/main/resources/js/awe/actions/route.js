// Route method definition
export const routeMethods = {
  "screenDataBase": ["$stateParams", "ServerData", "$ngRedux", (p, dataService, $ngRedux) => {
    const view = "base";
    dataService.getScreenData(routeMethods.screen(p, view), view);
    return view;
  }],
  "screenDataBaseCompound": ["$stateParams", "ServerData", (p, dataService) => {
    const view = "base";
    dataService.getScreenData(routeMethods.screen(p, view), view);
    return view;
  }],
  "templateBase": ["$stateParams", "ServerData", (p, dataService) => {
    const view = "base";
    return dataService.getTemplateUrl(routeMethods.screen(p, view), view, "r" in p);
  }],
  "screenDataReport": ["$stateParams", "ServerData", "$ngRedux", (p, dataService, $ngRedux) => {
    const view = "report";
    dataService.getScreenData(routeMethods.screen(p, view), view);
    return view;
  }],
  "templateReport": ["$stateParams", "ServerData", (p, dataService) => {
    const view = "report";
    return dataService.getTemplateUrl(routeMethods.screen(p, view), view, "r" in p);
  }],
  "screen": (p, view) => view === "report" && "subScreenId" in p ? p.subScreenId : view === "base" && "screenId" in p ? p.screenId : null
};

// Set up states
export const states = [
  { name: 'index', url: '/', views: { base: 'baseViewComponent' }, resolve: { view: routeMethods.screenDataBase } },
  { name: 'global', url: '/screen/{screenId}?:r', views: { base: 'baseViewComponent' }, resolve: { view: routeMethods.screenDataBase } },
  { name: 'public', url: '/screen/public/{screenId}', views: { base: 'baseViewComponent' }, resolve: { view: routeMethods.screenDataBaseCompound } },
  { name: 'private', url: '/screen/private/{screenId}', views: { base: 'baseViewComponent' }, resolve: { view: routeMethods.screenDataBaseCompound } },
  { name: 'public.screen', url: '/{subScreenId}?:r', views: { report: 'reportViewComponent' }, resolve: { view: routeMethods.screenDataReport } },
  { name: 'private.screen', url: '/{subScreenId}?:r', views: { report: 'reportViewComponent' }, resolve: { view: routeMethods.screenDataReport } }
];
