/*
 * action types
 */

export const UPDATE_MESSAGES = 'UPDATE_MESSAGES';
export const CONFIRM_MESSAGE = 'CONFIRM_MESSAGE';

/*
 * action creators
 */

export function updateMessages(view, data) {
  return {type: UPDATE_MESSAGES, view, data}
}

export function confirmMessage(data) {
  return {type: CONFIRM_MESSAGE, data}
}