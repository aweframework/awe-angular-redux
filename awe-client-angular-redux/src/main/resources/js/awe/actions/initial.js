// Initialize the store
export const InitialState = {
  actions: {
    running: false,
    sync: [[]],
    async: []
  },
  screen: {
    view: "base",
    base: {},
    report: {}
  },
  messages: {
    base: {},
    report: {}
  },
  components: {
  },
  menu: {
    options: [],
    status: { minimized: false, animating: false, resolution: "desktop" }
  },
  settings: {},
  router: {}
};