/*
 * action types
 */

export const CLEAR_COMPONENTS = 'CLEAR_COMPONENTS';
export const GENERATE_CELL_COMPONENTS = 'GENERATE_CELL_COMPONENTS';
export const UPDATE_COMPONENTS = 'UPDATE_COMPONENTS';
export const UPDATE_COMPONENT = 'UPDATE_COMPONENT';
export const UPDATE_ATTRIBUTES = 'UPDATE_ATTRIBUTES';
export const UPDATE_MULTIPLE_ATTRIBUTES = 'UPDATE_MULTIPLE_ATTRIBUTES';
export const UPDATE_MODEL = 'UPDATE_MODEL';
export const UPDATE_ROW_MODEL = 'UPDATE_ROW_MODEL';
export const UPDATE_VALIDATION = 'UPDATE_VALIDATION';
export const UPDATE_MULTIPLE_COMPONENTS = 'UPDATE_MULTIPLE_COMPONENTS';
export const UPDATE_MULTIPLE_MODELS = 'UPDATE_MULTIPLE_MODELS';

// Keep, restore & reset actions
export const KEEP_VALIDATION = 'KEEP_VALIDATION';
export const KEEP_ATTRIBUTE = 'KEEP_ATTRIBUTE';
export const KEEP_MODEL = 'KEEP_MODEL';
export const KEEP_ROW_MODEL = 'KEEP_ROW_MODEL';
export const RESTORE_VALIDATION = 'RESTORE_VALIDATION';
export const RESTORE_ATTRIBUTE = 'RESTORE_ATTRIBUTE';
export const RESTORE_MODEL = 'RESTORE_MODEL';
export const RESTORE_ROW_MODEL = 'RESTORE_ROW_MODEL';
export const RESET_MODEL = 'RESET_MODEL';

/*
 * action status
 */

export const ComponentStatus = {
  STATUS_DEFINED: 'defined',
  STATUS_COMPILED: 'compiled',
  STATUS_INITIALIZED: 'initialized'
};

/**
 * Address type
 */

export const ComponentAddressType = {
  ADDRESS_CELL: 'cell',
  ADDRESS_COMPONENT: 'component',
  ADDRESS_VIEW: 'view',
  ADDRESS_INVALID: 'invalid'
};

/**
 * Component type
 */

export const ComponentType = {
  COMPONENT_GRID: 'grid',
  COMPONENT_OTHER: 'other',
  COMPONENT_TEXT: 'text',
  COMPONENT_TEXTAREA: 'textarea',
  COMPONENT_NUMERIC: 'numeric',
  COMPONENT_SELECT: 'select',
  COMPONENT_SELECT_MULTIPLE: 'select-multiple',
  COMPONENT_SUGGEST: 'suggest',
  COMPONENT_SUGGEST_MULTIPLE: 'suggest-multiple',
  COMPONENT_DATE: 'date',
  COMPONENT_FILTERED_CALENDAR: 'filtered-calendar',
  COMPONENT_TIME: 'time',
  COMPONENT_HIDDEN: 'hidden',
  COMPONENT_PASSWORD: 'password',
  COMPONENT_FILE: 'file',
  COMPONENT_CHECKBOX: 'checkbox',
  COMPONENT_RADIO: 'radio',
  COMPONENT_BUTTON_CHECKBOX: 'button-checkbox',
  COMPONENT_BUTTON_RADIO: 'button-radio',
  COMPONENT_TAB: 'tab',
  COMPONENT_UPLOADER: 'uploader',
  COMPONENT_COLOR: 'color',
  COMPONENT_TEXT_VIEW: 'text-view',
  COMPONENT_WYSIWYG: 'wysiwyg',
  COMPONENT_MARKDOWN_EDITOR: 'markdown-editor',
  COMPONENT_ICON: 'icon',
  COMPONENT_IMAGE: 'image',
  COMPONENT_PROGRESS: 'progress',
  COMPONENT_SPARKLINE: 'sparkline',
  COMPONENT_DIALOG: 'dialog',
  COMPONENT_ACCORDION: 'accordion',
  COMPONENT_WIZARD: 'wizard'
};

// Grid constants
export const ROW_IDENTIFIER = 'id';
export const SMALL_GRID = 750;

/*
 * Utilities
 */

/**
 * Get component id
 */
export function getComponentId(address) {
  switch (getAddressType(address)) {
    case ComponentAddressType.ADDRESS_CELL:
      return `${address.component}-${address.row}-${address.column}`;
    case ComponentAddressType.ADDRESS_COMPONENT:
      return address.component;
    default:
      return null;
  }
}

/**
 * Check address type
 */
export function getAddressType(address) {
  if (angular.isObject(address)) {
    if ("row" in address && "column" in address
      && address.row !== undefined && address.column !== undefined) {
      return ComponentAddressType.ADDRESS_CELL;
    } else if ("component" in address && address.component !== undefined) {
      return ComponentAddressType.ADDRESS_COMPONENT;
    } else if ("view" in address && address.view !== undefined) {
      return ComponentAddressType.ADDRESS_VIEW;
    }
  }
  return ComponentAddressType.ADDRESS_INVALID;
}

/*
 * action creators
 */

export function clearComponents(view) {
  return {type: CLEAR_COMPONENTS, view};
}

export function updateComponents(view, data) {
  return {type: UPDATE_COMPONENTS, view, data};
}

export function updateComponent(address, data) {
  return {type: UPDATE_COMPONENT, address, data};
}

export function updateMultipleComponents(data) {
  return {type: UPDATE_MULTIPLE_COMPONENTS, data};
}

export function updateMultipleModels(data) {
  return {type: UPDATE_MULTIPLE_MODELS, data};
}

export function generateCellComponents(address, data) {
  return {type: GENERATE_CELL_COMPONENTS, address, data};
}

export function updateAttributes(address, data) {
  return {type: UPDATE_ATTRIBUTES, address, data};
}

export function updateMultipleAttributes(componentList) {
  return {type: UPDATE_MULTIPLE_ATTRIBUTES, componentList};
}

export function updateModel(address, data) {
  return {type: UPDATE_MODEL, address, data};
}

export function updateSelected(address, data) {
  return {type: UPDATE_SELECTED, address, data};
}

export function updateRowModel(address, data) {
  return {type: UPDATE_ROW_MODEL, address, data};
}

export function updateValidation(address, data) {
  return {type: UPDATE_VALIDATION, address, data};
}

export function keepValidation(address, data) {
  return {type: KEEP_VALIDATION, address, data};
}

export function keepAttribute(address, data) {
  return {type: KEEP_ATTRIBUTE, address, data};
}

export function keepModel(address, data) {
  return {type: KEEP_MODEL, address, data};
}

export function keepRowModel(address, data) {
  return {type: KEEP_ROW_MODEL, address, data};
}

export function restoreValidation(address, data) {
  return {type: RESTORE_VALIDATION, address, data};
}

export function restoreAttribute(address, data) {
  return {type: RESTORE_ATTRIBUTE, address, data};
}

export function restoreModel(address, data) {
  return {type: RESTORE_MODEL, address, data};
}

export function restoreRowModel(address, data) {
  return {type: RESTORE_ROW_MODEL, address, data};
}

export function resetModel(address, data) {
  return {type: RESET_MODEL, address, data};
}