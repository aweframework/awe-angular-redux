import {
  ABORT_ACTION,
  ACCEPT_ACTION,
  ActionStatus,
  ADD_ACTION,
  ADD_ACTIONS,
  ADD_ACTIONS_TOP,
  ADD_STACK,
  CLOSE_ALL_ACTIONS,
  DELETE_STACK,
  REJECT_ACTION,
  REMOVE_ACTION,
  REMOVE_STACK,
  RUN_ACTION,
  START_ACTION,
  TOGGLE_ACTIONS_RUNNING
} from '../actions/actions'

const {STATUS_INITIAL, STATUS_STARTED, STATUS_RUNNING, STATUS_ACCEPTED, STATUS_REJECTED, STATUS_ABORTED} = ActionStatus;

/**
 * Add a sync action
 * @param {object} state Current state
 * @param {object} action action to add
 * @return {object} next state
 */
function addActionSync(state, action) {
  let currentStackIndex = Math.max(state.sync.length - 1, 0);
  let currentStack = state.sync[currentStackIndex];
  return {
    ...state,
    sync: [
      ...state.sync.slice(0, currentStackIndex),
      [
        ...currentStack,
        action.payload
      ]
    ]
  }
}

/**
 * Add an async action
 * @param {object} state Current state
 * @param {object} action action to add
 * @return {object} next state
 */
function addActionAsync(state, action) {
  return {
    ...state,
    async: [
      ...state.async,
      action.payload
    ]
  }
}

/**
 * Remove an async action
 * @param {object} state Current state
 * @param {object} action action to add
 * @return {object} next state
 */
function removeAction(state, action) {
  return {
    ...state,
    sync: state.sync.map((stack) => stack.filter(a => a.id !== action.payload.id)),
    async: state.async.filter(a => a.id !== action.payload.id)
  }
}

/**
 * Set action attribute
 * @param action Action
 * @param actionId Action id
 * @param data Action data
 */
function setActionAttribute(action, actionId, data) {
  if (action.id === actionId) {
    return {
      ...action,
      ...data
    }
  }
  return action;
}

/**
 * Set action as running
 * @param state State
 * @param actionId Action id
 * @param data Action data
 */
function changeActionAttribute(state, actionId, data) {
  let currentStackIndex = Math.max(state.sync.length - 1, 0);
  let currentStack = state.sync[currentStackIndex];
  return {
    ...state,
    sync: [
      ...state.sync.slice(0, currentStackIndex),
      currentStack.map((actionInStack) => setActionAttribute(actionInStack, actionId, data))
    ],
    async: state.async.map((actionInStack) => setActionAttribute(actionInStack, actionId, data))
  };
}

/**
 * Actions reducer
 * @param state Previous state
 * @param action Action
 * @returns new state
 */
export function actions(state = {}, action = {}) {
  let currentStackIndex;
  let currentStack
  switch (action.type) {
    case ADD_ACTION:
      if (action.payload.async) {
        return addActionAsync(state, action);
      } else {
        return addActionSync(state, action);
      }
    case REMOVE_ACTION:
      return removeAction(state, action);
    case ADD_ACTIONS:
      currentStackIndex = Math.max(state.sync.length - 1, 0);
      currentStack = state.sync[currentStackIndex];
      return {
        ...state,
        sync: [
          ...state.sync.slice(0, currentStackIndex),
          [
            ...currentStack,
            ...action.payload
          ]
        ]
      }
    case ADD_ACTIONS_TOP:
      currentStackIndex = Math.max(state.sync.length - 1, 0);
      currentStack = state.sync[currentStackIndex];
      return {
        ...state,
        sync: [
          ...state.sync.slice(0, currentStackIndex),
          [
            ...action.payload,
            ...currentStack
          ]
        ]
      }
    case ADD_STACK:
      return {
        ...state,
        sync: [
          ...state.sync,
          []
        ]
      }
    case REMOVE_STACK:
      let previousStackIndex = Math.max(state.sync.length - 2, 0);
      let previousStack = state.sync[previousStackIndex];
      currentStackIndex = Math.max(state.sync.length - 1, 0);
      currentStack = state.sync[currentStackIndex];
      if (currentStackIndex === 0) return state;
      return {
        ...state,
        sync: [
          ...state.sync.slice(0, previousStackIndex),
          [
            ...currentStack,
            ...previousStack
          ]
        ]
      }
    case DELETE_STACK:
      currentStackIndex = Math.max(state.sync.length - 1, 0);
      return {
        ...state,
        sync: [
          ...state.sync.slice(0, currentStackIndex),
          []
        ]
      }
    case RUN_ACTION:
      return changeActionAttribute(state, action.payload.id, {"status": STATUS_RUNNING});
    case START_ACTION:
      return changeActionAttribute(state, action.payload.id, {"status": STATUS_STARTED, "startDate": new Date()});
    case ACCEPT_ACTION:
      return changeActionAttribute(state, action.payload.id, {"status": STATUS_ACCEPTED, "endDate": new Date()});
    case REJECT_ACTION:
      return changeActionAttribute(state, action.payload.id, {"status": STATUS_REJECTED, "endDate": new Date()});
    case ABORT_ACTION:
      return changeActionAttribute(state, action.payload.id, {"status": STATUS_ABORTED, "endDate": new Date()});
    case TOGGLE_ACTIONS_RUNNING:
      return {
        ...state,
        running: action.running,
      };
    case CLOSE_ALL_ACTIONS:
      return {
        ...state,
        running: false,
        sync: [[]],
        async: []
      }
    default:
      return state
  }
}