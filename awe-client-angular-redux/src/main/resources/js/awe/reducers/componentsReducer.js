import {
  CLEAR_COMPONENTS,
  GENERATE_CELL_COMPONENTS,
  UPDATE_COMPONENTS,
  UPDATE_COMPONENT,
  UPDATE_MULTIPLE_COMPONENTS,
  UPDATE_MULTIPLE_MODELS,
  UPDATE_ATTRIBUTES,
  UPDATE_MULTIPLE_ATTRIBUTES,
  UPDATE_MODEL,
  UPDATE_ROW_MODEL,
  UPDATE_VALIDATION,
  KEEP_VALIDATION,
  KEEP_ATTRIBUTE,
  KEEP_MODEL,
  KEEP_ROW_MODEL,
  RESTORE_VALIDATION,
  RESTORE_ATTRIBUTE,
  RESTORE_MODEL,
  RESTORE_ROW_MODEL,
  RESET_MODEL,
  ComponentAddressType,
  ComponentStatus,
  getComponentId,
  getAddressType
} from '../actions/components';

import { getUID } from "../actions/settings";

const { STATUS_DEFINED, STATUS_INITIALIZED } = ComponentStatus;
const { ADDRESS_CELL, ADDRESS_COMPONENT } = ComponentAddressType;

import _ from 'lodash';

/**
 * Returns true if a variable is null
 * @param {String} n Variable to test
 * @return {boolean} String is null or undefined
 */
function isNull(n) {
  return n === null || n === undefined;
}
/**
 * Returns true if a variable is null or empty
 * @param {String} n Variable to test
 * @return {boolean} String is null or undefined
 */
function isEmpty(n) {
  return isNull(n) || $.trim(n) === "";
}

/**
 * Manage parameter as array
 * @param {Array|Object|String|Number} value
 * @return {Array} value as array
 */
function asArray(value) {
  return isEmpty(value) ? [] : [...(angular.isArray(value) ? value : [value])];
}

/**
 * Check if component is a grid or not
 * @param {Object} component
 */
function isGrid(component) {
  return "columnModel" in (component.attributes || {});
}

/**
 * Check if component is part of a group
 * @param {Object} component
 * @return {Boolean} component belongs a group
 */
function isGroup(component) {
  let attributes = component.attributes || {};
  return "group" in attributes && (attributes.component || "").includes("radio");
}

/**
 * Fix cell model
 * @param selected Selected data
 * @param model Select model
 * @returns {object} Model fixed
 */
function fixCellModel(selected, model) {
  let selectedData = [...asArray(selected)];
  if (model && model.values && model.values.length) {
    let selectedString = selectedData.map(value => angular.isObject(value) ? String(value.value) : String(value));
    return {...model, values: model.values.map(value => ({...value, selected: selectedString.includes(String(value.value))}))};
  } else {
    return {values: selectedData.map(value => angular.isObject(value) ? ({...value, selected: true}) : ({value: value, label: value, selected: true}))};
  }
}

/**
 * Get component id
 * @param cellFunction
 * @param componentFunction
 * @param state
 * @param action
 * @returns {null|*}
 */
function launchAddressFunction(cellFunction, componentFunction, state, action) {
  switch (getAddressType(action.address)) {
    case ADDRESS_CELL:
      return cellFunction(state, action.address, action.data);
    case ADDRESS_COMPONENT:
      return componentFunction(state, action.address, action.data);
    default:
      return null;
  }
}

/**
 * Update attributes
 * @param {Object} state
 * @param {String} component
 * @param {Object} data
 * @return {Object} updated state
 */
function updateAttributesComponent(state= {}, component= "", data= {}) {
  return !(component in state) ? state : {
    ...state,
    [component] : {
      ...state[component],
      attributes: {
        ...state[component].attributes,
        ...data
      }
    }
  };
}

/**
 * Keep validation
 * @param {Object} state
 * @param {Object} component
 * @param {Object} data
 * @return {Object} updated state
 */
function keepAttributeComponent(state, component, data) {
  return {
    ...state,
    [component] : {
      ...state[component],
      storedAttributes: {
        ...state[component].storedAttributes,
        [data]: {
          ...state[component].attributes[data]
        }
      }
    }
  };
}

/**
 * Restore application
 * @param {Object} state
 * @param {Object} component
 * @param {Object} data
 * @return {Object} updated state
 */
function restoreAttributeComponent(state, component, data) {
  return {
    ...state,
    [component] : {
      ...state[component],
      attributes: {
        ...state[component].attributes,
        [data]: state[component].storedAttributes[data]
      }
    }
  };
}

/**
 * Update validation
 * @param {Object} state
 * @param {Object} component
 * @param {Object} data
 * @return {Object} updated state
 */
function updateValidationComponent(state, component, data) {
  return component == null ? state : {
    ...state,
    [component] : {
      ...state[component],
      validationRules: {
        ...state[component].validationRules,
        ...data
      }
    }
  };
}

/**
 * Keep validation
 * @param {Object} state
 * @param {Object} component
 * @return {Object} updated state
 */
function keepValidationComponent(state, component) {
  return {
    ...state,
    [component] : {
      ...state[component],
      storedValidationRules: {
        ...state[component].validationRules
      }
    }
  };
}

/**
 * Restore validation
 * @param {Object} state
 * @param {Object} component
 * @return {Object} updated state
 */
function restoreValidationComponent(state, component) {
  return {
    ...state,
    [component] : {
      ...state[component],
      validationRules: {
        ...state[component].storedValidationRules
      }
    }
  };
}

/**
 * Update attributes
 * @param {Object} state
 * @param {Object} component
 * @param {Object} data
 * @return {Object} updated state
 */
function updateComponentData(state, component, data) {
  return component == null ? state : {
    ...state,
    [component] : {
      ...state[component],
      ...data
    }
  };
}

/**
 * Update attributes
 * @param {Object} state
 * @param {Object[]} data
 * @return {Object} updated state
 */
function updateMultipleComponentData(state= {}, data= []) {
  let componentData = data.reduce((componentList, component) => {
    componentList[component.id] = {...state[component.id], ...component.data};
    return componentList;
  }, {});
  return {
    ...state,
    ...componentData
  };
}

/**
 * Update multiple model
 * @param {Object} state
 * @param {Object} data
 * @return {Object} updated state
 */
function updateMultipleModel(state, data) {
  console.debug("UPDATE MULTIPLE MODELS", data);
  let componentActions = data.filter(component => !(component.address.row && component.address.column));
  let cellActions = data.filter(component => (component.address.row && component.address.column));

  // Split cell actions into grids
  let cellActionsGrid = cellActions.reduce((grids, action) => {
    let keysWithoutEvent = _.pullAll(Object.keys(action.data), ["event"]);
    let isModel = keysWithoutEvent.length > 1 || !keysWithoutEvent.includes("selected");
    let isSelected = "selected" in action.data;
    // Add action to grid actions
    if (!(action.address.component in grids)) {
      grids[action.address.component] = {"model": [], "selected": []};
    }

    // Add model action
    if (isModel) {
      grids[action.address.component]["model"].push(action);
    }

    // Add selected action
    if (isSelected) {
      grids[action.address.component]["selected"].push(action);
    }
    return grids;
  }, {});

  // Generate component data
  let componentData = componentActions.reduce((components, component) => {
    let keysWithoutEvent = _.pullAll(Object.keys(component.data), ["event"]);
    let isModel = keysWithoutEvent.length > 1 || !keysWithoutEvent.includes("selected");
    let isSelected = "selected" in component.data;

    // Update model if there are more attributes than 'selected' or if there is only one attribute and is not 'selected'
    if (isModel) {
      Object.assign(components, getModelUpdate(state, component.address, component.data));
    }
    // Update selected if defined
    if (isSelected) {
      let update = getSelectedUpdate({...state, ...components}, component.address, component.data)
      if (update !== null) {
        Object.assign(components, update);
      }
    }
    return components;
  }, {});

  let newState = {...state, ...componentData};

  // Generate cell data
  let cellData = Object.entries(cellActionsGrid).reduce((components, [key, value]) => {
    value.model.length && Object.assign(components, getMultipleCellModel(newState, key, value.model));
    value.selected.length && Object.assign(components, getMultipleCellSelected({...newState, ...components}, key, value.selected));
    return components;
  }, {});

  // Return new state
  return {
    ...newState,
    ...cellData
  };
}

/**
 * Generate cell components for a grid
 * @param {object} state State
 * @param {object} grid Grid component
 * @param {array} rows Rows to generate
 */
function generateCellComponents(state, grid, rows) {
  let model = grid.model;
  let columns = grid.attributes.columnModel.filter(column => column.component);
  let cellComponents = {};
  model.values
    .filter(row => rows.includes(row.id))
    .forEach(row => columns
      .filter(column => !(getComponentId({...grid.address, row: row.id, column: column.name}) in state))
      .forEach(column => Object.assign(cellComponents, generateCellComponent(grid, {...grid.address, row: row.id, column: column.name}, row[column.name], column))));
  return cellComponents;
}

/**
 * Generate a cell component in redux model
 * @param {object} grid Grid component
 * @param {object} address Address
 * @param {object} cellModel Cell model
 * @param {object} cellAttributes Cell attributes
 * @return {object} Component data
 */
function generateCellComponent(grid, address, cellModel, cellAttributes) {
  let model = fixCellModel(cellModel, cellAttributes.model);
  let componentId = getComponentId(address);
  return {
    [componentId] : {
      uid: getUID(),
      address: { ...address},
      model: { ...model },
      storedModel: { ...model },
      attributes: { ...cellAttributes },
      storedAttributes: { ...cellAttributes },
      actions: cellAttributes.actions || [],
      dependencies: cellAttributes.dependencies || [],
      status: cellAttributes.dependencies.length > 0 ? STATUS_DEFINED : STATUS_INITIALIZED
    }
  };
}

/**
 * Update cells model for a grid
 * @param {object} state State
 * @param {object} grid Grid component
 */
function updateCellsModel(state, grid) {
  let model = grid.model;
  let columns = grid.attributes.columnModel.filter(column => column.component);
  let cellComponents = {};
  model.values
    .forEach(row => columns
      .filter(column => getComponentId({...grid.address, row: row.id, column: column.name}) in state)
      .forEach(column => Object.assign(cellComponents,
        getModelUpdate(state,
          {...grid.address, row: row.id, column: column.name},
          fixCellModel(row[column.name], column.model)
        )
      ))
    );
  return cellComponents;
}

/**
 * Get cell model update
 * @param {object} state State
 * @param {object} address Address
 * @param {object} model Cell model
 * @returns {object} Component update
 */
function getModelUpdate(state, address, model) {
  let componentId = getComponentId(address);
  return {
    [componentId]: {
      ...state[componentId],
      model: {
        ...state[componentId].model,
        ...model,
        changed: true
      }
    }
  };
}

/**
 * Get cell model update
 * @param {object} state State
 * @param {object} address Address
 * @param {object} model Cell model
 * @param {boolean} update Update cells model
 * @returns {object} Component update
 */
function getGridModelUpdate(state, address, model, update = true) {
  let componentId = getComponentId(address);
  let gridComponent = getModelUpdate(state, address, model);
  let cellsState = model.values && update ? updateCellsModel({...state, ...gridComponent}, gridComponent[componentId]) : {};
  return {
    ...gridComponent,
    ...cellsState
  };
}

/**
 * Update model
 * @param {object} state
 * @param {object} address
 * @param {object} data
 * @param {boolean} update Update grid components
 * @return {object} updated state
 */
function updateModel(state, address, data, update = true) {
  let componentId = getComponentId(address);
  let component = state[componentId];
  if (component === null) return state;
  let newModel = isGrid(component) && data.values && update ?
    getGridModelUpdate(state, address, data, update) :
    getModelUpdate(state, address, data);
  return {
    ...state,
    ...newModel
  };
}

/**
 * Update cell model
 * @param {Object} state
 * @param {Object} address
 * @param {Object} data
 * @return {Object} updated state
 */
function updateCellModel(state, address, data) {
  let gridId = address.component;
  let values = state[gridId].model.values;
  let rowIndex = values.findIndex((row) => String(row.id) === String(address.row));
  return {
    ...updateModel(state, address, data),
    [gridId] : {
      ...state[gridId],
      model: {
        values: [
          ...values.slice(0, rowIndex),
          {
            ...values[rowIndex],
            [address.column] : data.values || null
          },
          ...values.slice(rowIndex + 1, values.length)
        ]
      }
    }
  };
}

/**
 * Update cell selected
 * @param {object} state
 * @param {object} address
 * @param {array} data
 * @return {object} updated state
 */
function updateCellSelected(state, address, data) {
  let gridId = address.component;
  let values = state[gridId].model.values;
  let rowIndex = values.findIndex((row) => String(row.id) === String(address.row));
  let sameValue = checkSelected(data, values[rowIndex][address.column]);
  return sameValue ? state : {
    ...updateSelected(state, address, {selected: data}),
    [gridId] : {
      ...state[gridId],
      model: {
        values: [
          ...values.slice(0, rowIndex),
          {
            ...values[rowIndex],
            [address.column] : fixCellModel(data, values[rowIndex][address.column]).values
          },
          ...values.slice(rowIndex + 1, values.length)
        ]
      }
    }
  };
}

/**
 * Get multiple cell model
 * @param {object} state State
 * @param {string} gridId Grid identifier
 * @param {array} actions Action list
 * @returns {object} Updated model
 */
function getMultipleCellModel(state, gridId, actions) {
  let values = state[gridId].model.values;
  let rowsToUpdate = getRowsToUpdate(actions);

  // Get indexes && prepare values
  rowsToUpdate = rowsToUpdate.map(rowActions => ({
    ...rowActions,
    index: values.findIndex((row) => String(row.id) === String(rowActions.id)),
    values: rowActions.actions.reduce((rowValues, cell) => {
        rowValues[cell.address.column] = cell.data.values || null;
        return rowValues;
      }, {})
  }));

  // Return updated values
  return {
    ...actions.reduce((components, component) => {
      Object.assign(components, getModelUpdate(state, component.address, component.data));
      return components;
    }, {}),
    ...getGridUpdateMultipleValues(state, gridId, rowsToUpdate, values)
  };
}

/**
 * Get multiple cell selected
 * @param {object} state State
 * @param {string} gridId Grid identifier
 * @param {array} actions Action list
 * @returns {object} Updated model
 */
function getMultipleCellSelected(state, gridId, actions) {
  let values = state[gridId].model.values;
  let rowsToUpdate = getRowsToUpdate(actions);

  // Get indexes && prepare values
  rowsToUpdate = rowsToUpdate.map(rowActions => {
    let rowIndex = values.findIndex((row) => String(row.id) === String(rowActions.id));
    let rowActionsFiltered = rowActions.actions.filter(action => !checkSelected(asArray(action.data.selected), values[rowIndex][action.address.column]));
    return {
      id: rowActions.id,
      index: rowIndex,
      actions: rowActionsFiltered,
      values: rowActionsFiltered.reduce((rowValues, cell) => {
        rowValues[cell.address.column] = fixCellModel(cell.data.selected, values[rowIndex][cell.address.column]).values;
        return rowValues;
      }, {})
    };
  });

  // Retrieve filtered actions
  let filteredActions = _.flatten(rowsToUpdate.map(row => row.actions));

  // Return updated values
  return {
    ...filteredActions.reduce((components, component) => {
      let update = getSelectedUpdate(state, component.address, component.data);
      if (update !== null) {
        Object.assign(components, update);
      }
      return components;
    }, {}),
    ...getGridUpdateMultipleValues(state, gridId, rowsToUpdate, values)
  };
}

/**
 * Retrieve rows to update in actions
 * @param {array} actions
 * @returns {array} Rows to update
 */
function getRowsToUpdate(actions) {
  // Get rows to update
  return Object.values(actions.reduce((rows, component) => {
    if (!(component.address.row in rows)) {
      rows[component.address.row] = {id: component.address.row, actions: []};
    }
    rows[component.address.row].actions.push(component);
    return rows;
  }, {}));
}

/**
 * Get grid update multiple values
 * @param {object} state
 * @param {string} gridId
 * @param {array} rowsToUpdate
 * @param {array} values
 * @returns {object} Grid update
 */
function getGridUpdateMultipleValues(state, gridId, rowsToUpdate, values) {
  let filteredRows = rowsToUpdate.filter(row => row.actions.length > 0);
  return filteredRows.length === 0 ? {} : {
    [gridId] : {
      ...state[gridId],
      model: {
        values: filteredRows.reduce((previousValues, rowActions) => {
          return [
            ...previousValues.slice(0, rowActions.index),
            {
              ...previousValues[rowActions.index],
              ...rowActions.values
            },
            ...previousValues.slice(rowActions.index + 1, previousValues.length)
          ];
        }, values)
      }
    }
  };
}

/**
 * Update selected
 * @param {object} state
 * @param {object} values
 * @param {object} address
 * @param {object} selected
 * @param {string} event
 * @return {object} updated state
 */
function updateSelectedGrid(state, values, address, selected, event) {
  // Get values to unselect
  let filtered = values;
  let toUnselect = values
    .filter(row => row.selected)
    .filter(row => !selected.includes(row.id))
    .map(row => ({id: row.id, selected: false}));
  let toSelect = selected
    .map(value => ({id: value, selected: true}));
  // Unselect values
  [...toUnselect, ...toSelect].forEach(item => {
    let index = filtered.findIndex((row) => String(row.id) === String(item.id));
    filtered = [...filtered.slice(0, index), {...filtered[index], selected: item.selected}, ...filtered.slice(index+1, filtered.length)];
  });

  // Update values
  return getGridModelUpdate(state, address, {values: filtered, event: event}, false);
}

/**
 * Update selected
 * @param {object} state
 * @param {array} values
 * @param {object} address
 * @param {array} selected
 * @param {string} event
 * @return {object} updated state
 */
function updateSelectedComponent(state, values, address, selected, event) {
  let component = state[getComponentId(address)];
  let filtered = getFilteredValues(values, selected);
  if (_.isEqual(filtered, component.model.values)) {
    return null;
  }

  // Update values
  return getModelUpdate(state, address, {values: filtered, event: event});
}

/**
 * Retrieve filtered values
 * @param {array} values Values
 * @param {array} selected Selected
 * @returns {*} Filtered values
 */
function getFilteredValues(values, selected) {
  let filtered = values.map((value) => ({ ...value, selected: selected.includes(value.value)}));
  return filtered.filter(value => value.selected).length === 0 ?
    selected.map(value => (angular.isObject(value) ?
      {...value, selected: true} : {value: value, selected: true})) :
    filtered;
}

/**
 * Update selected
 * @param {object} state
 * @param {array} values
 * @param {object} view
 * @param {object} group
 * @param {array} selected
 * @param {string} event
 * @return {Object} updated state
 */
function updateSelectedGroup(state, values, view, group, selected, event) {
  // Check equality to avoid update state if there are no changes
  return {
    ...Object.entries(state)
      .filter(([name, component]) => (component.address || {}).view === view && (component.attributes || {}).group === group)
      .map(([name, component]) => ({
        name: name,
        value: {
          ...component,
          model: {
            ...component.model,
            values: component.model.values.map(value => ({...value, selected: selected.includes(value.value)})),
            changed: true,
            event: event
          }
        }
      }))
      .reduce((current, entry) => ({...current, [entry.name]: entry.value}), {})
  };
}

/**
 * Update selected
 * @param {object} state
 * @param {object} address
 * @param {object} data
 * @return {object} updated state
 */
function updateSelected(state, address, data) {
  let update = getSelectedUpdate(state, address, data);
  if (update) {
    return {
      ...state,
      ...update
    };
  } else {
    return state;
  }
}

/**
 * Get update to be done
 * @param {object} state State
 * @param {object} address Address
 * @param {object} data Data
 * @return {object|null} Update to apply
 */
function getSelectedUpdate(state, address, data) {
  let component = state[getComponentId(address)];
  let values = component.model.values || [];
  let selected = asArray(data.selected);
  if (isGrid(component)) {
    return updateSelectedGrid(state, values, address, selected, data.event || null);
  } else if (isGroup(component)) {
    return updateSelectedGroup(state, values, address.view, component.attributes.group, selected, data.event || null);
  } else {
    return updateSelectedComponent(state, values, address, selected, data.event || null);
  }
}

/**
 * Check if selected values are the same as cell values
 * @param {array} selectedValues Selected values
 * @param {*} cellValue Cell value
 * @return {boolean} Same selected value
 */
function checkSelected(selectedValues, cellValue) {
  let selectedAsString = selectedValues.map(item => String(item));
  if (angular.isArray(cellValue)) {
    return cellValue.filter(item => item.selected && selectedAsString.includes(String(item.value))).length === 1;
  } else if (angular.isObject(cellValue) && "value" in cellValue) {
    return selectedAsString.includes(String(cellValue.value));
  } else {
    return selectedAsString.includes(String(cellValue));
  }
}

/**
 * Update cell values
 * @param {Object} state
 * @param {Object} address
 * @param {Object} data
 * @return {Object} updated state
 */
function updateRowModel(state, address, data) {
  let values = state[address.component].model.values;
  let rowIndex = values.findIndex((row) => String(row.id) === String(address.row));
  return {
    ...state,
    [address.component] : {
      ...state[address.component],
      model: {
        values: [
          ...values.slice(0, rowIndex),
          data,
          ...values.slice(rowIndex + 1, values.length)
        ]
      }
    }
  };
}

/**
 * Keep model component
 * @param {Object} state
 * @param {Object} component
 * @return {Object} updated state
 */
function keepModelComponent(state, component) {
  return {
    ...state,
    ...getKeepModelComponent(state, component)
  };
}

/**
 * Get changes for keep model component
 * @param state
 * @param component
 * @returns {{}}
 */
function getKeepModelComponent(state, component) {
  return {
    [component] : {
      ...state[component],
      storedModel: {
        ...state[component].model,
        values: state[component].model.values.map(value => ({...value}))
      }
    }
  };
}

/**
 * Keep row model
 * @param {Object} state
 * @param {Object} address
 * @return {Object} updated state
 */
function keepRowModel(state, address) {
  let values = state[address.component].model.values;
  let rowIndex = values.findIndex((row) => String(row.id) === String(address.row));
  let rowValues = values[rowIndex];
  let cellModel = {};
  Object.keys(rowValues).forEach(column => {
    let cellAddress = {...address, column: column};
    let componentId = getComponentId(cellAddress);
    if (componentId in state) {
      cellModel = {...cellModel, ...getKeepModelComponent(state, componentId)};
    }
  });
  return {
    ...state,
    ...cellModel,
    [address.component] : {
      ...state[address.component],
      storedModel: {
        ...state[address.component].storedModel,
        storedRows: {
          ...state[address.component].storedModel.storedRows,
          [address.row]: {
            ...values[rowIndex]
          }
        }
      }
    }
  };
}

/**
 * Keep model component
 * @param {Object} state
 * @param {Object} component
 * @return {Object} updated state
 */
function restoreModelComponent(state, component) {
  return {
    ...state,
    ...getRestoreModelComponent(state, component)
  };
}

/**
 * Get changes for restore model component
 * @param state
 * @param component
 * @returns {{}}
 */
function getRestoreModelComponent(state, component) {
  return {
    [component] : {
      ...state[component],
      model: {
        ...state[component].storedModel,
        values: state[component].storedModel.values.map(value => ({...value})),
        changed: false,
        event: "restore"
      }
    }
  };
}

/**
 * Restore row model
 * @param {Object} state
 * @param {Object} address
 * @return {Object} updated state
 */
function restoreRowModel(state, address) {
  let values = state[address.component].model.values;
  let rowIndex = values.findIndex((row) => String(row.id) === String(address.row));
  let rowValues = values[rowIndex];
  let cellModel = {};

  // Get components model
  Object.keys(rowValues).forEach(column => {
    let cellAddress = {...address, column: column};
    let componentId = getComponentId(cellAddress);
    if (componentId in state) {
      cellModel = {...cellModel, ...getRestoreModelComponent(state, componentId)};
    }
  });

  // Generate new state
  return {
    ...state,
    ...cellModel,
    [address.component] : {
      ...state[address.component],
      model: {
        values: [
          ...values.slice(0, rowIndex),
          {...state[address.component].storedModel.storedRows[address.row], selected: false},
          ...values.slice(rowIndex + 1, values.length)
        ]
      }
    }
  };
}

/**
 * Reset model
 * @param {Object} state
 * @param {Object} address
 * @param {Object} data
 * @return {Object} updated state
 */
function resetModel(state, address, data) {
  if (isGrid(state[getComponentId(address)])) {
    return {
      ...state,
      [address.component] : {
        ...state[address.component],
        model: {
          ...state[address.component].model,
          values: [],
          page: 1,
          total: 1,
          records: 0
        }
      }
    };
  } else {
    return updateSelected(state, address, {selected: state[address.component].model.defaultValues, event: "reset"});
  }
}

/**
 * Update cell model
 * @param {Object} state
 * @param {Object} address
 * @param {Object} data
 * @return {Object} updated state
 */
function resetCellModel(state, address, data) {
  let columnModel = state[address.component].attributes.columnModel.filter(value => value.id === address.column)[0] || {};
  return updateCellSelected(state, address, asArray((columnModel.model || {}).values));
}

/**
 * Update model
 * @param {object} state State
 * @param {object} action Action
 * @returns {*} Action updated
 */
function updateModelAction(state = {}, action = {}) {
  let modelState = state;
  let data = {...action.data};
  let selectedData = [...asArray(data.selected)];
  let keysWithoutEvent = _.pullAll(Object.keys(data), ["event"]);
  if (keysWithoutEvent.length > 1 || !keysWithoutEvent.includes("selected")) {
    modelState = launchAddressFunction(updateCellModel, updateModel, modelState, {address: action.address, data: data, event: data.event || null});
  }
  if ("selected" in data) {
    modelState = launchAddressFunction(updateCellSelected, updateSelected, modelState, {address: action.address, data: {selected: selectedData}, event: data.event || null});
  }
  return modelState;
}

/**
 * Components reducer
 */
export function components(state = {}, action = {}) {
  switch (action.type) {
    case CLEAR_COMPONENTS:
      // Remove the current view components
      return Object.entries(state)
        .filter((entry) => entry[1].address.view !== action.view)
        .reduce((obj, entry) => {
          obj[entry[0]] = entry[1];
          return obj;
        }, {});
    case UPDATE_COMPONENTS:
      return {
        ...state,
        ...action.data
      };
    case GENERATE_CELL_COMPONENTS:
      return {
        ...state,
        ...generateCellComponents(state, state[action.address.component], action.data)
      };
    case UPDATE_COMPONENT:
      return updateComponentData(state, getComponentId(action.address), action.data);
    case UPDATE_MULTIPLE_COMPONENTS:
      return updateMultipleComponentData(state, action.data);
    case UPDATE_MULTIPLE_MODELS:
      return updateMultipleModel(state, action.data);
    case UPDATE_ATTRIBUTES:
      return updateAttributesComponent(state, getComponentId(action.address), action.data);
    case UPDATE_MULTIPLE_ATTRIBUTES:
      for (let component of action.componentList) {
        state = updateAttributesComponent(state, getComponentId(component.address), component.data);
      }
      return state;
    case UPDATE_MODEL:
      return updateModelAction(state, action);
    case UPDATE_ROW_MODEL:
      return updateRowModel(state, action.address, action.data);
    case UPDATE_VALIDATION:
      return updateValidationComponent(state, getComponentId(action.address), action.data);
    case KEEP_VALIDATION:
      return keepValidationComponent(state, getComponentId(action.address));
    case KEEP_ATTRIBUTE:
      return keepAttributeComponent(state, getComponentId(action.address), action.data);
    case KEEP_MODEL:
      return keepModelComponent(state, getComponentId(action.address));
    case KEEP_ROW_MODEL:
      return keepRowModel(state, action.address);
    case RESTORE_VALIDATION:
      return restoreValidationComponent(state, getComponentId(action.address));
    case RESTORE_ATTRIBUTE:
      return restoreAttributeComponent(state, getComponentId(action.address), action.data);
    case RESTORE_MODEL:
      return restoreModelComponent(state, getComponentId(action.address));
    case RESTORE_ROW_MODEL:
      return restoreRowModel(state, action.address);
    case RESET_MODEL:
      return launchAddressFunction(resetCellModel, resetModel, state, {address: action.address, data: []});
    default:
      return state;
  }
}
