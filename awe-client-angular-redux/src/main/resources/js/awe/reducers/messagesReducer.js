import {UPDATE_MESSAGES, CONFIRM_MESSAGE} from '../actions/messages';

/**
 * Messages reducer
 * @param state Old state
 * @param action Action
 * @returns New state
 */
export function messages(state = {}, action = {}) {
  switch (action.type) {
    case UPDATE_MESSAGES:
      return {
        ...state,
        [action.view]: {
          ...action.data
        }
      }
    case CONFIRM_MESSAGE:
      return {
        ...state,
        confirm: {
          ...action.data
        }
      }
    default:
      return state
  }
}