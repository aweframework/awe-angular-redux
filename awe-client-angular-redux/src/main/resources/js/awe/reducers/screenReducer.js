import {CLEAR_SCREEN_VIEW, SET_SCREEN_VIEW, UPDATE_SCREEN, UPDATE_SCREEN_VIEW} from '../actions/screen';

/**
 * Views reducer
 * @param state Old state
 * @param action Action
 * @returns New state
 */
export function screen(state = {}, action = {}) {
  switch (action.type) {
    case UPDATE_SCREEN:
      return {
        ...state,
        ...action.data
      }
    case SET_SCREEN_VIEW:
      return {
        ...state,
        [action.view]: action.data
      }
    case UPDATE_SCREEN_VIEW:
      return {
        ...state,
        [action.view]: {
          ...state[action.view],
          ...action.data
        }
      }
    case CLEAR_SCREEN_VIEW:
      return {
        ...state,
        [action.view]: {}
      }
    default:
      return state
  }
}