import { aweApplication } from "../awe";

// Get bootstrap tabdrop
import "../../lib/bootstrap-tabdrop/bootstrap-tabdrop";

// UI Tabdrop directive
aweApplication.directive('uiTabdrop',
  ['AweUtilities',
    /**
     * UI Tab drop
     * @param {type} $utilities
     */
    function ($utilities) {
      return {
        // This directive only works when used in element's attribute (e.g: ui-time)
        restrict: 'A',
        priority: 1,
        compile: () => ($scope, $element) => {
          $element.tabdrop();
          $scope.$on("selectTab", () => $element.tabdrop('layout'));
        }
      };
    }
  ]);
