import { aweApplication } from "../awe";

// Translate multiple directive
aweApplication.directive('preventAnimations', ['$animate', ($animate) => ({restrict: 'A', link: ($scope, $element) => $animate.enabled($element,false)})]);