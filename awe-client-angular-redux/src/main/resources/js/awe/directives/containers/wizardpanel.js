import { aweApplication } from "../../awe";
import { getContainerTemplate } from "../../services/container";

// Wizard Panel directive
aweApplication.directive('aweWizardPanel', () => ({
  transclude: true,
  replace: true,
  restrict: "E",
  template: getContainerTemplate("wizard"),
  controllerAs: '$ctrl',
  bindToController: true,
  scope: {
    'id': '@wizardPanelId'
  },
  require: '^aweInputWizard',
  link: function ($scope, $element, $attrs, $parentController) {
    $scope.$parentCtrl = $parentController;
  },
  controller: ['$scope', '$element', 'Container', ($scope, $element, $container) => {
    // Initialize controller
    let $ctrl = $scope.$ctrl;

    // Initialize as container
    $container.init($ctrl, $scope, $element);
  }]
}));
