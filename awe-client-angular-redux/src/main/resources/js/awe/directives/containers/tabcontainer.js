import { aweApplication } from "../../awe";
import { getContainerTemplate } from "../../services/container";

// Tabcontainer directive
aweApplication.directive('aweTabcontainer', () => ({
  transclude: true,
  replace: true,
  restrict: "E",
  template: getContainerTemplate("tab"),
  controllerAs: '$ctrl',
  bindToController: true,
  scope: {
    'id': '@tabcontainerId'
  },
  require: '^aweInputTab',
  link: function ($scope, $element, $attrs, $parentController) {
    $scope.$parentCtrl = $parentController;
  },
  controller: ['$scope', '$element', 'Container',($scope, $element, $container) => {
    // Initialize controller
    let $ctrl = $scope.$ctrl;

    // Initialize as container
    $container.init($ctrl, $scope, $element);
  }]
}));


