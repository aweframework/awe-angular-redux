import { aweApplication } from "../awe";
import "bootstrap-timepicker";
import {initializePlugin} from "./plugins";

// Time plugin
aweApplication.directive('uiTime',
  ['AweUtilities',
    function ($utilities) {
      return {
        restrict: 'A',
        scope: {
          config: '=uiTime',
          initialize: '='
        },
        link: function ($scope, $element, $attributes) {
          /**
           * Specific plugin definition
           */
          function definePlugin(options) {
            // Generate element
            $element.timepicker(options);

            // Set timepicker to 0:00:00 without updating value
            $element.timepicker('setTime', null);

            // Timepicker events
            $element.on('keydown.timepicker', (e) => e.which === '9' ? $element.timepicker('hideWidget') : null);
            $scope.$on('trigger-change', () => $utilities.timeout(() => $element.timepicker('setTime', $element.val() || null)));
          }

          // Initialize plugin
          initializePlugin($scope, $element, $attributes, {}, definePlugin);
        }
      };
    }
  ]);