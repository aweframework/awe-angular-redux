import { aweApplication } from "../awe";

// Translate multiple directive
aweApplication.directive('closeWithDropdown', () => ({
  restrict: 'A',
  require: 'uiSelect',
  link: ($scope, $element, $attrs, $select) => $scope.$on('dropdown-hidden', () => $select.close())
}));