import { aweApplication } from "../awe";
import "select2";
import {initializePlugin} from "./plugins";

// Select plugin
aweApplication.directive('uiSelect',
  ['AweUtilities',
    function ($utilities) {
      // Generate plugin options
      let defaultOptions = {
        theme: "bootstrap",
        width: "100%",
        placeholder: {id: "?"},
        allowClear: true
      };

      return {
        restrict: 'A',
        scope: {
          onRefresh: '=',
          config: '=',
          initialize: '='
        },
        link: function ($scope, $element, $attributes) {
          /**
           * Specific plugin definition
           * @param {Object} options Options
           */
          function definePlugin(options) {
            // Generate element
            $element.select2(options);

            // Events
            $element.on('select2:unselecting', () => $element.one('select2:opening', (e) => e.preventDefault()));
            $scope.$on('trigger-change', () => $utilities.timeout(() => $element.trigger('change.select2')));
          }

          /**
           * Specific options update
           * @param {Object} options Options
           */
          function updateOptions(options, $scope) {
            let ajax = {};
            if ("ajax" in options) {
              ajax = {
                ajax: {
                  ...options.ajax,
                  delay: $scope.config.delay
                }
              };
            }
            return {
              ...options,
              ...$scope.config,
              ...ajax,
              placeholder: $scope.config.optional ? {id: "?", text: $scope.config.placeholder} : {},
              allowClear: $scope.config.optional
            };
          }

          // Define specific default options
          let options = defaultOptions;
          if ($scope.onRefresh) {
            options = {
              ...options,
              ajax: {
                delay: 200,
                transport: $scope.onRefresh
              }
            };
          }

          // Initialize plugin
          initializePlugin($scope, $element, $attributes, options, definePlugin, updateOptions);
        }
      };
    }
  ]);