import { aweApplication } from "../awe";
import AutoNumeric from "autonumeric";
import {initializePlugin} from "./plugins";
import _ from "lodash";

// Numeric plugin
aweApplication.directive('uiNumeric', function () {
  return {
    // This directive only works when used in element's attribute (e.g: ui-numeric)
    restrict: 'A',
    priority: 1,
    scope: {
      config: '=uiNumeric',
      initialize: '='
    },
    link: function ($scope, $element, $attributes) {
      // Initialize controller
      let $ctrl = $scope.$parent.$ctrl;

      /**
       * Update value
       */
      function updateValue() {
        let newValue = $ctrl.autonumeric.getNumber();
        if (newValue !== ($ctrl.value || {}).value) {
          $ctrl.value = {value: newValue, selected: true};
          $ctrl.onChange();
        }
      }

      /**
       * Specific plugin definition
       * @param {Object} options Options
       */
      function definePlugin(options) {
        // Generate element
        $ctrl.autonumeric = new AutoNumeric($element[0], options);
        $element.on("change", updateValue);
        $ctrl.updateNumericModel();
      }

      // Initialize plugin
      initializePlugin($scope, $element, $attributes, {}, definePlugin);

      /**
       * On numeric change
       * @param {object} options Slider options
       */
      $ctrl.onNumericChange = function(options) {
        if (!$ctrl.autonumeric) return;
        $ctrl.autonumeric.update(options);
        $element.trigger("change");
      };

      /**
       * Update model
       */
      $ctrl.updateNumericModel = function() {
        if (!$ctrl.autonumeric) return;
        let value = ($ctrl.value || {}).value;
        if ($ctrl.autonumeric.getNumber() !== value) {
          $ctrl.autonumeric.set(value);
          $element.trigger("change");
        }
      };

      // Initialize numeric plugin
      /*initializeNumericPlugin($scope, defaultOptions, {
        definePlugin: definePlugin,
        updateModel: updateModel,
        processNumericOptions: processNumericOptions,
        deletePlugin: deletePlugin
      }, {
        $ngRedux: $ngRedux,
        $utilities: $utilities
      });*/

      /**
       * Update model on state change
       * @param {Object} $api Plugin api
       * @param {Number} value Value to set
       */
      /*function updateModel($api, value) {
        if (value.value!== $api.getNumber()) {
          $api.set(value.value);
          if (!value.formatted) {
            $element.trigger("change");
          }
        }
      }*/

      /**
       * Update number format
       * @param {object} $ctrl Controller
       */
      /*function updateFormat($ctrl) {
        let formatted = {
          values: [{
            value: $ctrl.api.getNumber(),
            label: $ctrl.api.getFormatted(),
            formatted: true,
            selected: true
          }]
        };

        // Change model deferred
        $control.changeModelDeferred($ctrl.address, formatted);
      }*/

      /**
       * Define plugin
       * @param {Object} options Numeric options
       * @param {Object} previousOptions Numeric options
       * @param {Object} state Attributes state
       * @param {Function} onModelUpdate On model update function
       * @param {Object} $ctrl Controller
       */
      /*function definePlugin(options, previousOptions, state, onModelUpdate, $ctrl) {
        if (!$ctrl.initialized) {
          $ctrl.initialized = true;
          $ctrl.api = new AutoNumeric($element[0], options);

          // Bind changes
          $element.on("change", () => updateFormat($ctrl));

          // Add model status bindings
          let getModelUpdate = (state => ({model: (state.components[$ctrl.id] || {}).model, uid: (state.components[$ctrl.id] || {}).uid}));
          $ctrl.events.push($ngRedux.connect(getModelUpdate)(onModelUpdate));
        } else if (!_.isEqual(options, previousOptions)){
          $ctrl.api.update(options);
          $element.trigger("change");
        }
      }*/

      /**
       * Delete plugin
       * @param {Object} $api Plugin API
       */
      /*function deletePlugin($api) {
        if ($api) {
          $api.remove();
        }
      }*/
    }
  };
});
