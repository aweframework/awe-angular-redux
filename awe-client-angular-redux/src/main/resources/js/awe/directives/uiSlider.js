import { aweApplication } from "../awe";
import Slider from "bootstrap-slider";
import {initializePlugin} from "./plugins";
import _ from "lodash";

// Numeric plugin
aweApplication.directive('uiSlider', function () {
  return {
    // This directive only works when used in element's attribute (e.g: ui-numeric)
    restrict: 'A',
    priority: 1,
    scope: {
      config: '=uiSlider',
      initialize: '='
    },
    link: function ($scope, $element, $attributes) {
      // Initialize controller
      let $ctrl = $scope.$parent.$ctrl;

      /**
       * Update value
       */
      function updateValue() {
        let newValue = $ctrl.slider.getValue();
        if (Number(newValue) !== Number(($ctrl.value || {}).value)) {
          $ctrl.value = {value: newValue, selected: true};
          $ctrl.onChange();
        }
      }

      /**
       * Check if slider is readonly
       * @param {object} options Options
       */
      function checkReadonly(options) {
        // Enable/disable
        if (options.readonly) {
          $ctrl.slider.disable();
        } else {
          $ctrl.slider.enable();
        }
      }

      /**
       * Specific plugin definition
       * @param {Object} options Options
       */
      function definePlugin(options) {
        // Generate element
        $ctrl.slider = new Slider($element[0], options);
        $element.on("slideStop", updateValue);
        $ctrl.updateSliderModel();
        checkReadonly(options);
      }

      /**
       * On slider change
       * @param {object} options Slider options
       */
      $ctrl.onSliderChange = function(options) {
        if ($ctrl.slider) {
          _.each(options, (value, attribute) => $ctrl.slider.setAttribute(attribute, value));
          $ctrl.slider.refresh();
          checkReadonly(options);
        }
      };

      /**
       * Update model
       */
      $ctrl.updateSliderModel = function() {
        if (!$ctrl.slider) return;
        let value = ($ctrl.value || {}).value;
        if ($ctrl.slider.getValue() !== value) {
          $ctrl.slider.setValue(value);
          $ctrl.slider.setAttribute("value", value);
        }
      };

      // Initialize plugin
      initializePlugin($scope, $element, $attributes, {}, definePlugin);

      // Initialize numeric plugin
      /*initializeNumericPlugin($scope, defaultOptions, {
        definePlugin: definePlugin,
        updateModel: updateModel,
        processNumericOptions: processNumericOptions,
        deletePlugin: deletePlugin
      }, {
        $ngRedux: $ngRedux,
        $utilities: $utilities
      });*/

      /**
       * Update model on state change
       * @param {object} $api Plugin api
       * @param {object} value Value to set
       */
      /*function updateModel($api, value) {
        $api.setValue(value.value);
        $api.setAttribute("value", value.value);
      }*/

      /**
       * Define plugin
       * @param {Object} options Numeric options
       * @param {Object} previousOptions Numeric options
       * @param {Object} state Attributes state
       * @param {Function} onModelUpdate On model update function
       * @param {Object} $ctrl Controller
       */
      /*function definePlugin(options, previousOptions, state, onModelUpdate, $ctrl) {
        if (!$ctrl.initialized) {
          $ctrl.initialized = true;
          $ctrl.api = new Slider($element[0], options);

          // Bind changes
          $element.on("slideStop", () => {
            $control.changeModel($ctrl.address, { selected: [$ctrl.api.getValue()]});
          });

          // Add model status bindings
          let getModelUpdate = (state => ({model: (state.components[$ctrl.id] || {}).model, uid: (state.components[$ctrl.id] || {}).uid}));
          $ctrl.events.push($ngRedux.connect(getModelUpdate)(onModelUpdate));

          // Add screen visible status binding
          let getScreenUpdate = (state => ({visible: state.screen[$ctrl.address.view].visible}));
          let disconnectScreenUpdate = $ngRedux.connect(getScreenUpdate)((state) => {
            if (state.visible && $ctrl.api) {
              $utilities.timeout(() => $ctrl.api.relayout(), 200);
              disconnectScreenUpdate && disconnectScreenUpdate();
            }
          });
          $ctrl.events.push(disconnectScreenUpdate);
        } else if (!_.isEqual(options, previousOptions)) {
          _.each(options, (value, attribute) => $ctrl.api.setAttribute(attribute, value));
          $ctrl.api.refresh();
        }

        // Enable/disable
        if (state.readonly) {
          $ctrl.api.disable();
        } else {
          $ctrl.api.enable();
        }
      }*/

      /**
       * Delete plugin
       * @param {Object} $api Plugin API
       */
      /*function deletePlugin($api) {
        if ($api) {
          $api.destroy();
        }
      }*/
    }
  };
});
