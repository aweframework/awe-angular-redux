import { aweApplication } from "../awe";

// UI Modal directive
aweApplication.directive('uiModal',
  ['AweUtilities',
    function ($utilities) {
      return {
        restrict: 'A',
        priority: 1,
        scope: {
          onOpen: '&',
          onClose: '&',
          close: '&',
          opened: '='
        },
        link: function ($scope, $element, $attributes) {
          let opened = false;
          $scope.$watch("opened", (n, old) => {opened = n; opened !== old ? opened ? $element.modal('show') : $element.modal('hide') : ''});

          // Call on finish showing
          $element.on('shown.bs.modal', () => $attributes.onOpen ? $utilities.timeout(() => $scope.onOpen()) : '');

          // Call on finish hiding
          $element.on('hidden.bs.modal', () => $attributes.onClose ? $utilities.timeout(() => $scope.onClose()) : '');

          // Call on hide
          $element.on('hide.bs.modal', () => $attributes.close && opened ? $utilities.timeout(() => $scope.close()) : '');
        }
      };
    }
  ]);
