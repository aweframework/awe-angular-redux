
/**
 * Default update options
 * @param {Object} options Options
 * @param {Object} $scope Scope
 */
function defaultUpdateOptions(options, $scope) {
  return {
    ...options,
    ...$scope.config
  };
}

/**
 * Plugin standard initialization
 * @param {Object} $scope Scope
 * @param {Object} $element HTML node
 * @param {Object} $attributes HTML attributes
 * @param {Object} defaultOptions Default options
 * @param {Function} definePlugin Specific plugin definition
 * @param {Function} updateOptions Update options function
 */
export const initializePlugin = function($scope, $element, $attributes, defaultOptions, definePlugin, updateOptions = defaultUpdateOptions) {
  let initialized = false;
  let onInitialize;

  /**
   * Initialize plugin
   */
  function initPlugin(init) {
    if (init) {
      let options = updateOptions(defaultOptions, $scope);

      if (!initialized) {
        // Initialize plugin
        initialized = true;

        // Define plugin
        definePlugin(options);

        // Disconnect watch
        onInitialize();
      }
    }
  }

  onInitialize = $scope.$watch('config', initPlugin);
};

/**
 * Numeric plugin standard initialization
 * @param {Object} $scope Scope
 * @param {Object} defaultOptions Default options
 * @param {Object} specificFunctions Specific functions
 * @param {Object} services Services
 */
export const initializeNumericPlugin = function($scope, defaultOptions, specificFunctions, services) {
  let $ctrl = {events: [], initialized: false, id: $scope.id};
  let previousOptions = defaultOptions;
  const {definePlugin, updateModel, processNumericOptions, deletePlugin} = specificFunctions;
  const {$ngRedux, $utilities} = services;

  // Connect redux component update
  let getNumericOptionsUpdate = (state => ({numericOptions: ((state.components[$ctrl.id] || {}).attributes || {}).numberFormat, readonly: ((state.components[$ctrl.id] || {}).attributes || {}).readonly, address: (state.components[$ctrl.id] || {}).address, uid: (state.components[$ctrl.id] || {}).uid}));

  // Add attribute status bindings
  $ctrl.events.push($ngRedux.connect(getNumericOptionsUpdate)(onNumericOptionsUpdate));

  /**
   * On model update
   * @param {Object} state State
   */
  function onModelUpdate(state) {
    // Avoid fake updates
    if (state.uid) {
      // Calculate new value
      $ctrl.value = state.model.values
        .filter(option => option.selected)
        .reduce((previous, option) => option, {});

      // Update model
      if ($ctrl.initialized) {
        updateModel($ctrl.api, $ctrl.value);
      }
    }
  }

  /**
   * On numeric options update
   * @param {Object} state State
   */
  function onNumericOptionsUpdate(state) {
    // Avoid fake updates
    if (!state.uid) return;

    // Check numeric options
    let numericOptions = angular.isString(state.numericOptions) ? $utilities.evalJSON(state.numericOptions) : state.numericOptions || {};

    // Define options
    let options = processNumericOptions({
      ...defaultOptions,
      ...numericOptions
    });

    // Define address
    $ctrl.address = state.address;

    // Define plugin
    definePlugin(options, previousOptions, state, onModelUpdate, $ctrl);

    // Store previous options
    previousOptions = options;
  }

  /**
   * Destroy plugin
   */
  function destroy() {
    deletePlugin($ctrl.api);
    $ctrl.initialized = false;
    $utilities.clearListeners($ctrl.events);
  }

  // Observe destroy event
  $ctrl.events.push($scope.$on("$destroy", destroy));

  // Return controller
  return $ctrl;
};
