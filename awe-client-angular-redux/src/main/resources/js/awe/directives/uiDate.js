import { aweApplication } from "../awe";
import "bootstrap-datepicker";
import {initializePlugin} from "./plugins";

// Date plugin
aweApplication.directive('uiDate',
  ['AweUtilities',
    function ($utilities) {
      return {
        restrict: 'A',
        scope: {
          config: '=uiDate',
          initialize: '='
        },
        link: function ($scope, $element, $attributes) {
          /**
           * Specific plugin definition
           * @param {Object} options Options
           */
          function definePlugin(options) {
            // Generate element
            $element.datepicker(options);
            $scope.$on('trigger-change', () => $utilities.timeout(() => $element.datepicker('update')));
          }

          // Initialize plugin
          initializePlugin($scope, $element, $attributes, {}, definePlugin);
        }
      };
    }
  ]);
