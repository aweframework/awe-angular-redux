import { aweApplication } from "../awe";

// Grid header directive
aweApplication.directive('uiGridSuperHeader', ['AweUtilities', function ($utilities) {
  return {
    restrict: 'A',
    replace: true,
    transclude: true,
    template:
      `<div class="no-animate ui-grid-top-panel">
        <div class="no-animate ui-grid-header-viewport">
          <div class="no-animate grid-header-scroller ui-grid-header-canvas">
            <!-- fixes scrollbar issue -->
            <div class="no-animate ui-grid-header-cell-wrapper">
              <div class="no-animate ui-grid-header-cell ui-grid-clearfix" ng-repeat="cat in categories track by $index" style="{{'width:' + cat.width + 'px; max-width:' + cat.width + 'px; min-width:' + cat.width + 'px;'}}">
                <div class="no-animate ui-grid-cell-contents text-center">{{cat.displayName| translateMultiple}}</div>
              </div>
            </div>
          </div>
        </div>
      </div>`,
    scope: {
      'uiGridSuperHeader': '='
    },
    link: function ($scope, $element) {
      // setup listener for scroll events to sync categories with table
      let viewPort;
      let headerContainer;

      /**
       * Reload headers based on column movements
       * @param {object} data
       */
      function refreshHeaders(data) {
        let columns = data.columns;
        let headers = data.headers;
        $scope.categories = [];
        let lastHeader = null;
        let totalWidth = 0;
        $scope.categories = columns
          .filter(column => column.visible)
          .map(column => ({
            name: column.name,
            width: Math.floor(column.width),
            header: headers[column.name] || null
          }))
          .reduce((categories, column) => {
            totalWidth += column.width;
            if (column.header !== lastHeader) {
              categories.push({
                displayName: lastHeader === null ? "\u00A0" : lastHeader.label,
                width: totalWidth - column.width
              });
              totalWidth = column.width;
              lastHeader = column.header;
            }
            return categories;
          }, []);
        if (totalWidth > 0) {
          $scope.categories.push({
            displayName: lastHeader === null ? "\u00A0" : lastHeader.label,
            width: totalWidth
          });
        }

        // Update scroll
        if (data.columns.length !== 0 && data.columns[0].grid.id) {
          let grid = $(".grid" + data.columns[0].grid.id);
          let viewport = $(".ui-grid-viewport", grid);
          updateScroll({currentTarget: viewport});
        }
      }

      /**
       * Adapt scroll to viewport
       */
      function updateScroll(data) {
        // copy total width to compensate scrollbar width
        $element.find(".ui-grid-header-canvas").width($(headerContainer).find(".ui-grid-header-canvas").width());
        $element.find(".ui-grid-header-viewport").scrollLeft($(data.currentTarget).scrollLeft());
      }

      /**
       * Adapt scroll to viewport
       */
      function resize() {
        refreshHeaders($scope.uiGridSuperHeader);
      }

      // create cols as soon as $gridscope is available
      // grids in tabs with lazy loading come later, so we need to
      // setup a watcher
      let unwatch = $scope.$watch('uiGridSuperHeader', function (gridOptions) {
        if (gridOptions && "layers" in gridOptions && "viewport" in gridOptions.layers) {
          // Bind grid layers
          viewPort = gridOptions.layers.viewport;
          headerContainer = gridOptions.layers.header;

          // Bind scroll once
          angular.element(viewPort).bind("scroll", updateScroll);

          // Check if gridOptions have columnHeaders
          refreshHeaders(gridOptions);

          /******************************************************************************
           * EVENT LISTENERS
           *****************************************************************************/
          let listeners = [
            $scope.$on("columns-changed", resize),
            $scope.$on("resize", resize),
            $scope.$on("$destroy", () => $utilities.clearListeners(listeners))
          ];

          // Unwatch initialization
          unwatch();
        }
      });
    }
  };
}]);
