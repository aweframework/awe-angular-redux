describe('Screen controller', function() {
  let scope, controller, $utilities, $settings, $actionController, $screen, $window;
  let originalTimeout;

  // Mock module
  beforeEach(function() {
    angular.mock.module('aweApplication');

    // Inject controller
    inject(["$rootScope", "$controller", "Screen", "AweSettings", "AweUtilities", "ActionController", "$window",
      function($rootScope, $controller, _Screen_, _AweSettings_, _AweUtilities_, _ActionController_, _$window_){
      scope = $rootScope.$new();
      $utilities = _AweUtilities_;
      $screen = _Screen_;
      $settings = _AweSettings_;
      $actionController = _ActionController_;
      $window = _$window_;
      controller = $controller('ScreenController', {
        '$scope': scope,
        'Screen': $screen,
        'AweSettings': $settings,
        'AweUtilities': $utilities,
        'ActionController': $actionController,
        '$window': $window
      });
    }]);

    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  // A simple test to verify the controller exists
  it('should exist', function() {
    expect(controller).toBeDefined();
  });

  // Once initialized, launch tests
  describe('once initialized', function() {
    // Check show actions
    it('checks whether show actions or not', function() {
      expect(controller.showActions()).toBe($settings.get("actionsStack") > 0);
    });

    // Check action information
    it('should log action information', function() {
      let action = {action: "miAccion", parameters: ["tutu", {lala: 1}]};
      spyOn(console, 'info').and.callThrough();
      controller.showInfo(action);
      expect(console.info).toHaveBeenCalled();
    });

    // Check action status
    it('should check if action is running', function() {
      let action = {action:"miAccion", status:"STATUS_RUNNING", parameters: ["tutu", {lala: 1}]};
      expect(controller.isRunning(action)).toBe(true);
    });

    /**
     * Call a screen action
     * @param {string} actionName Action name
     * @param {string} actionMethod Action method
     * @param {object} parameters Parameters
     * @param {boolean} async Async execution
     * @param {boolean} silent Silent execution
     * @param {boolean} top Add on top action
     * @param {function} done Launch when done
     */
    function callScreenAction(actionName, actionMethod, parameters, async, silent, top, done) {
      // Spy screen action
      spyOn($screen, actionMethod).and.callFake(done);

      // Launch action
      $actionController.closeAllActions();
      let action = $actionController.generateAction({type: actionName, context: "screen", ...parameters}, {view: "base"}, async, silent);
      $actionController.addActionList([action], top, {});
    }

    // Call screen action
    it('should call a screen action', function(done) {
      return callScreenAction("screen", "screen", {parameters:{language:"es", theme:"clean", token: null}, target: "signin"}, true, true, true, done);
    });

    // Call reload action
    it('should call a reload action', function(done) {
      return callScreenAction("reload", "reload", {parameters:{}}, false, true, true, done);
    });

    // Call back action
    it('should call a back action', function(done) {
      return callScreenAction("back", "back", {parameters:{}}, true, true, false, done);
    });

    // Call wait action
    it('should call a wait action', function(done) {
      return callScreenAction("wait", "wait", {parameters:{}}, false, false, true, done);
    });

    // Call change-language action
    it('should call a change-language action', function(done) {
      return callScreenAction("change-language", "changeLanguage", {parameters:{}}, true, false, false, done);
    });

    // Call change-theme action
    it('should call a change-theme action', function(done) {
      return callScreenAction("change-theme", "changeTheme", {parameters:{}}, false, false, false, done);
    });

    // Call screen-data action
    it('should call a screen-data action', function(done) {
      return callScreenAction("screen-data", "screenData", {parameters:{}}, false, true, false, done);
    });

    // Call dialog action
    it('should call a dialog action', function(done) {
      return callScreenAction("dialog", "openDialog", {parameters:{}}, true, true, true, done);
    });

    // Call close action
    it('should call a close action', function(done) {
      return callScreenAction("close", "closeDialog", {parameters:{}}, true, false, true, done);
    });

    // Call close-cancel action
    it('should call a close-cancel action', function(done) {
      return callScreenAction("close-cancel", "closeDialogAndCancel", {parameters:{}}, true, true, false, done);
    });

    // Call get-file action
    it('should call a get-file action', function(done) {
      return callScreenAction("get-file", "getFile", {parameters:{}}, false, true, false, done);
    });

    // Call enable-dependencies action
    it('should call a enable-dependencies action', function(done) {
      return callScreenAction("enable-dependencies", "enableDependencies", {parameters:{}}, false, false, true, done);
    });

    // Call disable-dependencies action
    it('should call a disable-dependencies action', function(done) {
      return callScreenAction("disable-dependencies", "disableDependencies", {parameters:{}}, true, false, true, done);
    });

    // Call add-class action
    it('should call a add-class action', function(done) {
      return callScreenAction("add-class", "addClass", {parameters:{}}, true, false, false, done);
    });

    // Call remove-class action
    it('should call a remove-class action', function(done) {
      return callScreenAction("remove-class", "removeClass", {parameters:{}}, true, false, false, done);
    });

    // Call print action
    it('should call a print action', function(done) {
      return callScreenAction("print", "screenPrint", {parameters:{}}, true, false, false, done);
    });

    // Call close-window action
    it('should call a close-window action', function(done) {
      return callScreenAction("close-window", "closeWindow", {parameters:{}}, true, false, false, done);
    });

    // Call end-dependency action
    it('should call a end-dependency action', function(done) {
      return callScreenAction("end-dependency", "endDependency", {parameters:{}}, true, false, false, done);
    });
  });
});