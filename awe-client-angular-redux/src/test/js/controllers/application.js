describe('Application controller', function() {
  var scope, controller, $utilities, $ngRedux, $loadingBar, $settings;
  var currentStatus = 0;
  // Mock module
  beforeEach(function() {
    angular.mock.module('aweApplication');
    inject(["$rootScope", "$controller", "$log", "AweUtilities", "LoadingBar", "$ngRedux", "AweSettings",
    function($rootScope, $controller, _$log_, _AweUtilities_, _LoadingBar_, _$ngRedux_, _AweSettings_){
      scope = $rootScope.$new();
      $utilities = _AweUtilities_;
      $ngRedux = _$ngRedux_;
      $loadingBar = _LoadingBar_;
      $settings = _AweSettings_;

      controller = $controller('AppController', {
        '$scope': scope,
        '$log': _$log_,
        '$ngRedux': $ngRedux,
        'LoadingBar': $loadingBar,
        'AweUtilities': $utilities
      });
    }]);
  });

  // Mock module
  afterAll(function(done) {
    $settings.update({"actionsStack": 0});
    var disconnect = $ngRedux.connect(state => ({actionsStack: state.settings.actionsStack}))(state => {
      currentStatus = 0;
      done();
    });
  });

  // A simple test to verify the controller exists
  it('should exist', function() {
    expect(controller).toBeDefined();
  });

  // Once initialized, launch tests
  describe('once initialized', function() {

    // Check browser
    it('checks if browser is internet explorer', function() {
      let browser = $utilities.getBrowser();
      expect(controller.isIE()).toEqual(browser.includes("ie") ? browser : `not-ie ${browser}`);
    });

    // Check screen data
    it('checks screen data', function() {
      expect(controller.screen.view).toEqual("base");
    });

    // Check settings data
    it('check settings', function() {
      expect(controller.settings.applicationName).toEqual("AWE (Almis Web Engine)");
    });

    // Check keypress event
    it('checks event keypress with a Alt+Shift+1 key', function(done) {
      controller.onKeydown({
        shiftKey: true,
        altKey: true,
        which: 49
      });
      var disconnect = $ngRedux.connect(state => ({actionsStack: state.settings.actionsStack}))(state => {
        currentStatus = 1000;
        expect(state.actionsStack).toEqual(currentStatus);
        done();
      });
      scope.$on("$destroy", disconnect);
    });

    // Check keypress event
    it('checks event keypress with a Alt+Shift+0 key', function(done) {
      controller.onKeydown({
        shiftKey: true,
        altKey: true,
        which: 48
      });
      var disconnect = $ngRedux.connect(state => ({actionsStack: state.settings.actionsStack}))(state => {
        currentStatus = 0;
        expect(state.actionsStack).toEqual(currentStatus);
        done();
      });
      scope.$on("$destroy", disconnect);
    });

    // Check keypress event
    it('checks event keypress with other key', function(done) {
      controller.onKeydown({
        shiftKey: false,
        altKey: true,
        which: 33
      });
      var disconnect = $ngRedux.connect(state => ({actionsStack: state.settings.actionsStack}))(state => {
        expect(state.actionsStack).toEqual(currentStatus);
        done();
      });
      scope.$on("$destroy", disconnect);
    });
  });
});