describe('Screen service', function() {
  var $utilities, $ngRedux, $settings, $actionController, $screen, $windowMock, $control, $provide, $rootScope, $load;
  var currentStatus = 0;
  var originalTimeout;

  // Mock module
  beforeEach(function() {
    $windowMock = {print: () => null, close: () => null, sessionStorage: {removeItem: () => null}};
    angular.mock.module('aweApplication', {
      '$window': $windowMock
    });

    inject(["$injector", function($injector) {
      $rootScope = $injector.get('$rootScope');
      $utilities = $injector.get('AweUtilities');
      $settings = $injector.get('AweSettings');
      $screen = $injector.get('Screen');
      $control = $injector.get('Control');
      $ngRedux = $injector.get('$ngRedux');
      $actionController = $injector.get('ActionController');
      $load = $injector.get('Load');

      // Catch clearToken && load
      spyOn($settings, "clearToken");
      spyOn($load, "start");
    }]);

    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  /**
   * Launch a screen action
   * @param {String} actionName Action name
   * @param {String} actionMethod Action method
   * @param {Object} parameters Parameters
   * @param {Function} done Launch when done
   */
  function launchScreenAction(actionName, actionMethod, parameters, done) {
    // Spy screen action
    let acceptAction = $actionController.acceptAction.bind($actionController);
    spyOn($actionController, "acceptAction").and.callFake((action) => {
      acceptAction(action);
      done();
    });

    // Launch action
    $actionController.closeAllActions();
    let action = $actionController.generateAction({type: actionName, ...parameters}, {address: {view: "base"}}, true, true);
    $screen[actionMethod].call(this, action);
  }

  // Launch screen action
  it('should launch a screen action', function(done) {
    return launchScreenAction("screen", "screen", {parameters:{language:"es", theme:"clean", token: null}, context: "screen", target: "signin"}, done);
  });

  // Launch screen action
  it('should launch a screen action with token', function(done) {
    return launchScreenAction("screen", "screen", {parameters:{language:"es", theme:"clean", token: "alallalala", target: "/"}}, done);
  });

  // Launch screen action
  it('should launch a screen action with screen', function(done) {
    $settings.update({"reloadCurrentScreen": true});
    launchScreenAction("screen", "screen", {parameters:{screen: "/"}}, done);
    $settings.update({"reloadCurrentScreen": false});
  });

  // Launch reload action
  it('should launch a reload action', function(done) {
    return launchScreenAction("reload", "reload", {parameters:{}}, done);
  });

  // Launch back action
  it('should launch a back action', function(done) {
    spyOn($ngRedux, "getState").and.returnValue({router:{prevState:{}, prevParams:{}}});
    return launchScreenAction("back", "back", {parameters:{}}, done);
  });

  // Launch back action
  it('should launch a wait action', function() {
    jasmine.clock().install();
    spyOn($utilities, "timeout")
    launchScreenAction("wait", "wait", {parameters:{target:5}}, () => null);
    jasmine.clock().tick(6);
    jasmine.clock().uninstall();
    expect($utilities.timeout).toHaveBeenCalled();
  });

  // Launch change-language action
  it('should launch a change-language action', function(done) {
    return launchScreenAction("change-language", "changeLanguage", {parameters: {}, target: "language", context: "base"}, done);
  });

  // Launch change-language action
  it('should launch a change-language action with a defined language', function(done) {
    $control.changeComponent({component: "language", view: "base"}, {model: {values: [{selected: true, value: "fr", label: "Français"}]}});
    return launchScreenAction("change-language", "changeLanguage", {parameters: {}, target: "language", context: "base"}, done);
  });

  // Launch change-language action
  it('should launch a change-language action with a language parameter', function(done) {
    return launchScreenAction("change-language", "changeLanguage", {parameters: {"language": "fr"}, context: "base"}, done);
  });

  // Launch change-theme action
  it('should launch a change-theme action', function(done) {
    return launchScreenAction("change-theme", "changeTheme", {parameters: {}, target: "theme", context: "base"}, done);
  });

  // Launch change-theme action
  it('should launch a change-theme action with a defined theme', function(done) {
    $control.changeComponent({component: "theme", view: "base"}, {model: {values: [{selected: true, value: "default", label: "Default"}]}});
    return launchScreenAction("change-theme", "changeTheme", {parameters: {}, target: "theme", context: "base"}, done);
  });

  // Launch change-theme action with parameter
  it('should launch a change-theme action with a theme parameter', function(done) {
    return launchScreenAction("change-theme", "changeTheme", {parameters: {"theme": "sky"}, context: "base"}, done);
  });

  // Launch screen-data action
  it('should launch a screen-data action', function(done) {
    $rootScope.firstLoad = true;
    launchScreenAction("screen-data", "screenData", {parameters:{view: "base", screenData:{actions: [{type: "reload"}], components: [{
          id: "cod_usr",
          controller: {checkInitial: true, checkTarget:false, checked:false, component:"text", contextMenu:[], dependencies:[], icon:"user signin-form-icon", id:"cod_usr", loadAll:false, optional:false, placeholder:"SCREEN_TEXT_USER", printable:true, readonly:false, required:true, size:"lg", strict:true, style:"no-label", validation:"required", visible:true},
          model: {defaultValues:[], page:1, records:0, selected:[], total:0, values:[]}
        },{
          id: "pwd_usr",
          controller: {checkInitial: true, checkTarget:false, checked:false, component:"text", contextMenu:[], dependencies:[], icon:"user signin-form-icon", id:"cod_usr", loadAll:false, optional:false, placeholder:"SCREEN_TEXT_USER", printable:true, readonly:false, required:true, size:"lg", strict:true, style:"no-label", validation:"required", visible:true},
          model: {defaultValues:[], page:1, records:0, selected:["test"], total:0, values:[]}
         },{
           id: "oth_usr",
           controller: {checkInitial: true, checkTarget:false, checked:false, component:"text", contextMenu:[], dependencies:[], icon:"user signin-form-icon", id:"cod_usr", loadAll:false, optional:false, placeholder:"SCREEN_TEXT_USER", printable:true, readonly:false, required:true, size:"lg", strict:true, style:"no-label", validation:"required", visible:true},
           model: {defaultValues:[], page:1, records:2, selected:["test"], total:1, values:[{value:"test", label:"test"}, {value: "oth", label: "other"}]}
       },{
         id: "grd_usr",
         controller: {columnModel: [], checkInitial: true, checkTarget:false, checked:false, component:"text", contextMenu:[], dependencies:[], icon:"user signin-form-icon", id:"cod_usr", loadAll:false, optional:false, placeholder:"SCREEN_TEXT_USER", printable:true, readonly:false, required:true, size:"lg", strict:true, style:"no-label", validation:"required", visible:true},
         model: {defaultValues:[], page:1, records:1, selected:[], total:1, values:[{id:1, tutu:"aasd", lala: "awsda"}]}
       }
      ], screen: {name: "TEST"}, messages: []}}}, () => {
      expect($ngRedux.getState().screen.base.name).toBe("TEST");
      $actionController.closeAllActions();
      done();
    });
  });

  // Launch dialog action
  it('should launch a dialog action', function() {
    return launchScreenAction("dialog", "openDialog", {parameters:{}}, () => null);
  });

  // Launch close action
  it('should launch a close action', function(done) {
    return launchScreenAction("close", "closeDialog", {parameters:{}}, done);
  });

  // Launch close-cancel action
  it('should launch a close-cancel action', function(done) {
    return launchScreenAction("close-cancel", "closeDialogAndCancel", {parameters:{}}, done);
  });

  // Launch get-file action
  it('should launch a get-file action', function() {
    return launchScreenAction("get-file", "getFile", {parameters:{}}, () => null);
  });

  // Launch enable-dependencies action
  it('should launch a enable-dependencies action', function(done) {
    return launchScreenAction("enable-dependencies", "enableDependencies", {parameters:{}}, done);
  });

  // Launch disable-dependencies action
  it('should launch a disable-dependencies action', function(done) {
    return launchScreenAction("disable-dependencies", "disableDependencies", {parameters:{}}, done);
  });

  // Launch add-class action
  it('should launch a add-class action', function(done) {
    return launchScreenAction("add-class", "addClass", {parameters:{}}, done);
  });

  // Launch remove-class action
  it('should launch a remove-class action', function(done) {
    return launchScreenAction("remove-class", "removeClass", {parameters:{}}, done);
  });

  // Launch print action
  it('should launch a print action', function(done) {
    spyOn($windowMock, "print");
    return launchScreenAction("print", "screenPrint", {id: 1, parameters:{}}, () => {
      expect($windowMock.print).toHaveBeenCalled();
      done();
    });
  });

  // Launch close-window action
  it('should launch a close-window action', function(done) {
    spyOn($windowMock, "close").and.callFake(() => {
      console.info("spy window closed");
      done();
    });
    launchScreenAction("close-window", "closeWindow", {id: 2, parameters:{}}, () => {
      console.info("window close action accepted");
    });
  });

  // Launch end-dependency action
  it('should launch a end-dependency action', function(done) {
    return launchScreenAction("end-dependency", "endDependency", {parameters:{}}, done);
  });
});