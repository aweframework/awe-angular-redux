// Application
import "../../main/resources/js/awe/app";

// Tests libraries
import "angular-mocks";

// Tests
import './controllers/tests.js';
import './filters/tests.js';
import './components/tests.js';
import './services/tests.js';