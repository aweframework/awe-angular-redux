describe('Filters', function() {
  var $filter, $translate;
  // Mock module
  beforeEach(function() {
    angular.mock.module('aweApplication');
    inject(["$filter", "$translate",
    function (_$filter_, _$translate_) {
      $filter = _$filter_;
      $translate = _$translate_;
    }]);
  });

  // Reverse test
  it('should reverse an array', function() {
    // Arrange
    var foo = ['232', '323', 'sddf', 2, 'aa'], result;

    // Act
    result = $filter('reverse')(foo);

    // Assert
    expect(result).toEqual(['aa', 2, 'sddf', '323', '232']);
  });

  // Remove selected
  it('should remove selected results', function() {
    // Arrange
    let values = [
      {value: "tutuaaa", label: "asadsa", otros: "asaweee"},
      {value: "asdaa", label: "aweer", otros: "awghs"},
      {value: "FGFFG", label: "sdctutusd", otros: "asaweee"},
      {value: "asca", label: "asadsttha", otros: "tutu"}
    ];
    let selected = [
      {value: "tutuaaa", label: "asadsa", otros: "asaweee"},
      {value: "FGFFG", label: "sdctutusd", otros: "asaweee"}
    ];
    let result;

    // Act
    result = $filter('removeSelected')(values, selected);

    // Assert
    expect(result).toEqual([
      {value: "asdaa", label: "aweer", otros: "awghs"},
      {value: "asca", label: "asadsttha", otros: "tutu"}
    ]);
  });

  // Remove empty selected
  it('should let values untouched', function() {
    // Arrange
    let values = [
      {value: "tutuaaa", label: "asadsa", otros: "asaweee"},
      {value: "asdaa", label: "aweer", otros: "awghs"},
      {value: "FGFFG", label: "sdctutusd", otros: "asaweee"},
      {value: "asca", label: "asadsttha", otros: "tutu"}
    ];
    let result;

    // Act
    result = $filter('removeSelected')(values, null);

    // Assert
    expect(result).toEqual(values);
  });

  // Remove empty selected
  it('should let values untouched', function() {
    // Arrange
    let values = [
      {value: "tutuaaa", label: "asadsa", otros: "asaweee"},
      {value: "asdaa", label: "aweer", otros: "awghs"},
      {value: "FGFFG", label: "sdctutusd", otros: "asaweee"},
      {value: "asca", label: "asadsttha", otros: "tutu"}
    ];
    let result;

    // Act
    result = $filter('removeSelected')(values, null);

    // Assert
    expect(result).toEqual(values);
  });

  // Translate a value (without defined translations)
  it('should translate a value', function() {
    // Arrange
    let textToTranslate = "text to translate";
    let result;

    // Act
    result = $filter('translateMultiple')(textToTranslate);

    // Assert
    expect(result).toEqual("text to translate");
  });

  // Translate a value (with defined translations)
  it('should translate a value', function() {
    // Arrange
    let textToTranslate = "text to  translate";
    let result;
    spyOn($translate, "instant").and.callFake((text) => {
      switch (text) {
        case "text":
          return "LALA";
        case "to":
          return "2";
        case "translate":
          return "TRANSLATED!"
        default:
          return "";
      }
    });

    // Act
    result = $filter('translateMultiple')(textToTranslate);

    // Assert
    expect(result).toEqual("LALA 2  TRANSLATED!");
  });

  // Translate a null value
  it('should translate a null value', function() {
    // Arrange
    let textToTranslate = null;
    let result;


    // Act
    result = $filter('translateMultiple')(textToTranslate);

    // Assert
    expect(result).toEqual("");
  });
});